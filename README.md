# scorm

inrtoduction

මම මේකෙදි කියන්න හදන්නේ මේක යූස් කරන්නෙ කොහොමද කියලා ඔයාලට පුළුවන් files ටික අරගෙන manual Scorm  එකක් හදන්න 
මේකෙදි අපිට captivate භාවිතා කරන්න ඕනේ නැහැ 

අපි බලමු මේක වැඩ කරන්නෙ කොහොමද කියලා මුලින්ම මේක download කරලා බලන්න මේකේ තියෙනවා index කියලා file එකක් අන්න ඒ file එක තමයි අපේ Main file එක 

බලන්න මෙකේ මෙන්න මෙහෙම folders ටිකක් ඇති. 

activity - මෙන්න මේ ෆෝල්ඩර් එකට තමයි ඔයා ඔයාට දාන්න ඕන intractive activities ටික දාන්න ඕනේ මතක තියාගන්න එක දානකොට folders එකෙන් එකට දාන්න ඕනේ 

css 

favicon

js - මේකේ තියෙන්නේ අපි මේ හදල තියෙන්නේ scorm වැඩ කරන්න ඕන කරන java script file ටික 

Package_files - මෙන්න මේ ෆෝල්ඩර් එකේ තමයි ඔයාට scorm එක හදන්න ඕන කරන ෆයිල්ස් ටික තියෙන්නේ

slids - මෙන්න මේ ෆෝල්ඩර් එකේ තමයි අපි index.html එකට ලෝඩ් කරන්න හදන slids ටික තියෙන්නේ

Video - මෙතන තියෙන්නේ slids වලට අදාල කරන videos ටික


---------------------------------------------------------------------------------------------------

මුලින්ම slide කියන folder එක ඇතුලට ගිහිල්ලා මෙන්න මේ විදිහට slid1.html  නම දාල ඔයාට සයිට් එකක් හදන්න පුලුවන්.
දැන් ඔයාට පුලුවන් ඔයාගේ slide එකට use කරන්නේ වීඩියෝ එකක් නම් මෙන්න මේ code එක use කරන්න.

slids/slid1.html

<!DOCTYPE html>
<html id="full">

<head>
    <meta name="description" content="headstart (pvt) ltd New scorm standard">
    <meta name="keywords" content="HTML,CSS,JavaScript">
    <meta name="author" content="Kusal Rathna Bandra">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    
</head>

<body>
    <div id="max">
        <video oncontextmenu="return false;"  controls controlsList="nodownload" class="responsive-video" id="myVideo">
            <source src="./Video/slide_01.mp4" type="video/mp4">
        </video>
    </div>

 <script>
        window.document.getElementById('myVideo').addEventListener('ended', myHandler, true);

        function myHandler() {
            mess();
            a_count++;
            count_cz++;
            set_persentage();
            
            ;
        }
    </script> 
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

</body>

</html>


ඔයා ඒ slide එකට use කරන්නේ interactive activei තියෙනවනම් ඔයාට පුළුවන් මෙන්න මේ code එක use කරන්න

<meta name="description" content="headstart (pvt) ltd New scorm standard">
<meta name="keywords" content="HTML,CSS,JavaScript">
<meta name="author" content="Kusal Rathna Bandra">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" href="../css/materialize.min.css">
<html id="full">



<body>

    <div id="demo">

    </div>

    <script>
        document.getElementById("demo").innerHTML ='<object id="myVideo" type="text/html" data="./activity/slide_02/slide_02.html"></object>';
    </script>


    <script>
        document.getElementById('myVideo').addEventListener('ended', myHandler, false);
        function myHandler(e) {
          
        }
    </script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="./js/script.js"></script>
</body>

</html>


ඔයාට දැන් පුළුවන් මෙන්න මේ විදිහට ඔයාට අවශ්‍ය ප්‍රමාණයට ඔයාගෙ slide ටික හදාගන්න.



---------------------------------------------------------------------------------------------------

අපි දැන් බලමු මේ javascript file එකේ මොනවද අපිට වෙනස් කරන්න තියෙන්නෙ කියලා

js/main.js

// document.addEventListener('contextmenu', event => event.preventDefault());
// full screen page Load
function Fullscreen() {
  var element = document.getElementById("myVideo");
  //requestFullscreen is used to display an element in full screen mode.
  if ("requestFullscreen" in element) {
    element.requestFullscreen();
  } else if ("webkitRequestFullscreen" in element) {
    element.webkitRequestFullscreen();
  } else if ("mozRequestFullScreen" in element) {
    element.mozRequestFullScreen();
  } else if ("msRequestFullscreen" in element) {
    element.msRequestFullscreen();
  }
}
// Exit full screen 
function openFullscreen() {
  var element = document.documentElement;
  //requestFullscreen is used to display an element in full screen mode.
  if ("requestFullscreen" in element) {
    element.requestFullscreen();
  } else if ("webkitRequestFullscreen" in element) { /* Chrome, Safari and Opera */
    element.webkitRequestFullscreen();
  } else if ("mozRequestFullScreen" in element) { /* Firefox */
    element.mozRequestFullScreen();
  } else if ("msRequestFullscreen" in element) { /* IE/Edge */
    element.msRequestFullscreen();
  }
}

function closeFullscreen() {
  if (document.exitFullscreen) {
    document.exitFullscreen();
  } else if (document.mozCancelFullScreen) { /* Firefox */
    document.mozCancelFullScreen();
  } else if (document.webkitExitFullscreen) { /* Chrome, Safari and Opera */
    document.webkitExitFullscreen();
  } else if (document.msExitFullscreen) { /* IE/Edge */
    document.msExitFullscreen();
  }
}
var count_cz = 1;
var a_count = 1; 
var b_count = "This is your current slid  "

//set page on loding with jQuery
$(document).ready(function () {

  $('#demo').load("slids/slid" + a_count + ".html");
  
});


set_persentage();

function next() { // Next slide function 
  a_count++;
 count_cz++;
  set_persentage();

}

function priv() { // previous slide function 
  a_count--;
  count_cz--;
  set_persentage();

}

function set_persentage() {

  let hundred = count_cz / 4 * 100; // get prasent tage 
  
  $('#demo').load("slids/slid" + a_count + ".html");
  document.getElementById("slid").innerHTML = b_count + a_count;
  if (count_cz == 4) { // this is max slide caunt 
    document.getElementById("boxbtn3").style.visibility = "hidden";
  } else {
    document.getElementById("boxbtn2").style.visibility = "visible";
  }
  if (a_count == 1) {
    document.getElementById("boxbtn2").style.visibility = "hidden";
  } else {
    document.getElementById("boxbtn2").style.visibility = "visible";
  }
  document.getElementById("pross").style.width = hundred + "%"; // progress 

}



$("#cont").hover(function () {
  $("#toolbar,.p").delay(50).fadeIn();
  $("#main").addClass("container");

});

$("#demo,#myVideo").hover(function () {
  $("#toolbar").delay(300).fadeOut();


});



function mess() {
    swal({
        title: "Good job!",
        text: "Lesson Done",
        icon: "success",
        timer: 4000,
      });
}

$(document).ready(function(){
    $('.tooltipped').tooltip();
  });


මෙතන තියනවා  function set_persentage()  කියලා function එකක් 

function set_persentage() {

  let hundred = count_cz / 4 * 100; // get prasent tage 
  
  $('#demo').load("slids/slid" + a_count + ".html");
  document.getElementById("slid").innerHTML = b_count + a_count;
  if (count_cz == 4) { // this is max slide caunt 
    document.getElementById("boxbtn3").style.visibility = "hidden";
  } else {
    document.getElementById("boxbtn2").style.visibility = "visible";
  }
  if (a_count == 1) {
    document.getElementById("boxbtn2").style.visibility = "hidden";
  } else {
    document.getElementById("boxbtn2").style.visibility = "visible";
  }
  document.getElementById("pross").style.width = hundred + "%"; // progress 

}

මෙන්න මෙතන තියන 4 කියන value ඒකෙන් වෙන්නේ ඔයගේ scorm එකේ slide ප්‍රමාණය අපට කියන්න පුලුවන් 
හිතන්න ඔයාගේ scorm එකේ තියෙනවා slide දහයක් එහෙනම් ඔයා මේ හතර කියන තැනට 10 කියන ඉලක්කම අනිවාර්යෙන් දාන්න ඕනේ එතකොට තමයි scorm එකේ තියෙන progress bar එකේ හරියට slide count එක බෙදිලා එකට සමානව progress එක පෙන්නන්නේ.