(function (lib, img, cjs, ss, an) {

var p; // shortcut to reference prototypes
lib.ssMetadata = [
		{name:"slide_11_atlas_", frames: [[0,0,219,233],[221,188,195,186],[196,376,194,186],[221,0,195,186],[418,188,194,186],[812,0,195,186],[614,188,194,186],[418,0,195,186],[588,376,194,186],[615,0,195,186],[810,188,194,186],[0,235,194,186],[392,376,194,186]]}
];


// symbols:



(lib.Path_0 = function() {
	this.spriteSheet = ss["slide_11_atlas_"];
	this.gotoAndStop(0);
}).prototype = p = new cjs.Sprite();



(lib.Path_0_1 = function() {
	this.spriteSheet = ss["slide_11_atlas_"];
	this.gotoAndStop(1);
}).prototype = p = new cjs.Sprite();



(lib.Path_10_0 = function() {
	this.spriteSheet = ss["slide_11_atlas_"];
	this.gotoAndStop(2);
}).prototype = p = new cjs.Sprite();



(lib.Path_11_0 = function() {
	this.spriteSheet = ss["slide_11_atlas_"];
	this.gotoAndStop(3);
}).prototype = p = new cjs.Sprite();



(lib.Path_1_0 = function() {
	this.spriteSheet = ss["slide_11_atlas_"];
	this.gotoAndStop(4);
}).prototype = p = new cjs.Sprite();



(lib.Path_2_0 = function() {
	this.spriteSheet = ss["slide_11_atlas_"];
	this.gotoAndStop(5);
}).prototype = p = new cjs.Sprite();



(lib.Path_3_0 = function() {
	this.spriteSheet = ss["slide_11_atlas_"];
	this.gotoAndStop(6);
}).prototype = p = new cjs.Sprite();



(lib.Path_4_0 = function() {
	this.spriteSheet = ss["slide_11_atlas_"];
	this.gotoAndStop(7);
}).prototype = p = new cjs.Sprite();



(lib.Path_5_0 = function() {
	this.spriteSheet = ss["slide_11_atlas_"];
	this.gotoAndStop(8);
}).prototype = p = new cjs.Sprite();



(lib.Path_6_0 = function() {
	this.spriteSheet = ss["slide_11_atlas_"];
	this.gotoAndStop(9);
}).prototype = p = new cjs.Sprite();



(lib.Path_7_0 = function() {
	this.spriteSheet = ss["slide_11_atlas_"];
	this.gotoAndStop(10);
}).prototype = p = new cjs.Sprite();



(lib.Path_8_0 = function() {
	this.spriteSheet = ss["slide_11_atlas_"];
	this.gotoAndStop(11);
}).prototype = p = new cjs.Sprite();



(lib.Path_9_0 = function() {
	this.spriteSheet = ss["slide_11_atlas_"];
	this.gotoAndStop(12);
}).prototype = p = new cjs.Sprite();
// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.timer = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
		
		/*
		var clip = this;
		
		var count= 12;
		
		
		
		
		
		if(this.output.text==0){
		
			
		
			
				
				
				
			
		
			
			
		    	
			
		}
		
		*/
	}
	this.frame_398 = function() {
		this.getStage().getChildAt(0).validate_fun();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(398).call(this.frame_398).wait(1));

	// Layer 2
	this.output = new cjs.Text("15", "bold 90px 'Arial'", "#83A4DE");
	this.output.name = "output";
	this.output.textAlign = "center";
	this.output.lineHeight = 103;
	this.output.lineWidth = 151;
	this.output.parent = this;
	this.output.setTransform(285.5,148.1);

	this.timeline.addTween(cjs.Tween.get(this.output).wait(24).to({text:"14"},0).wait(25).to({text:"13"},0).wait(25).to({text:"12"},0).wait(25).to({text:"11"},0).wait(25).to({text:"10"},0).wait(25).to({text:"9"},0).wait(25).to({text:"8"},0).wait(25).to({text:"7"},0).wait(25).to({text:"6"},0).wait(25).to({text:"5"},0).wait(25).to({text:"4"},0).wait(25).to({text:"3"},0).wait(25).to({text:"2"},0).wait(25).to({text:"1"},0).wait(25).to({text:"0"},0).wait(25));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(208,146.1,155,112);


(lib.hid12 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#014749").ss(2).p("AI/AAQAADlizCnQixCkjqAAQjoAAijikQikikAAjoQAAjnCkikQCkikDnAAQDqAACxCkQCzCnAADkg");

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#2F93BA").s().p("AmaGMQikikAAjoQAAjnCkikQCkikDnAAQDqAACxCkQCzCnAADkQAADlizCnQixCkjqAAQjoAAijikg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-58.5,-56.9,117.1,114);


(lib.hid11 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#014749").ss(2).p("AINAaQAADsiBCTQiGCXjkAAQjmAAikikQikikAAjoQAAjnCkikQCkikDmAAQDgAACKCxQCBCmAADyg");

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#2F93BA").s().p("AloGMQikikAAjoQAAjnCkikQCkikDmAAQDgAACKCxQCBCmAADyQAADsiBCTQiGCXjkAAQjmAAikikg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-53.5,-56.9,107.1,114);


(lib.hid10 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#014749").ss(2).p("AHIAAQAADoiGCjQiFCki9AAQi8AAiFikQiGikAAjnQAAjnCGikQCFikC8AAQC9AACFCkQCGCkAADng");

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#2F93BA").s().p("AlBGLQiGikAAjnQAAjmCGikQCFilC8AAQC9AACFClQCGCjAADnQAADoiGCjQiFCli9AAQi8AAiFilg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-46.5,-56.9,93.2,113.9);


(lib.hid9 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#014749").ss(2).p("AIwAAQAAjnikikQikikjoAAQjnAAikCkQikCkAADnQAADoCkCjQCkCkDnAAQCsAACNhfQCKhdBAiZIi4h1g");

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#2F93BA").s().p("AmLGLQikijAAjoQAAjnCkijQCkilDnAAQDoAACkClQCkCkAADmIjlBlIC4B1QhACZiKBdQiNBgisAAQjnAAikilg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-56.9,-56.9,114,113.9);


(lib.hid8 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#014749").ss(2).p("AG5lXQB+ChgoDWQgoDXi3CPQi3CPjZgOQjagNh+iiQh+ihAojXQAojXC3iOQC3iPDZAOQDbANB9Cig");

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#2F93BA").s().p("AhgIHQjagOh+ihQh+iiAojWQAojXC3iOQC3iPDZAOQDbAOB9ChQB+ChgoDWQgoDXi3CPQinCCjDAAIgmgBg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-55.4,-53,110.2,106);


(lib.hid7 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#014749").ss(2).p("AIvg3QAADnijCkQikCkjoAAQjmAAikikQikikAAjnQAAjdChh3QCUhsD2AAQD2AACXBsQClB3AADdg");

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#2F93BA").s().p("AmLFUQijikAAjnQgBjdCih3QCThsD2AAQD2AACYBsQClB3AADdQAADnijCkQikCkjoAAQjnAAikikg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-56.9,-51.3,113.9,102.8);


(lib.hid6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#014749").ss(2).p("AIwAAQAADoikCkQikCkjoAAQjnAAijikQikikAAjoQAAjnCkikQCkikDmAAQDoAACkCkQCkCkAADng");

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#2F93BA").s().p("AmKGMQikikAAjoQAAjnCkikQCjikDnAAQDoAACkCkQCjCkAADnQAADoijCkQikCkjoAAQjnAAijikg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-56.9,-56.9,113.9,114);


(lib.hid5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#014749").ss(2).p("AIwAAQAADoikCkQikCkjoAAQjnAAikikQikikAAjoQAAjnCkikQCkikDnAAQDoAACkCkQCkCkAADng");

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#2F93BA").s().p("AmLGMQikikAAjoQAAjnCkikQCkikDnAAQDoAACkCkQCkCkAADnQAADoikCkQikCkjoAAQjnAAikikg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-56.9,-56.9,114,114);


(lib.hid4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#014749").ss(2).p("AIwAAQAADoikCkQikCjjoAAQjnAAikijQikikAAjoQAAjnCkikQCkijDnAAQDoAACkCjQCkCkAADng");

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#2F93BA").s().p("AmLGLQikikAAjnQAAjnCkikQCkikDnAAQDoAACkCkQCkCkAADnQAADoikCjQikCkjoAAQjnAAikikg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-56.9,-56.9,114,113.9);


(lib.hid3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#014749").ss(2).p("AIvAAQAADoikCjQijCkjoAAQjnAAikikQikijAAjoQAAjnCkikQCkikDnAAQDoAACjCkQCkCkAADng");

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#2F93BA").s().p("AmKGLQikijAAjoQAAjmCkikQCjilDnAAQDoAACkClQCjCkAADmQAADoijCjQikCljoAAQjnAAijilg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-56.9,-56.9,113.9,113.9);


(lib.hid2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#014749").ss(2).p("AIwAAQAADoikCkQikCkjoAAQjnAAikikQikikAAjoQAAjnCkikQCkikDnAAQDoAACkCkQCkCkAADng");

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#2F93BA").s().p("AmLGMQikikAAjoQAAjnCkikQCkikDnAAQDoAACkCkQCkCkAADnQAADoikCkQikCkjoAAQjnAAikikg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-56.9,-56.9,114,114);


(lib.hid1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#014749").ss(2).p("AIwAAQAADoikCkQikCkjoAAQjnAAikikQikikAAjoQAAjnCkikQCkikDnAAQDoAACkCkQCkCkAADng");

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#2F93BA").s().p("AmLGMQikikAAjoQAAjnCkikQCkikDnAAQDoAACkCkQCkCkAADnQAADoikCkQikCkjoAAQjnAAikikg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-56.9,-56.9,114,114);


(lib.Group_3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.lf(["#000000","rgba(0,0,0,0)"],[0,1],64.2,-171.5,10.8,-51.6).s().p("EhQdABsIAAplMCg7ACrIj1NIg");
	this.shape.setTransform(515,50.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#6E7679").s().p("EhQzABsIAAplMChmAARIkgPig");
	this.shape_1.setTransform(517.2,50.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Group_3, new cjs.Rectangle(0,0,1034.3,101.2), null);


(lib.Group_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.lf(["#000000","rgba(0,0,0,0)"],[0,1],-138.2,0,138.3,0).s().p("AzGAAMAjVgIhIFXK1MgrMAGOg");
	this.shape.setTransform(138.3,54.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#6E7679").s().p("AzJAoMAjigJJIFEK1Mgq5AGOg");
	this.shape_1.setTransform(137.3,54.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Group_2, new cjs.Rectangle(0,0,276.5,109.2), null);


(lib.Path = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(2).p("ALGDWI2LAAQgpAAgdgeQgegdAAgpIAAjjQAAgpAegdQAdgeApAAIWLAAQApAAAdAeQAeAdAAApIAADjQAAApgeAdQgdAegpAAg");
	this.shape.setTransform(82,22.4);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000000").s().p("ArFDWQgpAAgdgeQgdgdgBgqIAAjiQABgpAdgdQAdgdApgBIWLAAQApABAdAdQAdAdAAApIAADiQAAAqgdAdQgdAegpAAg");
	this.shape_1.setTransform(82,22.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Path, new cjs.Rectangle(0,0,163.9,48.5), null);


(lib.Path_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#000000").ss(2).p("ALFDWI2JAAQgpAAgegeQgdgdAAgpIAAjjQAAgpAdgdQAegeApAAIWJAAQAqAAAdAeQAdAdAAApIAADjQAAApgdAdQgdAegqAAg");
	this.shape_2.setTransform(81.9,22.4);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("ArEDWQgpAAgegeQgdgdAAgqIAAjiQAAgpAdgdQAegdApgBIWJAAQAqABAdAdQAdAdAAApIAADiQAAAqgdAdQgdAegqAAg");
	this.shape_3.setTransform(81.9,22.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Path_1, new cjs.Rectangle(0,0,163.9,48.5), null);


(lib.Path_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(2).p("ALFDWI2JAAQgpAAgegeQgdgdAAgpIAAjjQAAgpAdgdQAegeApAAIWJAAQAqAAAdAeQAdAdAAApIAADjQAAApgdAdQgdAegqAAg");
	this.shape_4.setTransform(81.9,22.4);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#000000").s().p("ArEDWQgpAAgegeQgdgdAAgqIAAjiQAAgpAdgdQAegdApgBIWJAAQAqABAdAdQAdAdAAApIAADiQAAAqgdAdQgdAegqAAg");
	this.shape_5.setTransform(81.9,22.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_5},{t:this.shape_4}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Path_2, new cjs.Rectangle(0,0,163.9,48.5), null);


(lib.Path_11 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.Path_11_0();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Path_11, new cjs.Rectangle(0,0,195,186), null);


(lib.Path_10 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.Path_10_0();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Path_10, new cjs.Rectangle(0,0,194,186), null);


(lib.Path_9 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.Path_9_0();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Path_9, new cjs.Rectangle(0,0,194,186), null);


(lib.Path_8 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.Path_8_0();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Path_8, new cjs.Rectangle(0,0,194,186), null);


(lib.Path_7 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.Path_7_0();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Path_7, new cjs.Rectangle(0,0,194,186), null);


(lib.Path_6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.Path_6_0();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Path_6, new cjs.Rectangle(0,0,195,186), null);


(lib.Path_5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.Path_5_0();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Path_5, new cjs.Rectangle(0,0,194,186), null);


(lib.Path_4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.Path_4_0();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Path_4, new cjs.Rectangle(0,0,195,186), null);


(lib.Path_3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.Path_3_0();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Path_3, new cjs.Rectangle(0,0,194,186), null);


(lib.Path_2_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.Path_2_0();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Path_2_1, new cjs.Rectangle(0,0,195,186), null);


(lib.Path_1_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.Path_1_0();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Path_1_1, new cjs.Rectangle(0,0,194,186), null);


(lib.Path_12 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.Path_0_1();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Path_12, new cjs.Rectangle(0,0,195,186), null);


(lib.Path_12_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("A4hJYIiRivIn2CvIKHyvMA7KAAAIAASvg");
	this.shape.setTransform(221.7,60);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Path_12_1, new cjs.Rectangle(0,0,443.3,120.1), null);


(lib.Path_11_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("A4gJYIiSivIn2CvIKIyvMA7JAAAIAASvg");
	this.shape.setTransform(221.7,60);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Path_11_1, new cjs.Rectangle(0,0,443.3,120.1), null);


(lib.Path_11_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000000").s().p("A4gJYIiSivIn2CvIKIyvMA7JAAAIAASvg");
	this.shape_1.setTransform(221.7,60);

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1));

}).prototype = getMCSymbolPrototype(lib.Path_11_2, new cjs.Rectangle(0,0,443.3,120.1), null);


(lib.Path_13 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance_1 = new lib.Path_0();
	this.instance_1.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(1));

}).prototype = getMCSymbolPrototype(lib.Path_13, new cjs.Rectangle(0,0,219,233), null);


(lib.Symbol1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AAQAoIAAgPQAKAAAGgGQAGgIAAgKQAAgKgGgIQgGgIgKABQgLgBgHAJQAJABAGAIQAGAHAAAJQAAAOgLAJQgJAJgQgBQglAAAAgoIAAgGIAOAAIAAABQAAADAEAAQADAAADgCIAFgEIACgFIABgCQAAgEgCgDQgBgCgDAAQgGAAAAAIIgPAAIgBgEQAAgIAGgFQAGgHAJAAQAHABAFADQAFADABAGQAKgNASAAQAQAAALAMQAKAMABAPQAAARgJAMQgJANgSgBIgDAAgAgPAYQAGAAAFgDQAEgEABgFIAAgEQAAgGgEgDIgFgBQgBAAgBAAQAAAAgBAAQAAABgBAAQAAAAgBAAIgJAIQgGAEgFAAIgFgBQAEAPATgBg");
	this.shape.setTransform(147,18.9);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgKAUIAAgZQAAgFgGAAQgGAAgCAFIAAgNQACgDAEgCQAEgCAEAAQAIAAACAHQACgHAKAAQAGAAADAFQAEAFABAGQAAAMgNADQgGABgEgCIAAAVQgHgFgGgBgAADgKIAAAHIADABQAEAAABgGQAAAAgBgBQAAAAAAgBQAAAAAAgBQgBAAAAAAQAAgBgBAAQAAAAAAgBQgBAAAAAAQAAAAgBAAQAAAAgBAAQAAAAgBABQAAAAAAAAQgBABAAABg");
	this.shape_1.setTransform(138.4,13.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AAQAoIAAgPQAKAAAGgGQAGgIAAgKQAAgKgGgIQgGgIgKABQgLgBgHAJQAJABAGAIQAGAHAAAJQAAAOgLAJQgJAJgQgBQglAAAAgoIAAgGIAOAAIAAABQAAADAEAAQADAAADgCIAFgEIACgFIABgCQAAgEgCgDQAAAAgBgBQAAAAgBgBQAAAAgBAAQAAAAgBAAQgGAAAAAIIgPAAIgBgEQAAgIAGgFQAGgHAJAAQAHABAFADQAFADABAGQAKgNASAAQAQAAALAMQAKAMABAPQAAARgJAMQgJANgSgBIgDAAgAgPAYQAGAAAFgDQAEgEABgFIAAgEQAAgGgEgDIgFgBQgBAAgBAAQAAAAgBAAQAAABgBAAQAAAAgBAAIgJAIQgGAEgFAAIgFgBQAEAPATgBg");
	this.shape_2.setTransform(135.3,18.9);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AABAgQgFAIgQAAQgPAAgHgLQgIgLABgSQABgQALgMQALgMAQAAIAFABIAAAOQgLgBgIAHQgHAIgBALQgBAJADAHQAEAIAIAAQAMAAAAgLIAAgHIAOAAIAAAHQAAAFAEADQAEADAGAAQAGAAADgEQADgEgBgGQAAgNgTAAIgSAAIAAgIQAAgbAYgCQAWAAACAVIgQAAQAAgGgHAAQgEAAgCACQgDACAAAEIAIAAQANAAAJAIQAIAIAAAMQAAANgHAIQgHAIgKAAIgCABQgSAAgFgJg");
	this.shape_3.setTransform(124.2,19);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AgjAhQgOgJAAgQQAAgKAJgIQAIgHANAAIAMAAQAAgIgMAAQgLAAACAHIgRAAIAAgDQAAgJAIgFQAHgFALAAQAOAAAFAIQAFgEAEgCQAGgCAJAAQALAAAHAHQADAEAAAHIgBAEIgRAAQAAgBAAAAQAAgBAAgBQAAAAAAAAQAAgBgBAAQgBgDgHAAQgKAAAAAIIALAAQANAAAIAFQAKAJAAAMQAAAQgPAIQgOAIgVAAQgWAAgNgIgAgbAAQgFACABAFQgBAJAKAFQAJAEANAAQAOAAAJgEQAJgFAAgIQAAgGgFgCQgEgDgHAAIggAAQgHAAgEADg");
	this.shape_4.setTransform(113.6,19);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AgSAoQgPAAgIgIQgHgIAAgOQABgJAHgEIgMAAIAAgNIAvAAQAAgJgLAAQgKAAAAAHIgQAAQgBgKAIgGQAHgFAKAAIAMABQAGACACAEQAEgFAHgBIANgBQAIAAAGAGQAGAGAAAIIgRAAQABgGgKAAQgKAAAAAJIAOAAQAdAAAAAcQAAANgIAHQgJAIgMAAQgRAAgFgJQgEAJgPAAIgBAAgAAKAHIAAAHQAAAKAOAAQAFAAAEgEQADgFAAgGQAAgGgFgDQgFgDgHAAIgiAAQgGAAgFADQgFAEAAAGQAAAGAEAEQADAEAGAAQAGAAAEgDQADgCAAgFIAAgHg");
	this.shape_5.setTransform(102.7,18.9);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AgQAhQgJgIABgRQAAgKAJgKQAHgJAIgDIgZAAIAAgNIAwAAIAAALQgNAJgHAHQgIAJgBAJIABAJQACAHAFgBQAEAAACgDQACgCAAgEIAAgEIAQAAIAAAHQAAAKgGAGQgHAGgJAAQgNgBgHgFg");
	this.shape_6.setTransform(88.8,19.2);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AgUAyQgJgEgGgHQgJgMAAgOQAAgHACgGQAGgSAVgIIAJgFIAKgJQAEgEAEgJIATAAQgCAIgEAGQgHAJgIAEIAGABQAOADAIAMQAHAKAAANQAAAKgEAJQgFAKgMAGQgLAGgNAAQgKAAgKgEgAgUgDQgGAGAAAJQAAALAIAHQAHAGAMAAQAOAAAHgJQAFgHAAgIQAAgLgIgHQgHgGgMAAQgNAAgHAJg");
	this.shape_7.setTransform(81.2,17.7);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AgSAPQgHgGAAgJQgCgSARgCIAAAOQAAAGADADQAEAEAFAAQAEAAADgDQACgDAAgCIAAgEIAPAAIABAFQAAAIgIAHQgHAGgKAAQgNAAgHgGg");
	this.shape_8.setTransform(74,23.6);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AgmAlQgKgEgCgJQAAgCABgDQABgBAAgBQAAgBABAAQAAgBAAAAQAAgBAAAAIgCgBIgEACIgCADIAAgOIAOgOIANgLIAAgCQAAAAAAgBQAAAAAAAAQAAgBgBAAQAAAAAAAAIgEgBQgEABAAAFIABAEIgQAAIAAgFQAAgIAGgFQAFgFAIAAQAOgBADALQAPgKASAAQAQAAALAKQAMAMAAATQAAARgKAKQgKALgRAAIgEgBIAAgNQALAAAGgGQAHgHAAgLQAAgMgHgHQgHgHgNAAQgHAAgFACQAIADAHAIQAGAIAAAKQAAAPgMAJQgKAJgQAAQgMAAgJgDgAgZgEIgLAJQACACACADIAAACIAAACIgBADQAAAEAFACQAEACAFAAQAIAAAHgEQAHgFAAgHQABgIgEgFQgFgFgHAAIgDAAQgEAAgGAFg");
	this.shape_9.setTransform(70.5,19);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AAZAPQABgIgJgEQgHgDgKAAQgJAAgHACQgJAEAAAGIABADIgRAAIgBgHQAAgKAOgGQANgGAPABQARAAAMAEQAOAHAAAJIgCAIg");
	this.shape_10.setTransform(59.2,13.7);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("AggApQgOgLgCgTIgBgGQAAgWAOgHQAAgMAHgHQAHgHALAAQAKAAAFAHQAGgIAMABQAKAAAHAKQAFAKAAAMIAAADIg5AAIgHABQABAKAHAFQAHAGAKAAQAPAAAGgOIARAAQgEANgKAIQgLAIgPAAQgMAAgKgGQgKgHgEgKIAAADQAAAPAKAKQAKAJAOAAQALAAAIgFQAJgFADgKIARAAQgCARgOAJQgNAKgTAAQgUAAgOgLgAALgcIASAAQgCgJgHAAQgHAAgCAJgAgTgcIAQAAQgBgJgHAAQgHAAgBAJg");
	this.shape_11.setTransform(59.5,20.1);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("AAZAPQABgIgJgEQgHgDgKAAQgJAAgHACQgJAEAAAGIABADIgRAAIgBgHQAAgKAOgGQANgGAPABQARAAAMAEQAOAHAAAJIgCAIg");
	this.shape_12.setTransform(48.8,13.7);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("AgjAhQgOgJAAgQQAAgKAJgIQAIgHANAAIAMAAQAAgIgMAAQgLAAABAHIgQAAIgBgDQAAgJAJgFQAHgFAKAAQAPAAAFAIQAFgEAEgCQAGgCAJAAQALAAAHAHQADAEAAAHIgBAEIgRAAQAAgBAAAAQAAgBAAgBQAAAAAAAAQAAgBgBAAQgBgDgHAAQgJAAgBAIIALAAQANAAAIAFQAKAJAAAMQAAAQgQAIQgNAIgVAAQgVAAgOgIgAgbAAQgFACABAFQAAAJAJAFQAJAEANAAQAOAAAJgEQAJgFAAgIQAAgGgFgCQgEgDgHAAIggAAQgHAAgEADg");
	this.shape_13.setTransform(48.9,19);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#F44336").s().p("As2C8QhNABg4g4Qg3g4AAhNQAAhNA3g3QA4g4BNAAIZtAAQBOAAA3A4QA3A3AABNQAABNg3A4Qg3A4hOgBg");
	this.shape_14.setTransform(101.1,18.9);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#F44336").s().p("AiBhPIEDAAIiCCfg");
	this.shape_15.setTransform(101.1,45.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol1, new cjs.Rectangle(0,0,202.3,53.4), null);


(lib.chk12 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#2EB2BC").ss(2).p("Ak2ovQByAABwAtQBsAqBXBOQC+CnAADjQAADji+CoQhXBNhsArQhwAthyAA");
	this.shape.setTransform(28.5,0);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.lf(["rgba(202,249,255,0)","#CAF9FF"],[0,0.984],-30.6,0,30.6,0).s().p("AkxovQByAABwAsQBsArBXBOQC+CnAADjQAADki+CnQhXBNhsAsQhwAshyAAg");
	this.shape_1.setTransform(28,0);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#2EB2BC").ss(2).p("AI/AAQAADlizCmQixCljpAAQjoAAikilQikijAAjoQAAjnCkijQCkilDoAAQDpAACxClQCzCmAADkg");
	this.shape_2.setTransform(-1.1,0);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.lf(["#15EEFF","#FFFFFF","#15EEFF","#FFFFFF"],[0,0.251,0.498,0.816],-121.7,58.9,171.3,-84.2).s().p("AmaGLQikijAAjoQAAjnCkijQCkilDoAAQDpAACxClQCzCmAADkQAADlizCmQixCljpAAQjoAAikilg");
	this.shape_3.setTransform(-1.1,0);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#014749").ss(2).p("Ak2ovQBzAABvAsQBsArBXBNQC+CoAADjQAADki+CoQhXBNhsArQhvAshzAA");
	this.shape_4.setTransform(28.5,0);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.lf(["rgba(202,249,255,0)","#CAF9FF"],[0,0.984],-30.6,0,30.6,0).s().p("AkxovQBzABBvAsQBsArBXBMQC+CpAADiQAADki+CnQhXBNhsAsQhvArhzAAg");
	this.shape_5.setTransform(28,0);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#014749").ss(2).p("AI/AAQAADlizCnQixCkjpAAQjoAAikikQikikAAjoQAAjnCkikQCkikDoAAQDpAACxCkQCzCnAADkg");
	this.shape_6.setTransform(-1.1,0);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.lf(["#15EEFF","#FFFFFF","#15EEFF","#FFFFFF"],[0,0.251,0.498,0.816],-121.7,58.9,171.3,-84.2).s().p("AmaGLQikijAAjoQAAjnCkikQCkikDoAAQDpAACxCkQCzCnAADkQAADlizCmQixCkjpAAQjoAAikikg");
	this.shape_7.setTransform(-1.1,0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).to({state:[{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-59.6,-56.9,119.2,113.9);


(lib.chk11 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#2EB2BC").ss(2).p("AINAZQAADtiBCTQiGCXjkAAQjmAAikilQikijAAjoQAAjnCkijQCkilDmAAQDgAACKCxQCBCmAADxg");

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.lf(["#15EEFF","#FFFFFF","#15EEFF","#FFFFFF"],[0,0.251,0.498,0.816],-112.1,56.3,160.9,-77).s().p("AloGLQikijAAjoQAAjmCkikQCkilDmAAQDgAACKCxQCBCmAADxQAADtiBCTQiGCWjkABQjmAAikilg");

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#014749").ss(2).p("AINAZQAADtiBCSQiGCYjkAAQjmAAikilQikijAAjoQAAjnCkikQCkikDmAAQDgAACKCxQCBCmAADxg");

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.lf(["#15EEFF","#FFFFFF","#15EEFF","#FFFFFF"],[0,0.251,0.498,0.816],-112.1,56.3,160.9,-77).s().p("AloGLQikijAAjoQAAjnCkikQCkikDmAAQDgAACKCxQCBCmAADxQAADtiBCTQiGCWjkAAQjmAAikikg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).to({state:[{t:this.shape_3},{t:this.shape_2}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-53.5,-56.9,107.1,113.9);


(lib.chk10 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#2EB2BC").ss(2).p("AHIAAQAADniGCkQiFCli9AAQi7AAiGilQiFikAAjnQAAjnCFijQCGilC7AAQC9AACFClQCGCjAADng");

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.lf(["#15EEFF","#FFFFFF","#15EEFF","#FFFFFF"],[0,0.251,0.498,0.816],-101.3,49.5,143.6,-70.1).s().p("AlBGMQiFilgBjnQABjmCFikQCFilC8AAQC8AACGClQCFCkAADmQAADniFClQiGCki8AAQi8AAiFikg");

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#014749").ss(2).p("AHIAAQAADniGClQiFCki9AAQi7AAiGikQiGilAAjnQAAjnCGikQCGikC7AAQC9AACFCkQCGCkAADng");

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.lf(["#15EEFF","#FFFFFF","#15EEFF","#FFFFFF"],[0,0.251,0.498,0.816],-101.3,49.5,143.6,-70.1).s().p("AlBGLQiFikgBjnQABjnCFikQCFikC8AAQC8AACGCkQCFCkAADnQAADniFCkQiGCki8AAQi8AAiFikg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).to({state:[{t:this.shape_3},{t:this.shape_2}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-46.5,-56.9,93.1,113.9);


(lib.chk9 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#2EB2BC").ss(2).p("AIwAAQAAjnikijQikiljoAAQjnAAikClQikCjAADnQAADoCkCjQCkClDnAAQCrAACOhgQCKhdBAiZIi4h1g");

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.lf(["#15EEFF","#FFFFFF","#15EEFF","#FFFFFF"],[0,0.251,0.498,0.816],-118.8,58.1,168.3,-82.2).s().p("AmLGMQikikAAjoQAAjmCkikQCkilDnAAQDoAACkClQCkCjAADnIjlBlIC4B2QhACYiKBdQiOBgirAAQjnAAikikg");

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#014749").ss(2).p("AIwAAQAAjnikikQikikjoAAQjnAAikCkQikCkAADnQAADoCkCkQCkCkDnAAQCrAACOhgQCKhdBAiZIi4h1g");

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.lf(["#15EEFF","#FFFFFF","#15EEFF","#FFFFFF"],[0,0.251,0.498,0.816],-118.8,58.1,168.3,-82.2).s().p("AmLGLQikijAAjoQAAjnCkikQCkikDnAAQDoAACkCkQCkCkAADnIjlBlIC4B2QhACYiKBdQiOBfirAAQjnAAikikg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).to({state:[{t:this.shape_3},{t:this.shape_2}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-56.9,-56.9,114,113.9);


(lib.chk8 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#2EB2BC").ss(2).p("AG5lXQB+CigoDVQgoDXi3CPQi3COjZgNQjagNh+iiQh+ihAojWQAojXC3iPQC3iODZANQDbAOB9Chg");

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.lf(["#15EEFF","#FFFFFF","#15EEFF","#FFFFFF"],[0,0.251,0.498,0.816],-56.7,115.1,79.5,-163.9).s().p("AhgIHQjagNh+iiQh+ihAojWQAojXC3iPQC3iODZANQDbAOB9ChQB+CigoDVQgoDXi3CPQioCCjEAAIgkgBg");

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#014749").ss(2).p("AG5lXQB+CigoDVQgoDXi3CPQi3COjZgNQjagNh+iiQh+ihAojWQAojXC3iPQC3iODZANQDbAOB9Chg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).to({state:[{t:this.shape_1},{t:this.shape_2}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-55.4,-53,110.2,107.2);


(lib.chk7 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#2EB2BC").ss(2).p("AIvg3QAADnikCkQijCkjoAAQjnAAikikQikikAAjnQAAjdCih3QCThsD2AAQD2AACXBsQCmB3AADdg");

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.lf(["#15EEFF","#FFFFFF","#15EEFF","#FFFFFF"],[0,0.251,0.498,0.816],-116.7,55.5,168.1,-83.6).s().p("AmKFUQikikAAjnQAAjdChh3QCUhsD1AAQD3AACWBsQCnB3AADdQAADnikCkQikCkjoAAQjnAAijikg");

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#014749").ss(2).p("AIwg3QAADnilCkQijCkjoAAQjnAAikikQikikAAjnQAAjdCih3QCThsD2AAQD2AACXBsQCnB3AADdg");

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.lf(["#15EEFF","#FFFFFF","#15EEFF","#FFFFFF"],[0,0.251,0.498,0.816],-116.7,55.5,168.1,-83.6).s().p("AmLFUQikikAAjnQABjdChh3QCThsD3AAQD2AACWBsQCmB3AADdQAADnikCkQijCkjoAAQjnAAikikg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).to({state:[{t:this.shape_3},{t:this.shape_2}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-56.9,-51.3,113.9,102.8);


(lib.chk6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#2EB2BC").ss(2).p("AIwAAQAADoikCjQikCljoAAQjmAAikilQikijAAjoQAAjnCkijQCkilDmAAQDoAACkClQCkCjAADng");

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.lf(["#15EEFF","#FFFFFF","#15EEFF","#FFFFFF"],[0,0.251,0.498,0.816],-118.8,58.1,168.3,-82.2).s().p("AmKGLQikijAAjoQAAjmCkikQCjilDnAAQDoAACkClQCkCkAADmQAADoikCjQikCljoAAQjnAAijilg");

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#014749").ss(2).p("AIwAAQAADoikCkQikCkjoAAQjnAAikikQikikAAjoQAAjnCkikQCkikDnAAQDoAACkCkQCkCkAADng");

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.lf(["#15EEFF","#FFFFFF","#15EEFF","#FFFFFF"],[0,0.251,0.498,0.816],-118.8,58.1,168.3,-82.2).s().p("AmLGLQikijAAjoQAAjnCkikQCkikDnAAQDoAACjCkQCkCkAADnQAADoikCjQijCkjoAAQjnAAikikg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).to({state:[{t:this.shape_3},{t:this.shape_2}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-56.9,-56.9,113.9,113.9);


(lib.chk5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#2EB2BC").ss(2).p("AIvgeQANDmibCtQibCtjnANQjnANisibQitibgNjnQgNjnCbisQCaitDogNQDmgNCtCbQCtCaANDog");

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.lf(["#15EEFF","#FFFFFF","#15EEFF","#FFFFFF"],[0,0.251,0.498,0.816],-115.4,64.6,163.4,-91.4).s().p("Al0GhQitibgNjnQgNjnCbisQCaitDogNQDmgNCtCbQCtCaANDoQANDmibCtQibCtjnANIgjABQjRAAifiPg");

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#014749").ss(2).p("AIwAAQAADoikCkQikCkjoAAQjnAAikikQikikAAjoQAAjnCkikQCkikDnAAQDoAACkCkQCkCkAADng");
	this.shape_2.setTransform(0,0.1);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.lf(["#15EEFF","#FFFFFF","#15EEFF","#FFFFFF"],[0,0.251,0.498,0.816],-118.8,58.1,168.3,-82.2).s().p("AmLGLQikijAAjoQAAjnCkikQCkikDnAAQDoAACkCkQCkCkAADnQAADoikCjQikCkjoAAQjnAAikikg");
	this.shape_3.setTransform(0,0.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).to({state:[{t:this.shape_3},{t:this.shape_2}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-56.9,-56.9,114,114);


(lib.chk4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#2EB2BC").ss(2).p("AIwAAQAADoikCjQikCljoAAQjnAAikilQikijAAjoQAAjnCkijQCkilDnAAQDoAACkClQCkCjAADng");

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.lf(["#15EEFF","#FFFFFF","#15EEFF","#FFFFFF"],[0,0.251,0.498,0.816],-118.8,58.1,168.3,-82.2).s().p("AmLGMQikikAAjoQAAjnCkijQCkilDnAAQDoAACkClQCkCkAADmQAADoikCkQikCkjoAAQjnAAikikg");

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#014749").ss(2).p("AIwAAQAADoikCkQikCkjoAAQjnAAikikQikikAAjoQAAjnCkikQCkikDnAAQDoAACkCkQCkCkAADng");

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.lf(["#15EEFF","#FFFFFF","#15EEFF","#FFFFFF"],[0,0.251,0.498,0.816],-118.8,58.1,168.3,-82.2).s().p("AmLGLQikijAAjoQAAjnCkikQCkikDnAAQDoAACkCkQCkCkAADnQAADoikCjQikCkjoAAQjnAAikikg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).to({state:[{t:this.shape_3},{t:this.shape_2}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-56.9,-56.9,114,113.9);


(lib.chk3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#2EB2BC").ss(2).p("AIvAAQAADoikCjQijCljoAAQjnAAikilQikijAAjoQAAjnCkijQCkilDnAAQDoAACjClQCkCjAADng");

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.lf(["#15EEFF","#FFFFFF","#15EEFF","#FFFFFF"],[0,0.251,0.498,0.816],-118.8,58.1,168.3,-82.2).s().p("AmKGMQikikAAjoQAAjmCkikQCjilDnAAQDoAACkClQCkCkAADmQAADoikCkQikCkjoAAQjnAAijikg");

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#014749").ss(2).p("AIwAAQAADoikCkQikCkjoAAQjnAAikikQikikAAjoQAAjnCkikQCkikDnAAQDoAACkCkQCkCkAADng");

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.lf(["#15EEFF","#FFFFFF","#15EEFF","#FFFFFF"],[0,0.251,0.498,0.816],-118.8,58.1,168.3,-82.2).s().p("AmLGLQikijAAjoQAAjnCkikQCkikDnAAQDoAACjCkQCkCkAADnQAADoikCjQijCkjoAAQjnAAikikg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).to({state:[{t:this.shape_3},{t:this.shape_2}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-56.9,-56.9,113.9,113.9);


(lib.chk2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#2EB2BC").ss(2).p("AIwAAQAADoikCkQikCkjoAAQjmAAikikQikikAAjoQAAjnCkikQCkikDmAAQDoAACkCkQCkCkAADng");

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.lf(["#15EEFF","#FFFFFF","#15EEFF","#FFFFFF"],[0,0.251,0.498,0.816],-118.8,58.1,168.3,-82.1).s().p("AmKGMQikikAAjoQAAjnCkikQCjikDnAAQDoAACkCkQCkCkAADnQAADoikCkQikCkjoAAQjnAAijikg");

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#014749").ss(2).p("AIwAAQAADoikCkQikCkjoAAQjnAAikikQikikAAjoQAAjnCkikQCkikDnAAQDnAAClCkQCkCkAADng");

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.lf(["#15EEFF","#FFFFFF","#15EEFF","#FFFFFF"],[0,0.251,0.498,0.816],-118.8,58.1,168.3,-82.1).s().p("AmLGMQikikAAjoQAAjnCkikQCkikDnAAQDnAACkCkQCkCkAADnQAADoikCkQijCkjoAAQjnAAikikg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).to({state:[{t:this.shape_3},{t:this.shape_2}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-56.9,-56.9,113.9,114);


(lib.chk1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#2EB2BC").ss(2).p("AIwAAQAADoikCkQikCkjoAAQjnAAikikQikikAAjoQAAjnCkikQCkikDnAAQDoAACkCkQCkCkAADng");

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.lf(["#15EEFF","#FFFFFF","#15EEFF","#FFFFFF"],[0,0.251,0.498,0.816],-118.8,58.1,168.3,-82.1).s().p("AmLGMQikikAAjoQAAjnCkikQCkikDnAAQDoAACkCkQCkCkAADnQAADoikCkQikCkjoAAQjnAAikikg");

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#014749").ss(2).p("AIwAAQAADoikCkQikCkjoAAQjnAAikikQikikAAjoQAAjnCkikQCkikDnAAQDoAACkCkQCkCkAADng");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).to({state:[{t:this.shape_1},{t:this.shape_2}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-56.9,-56.9,114,114);


(lib.ansview = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#0C6B60").ss(2).p("AIwAAQAADoikCkQikCkjoAAQjnAAikikQikikAAjoQAAjmCkilQClikDmAAQDoAACkCkQCkCkAADng");
	this.shape.setTransform(-75.1,-0.5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#20CDBB").s().p("AmLGMQikikAAjoQAAjmCkilQClikDmAAQDoAACkCkQCkCkAADnQAADoikCkQikCkjoAAQjnAAikikg");
	this.shape_1.setTransform(-75.1,-0.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#0C6B60").ss(2).p("AIwAAQAADoikCkQikCjjoAAQjmAAikijQikikAAjoQAAjmCkilQCkijDmAAQDoAACkCjQCkClAADmg");
	this.shape_2.setTransform(76.2,136.2);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#20CDBB").s().p("AmKGMQikikAAjoQAAjnCkijQCjikDnAAQDoAACkCkQCjCjAADnQAADoijCkQikCjjoAAQjnAAijijg");
	this.shape_3.setTransform(76.2,136.2);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#0C6B60").ss(2).p("AIwAAQAADoikCkQikCkjoAAQjnAAikikQikikAAjoQAAjnCkikQCkikDnAAQDoAACkCkQCkCkAADng");
	this.shape_4.setTransform(227.3,0.7);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#20CDBB").s().p("AmLGMQikikAAjoQAAjnCkikQCkikDnAAQDoAACkCkQCkCkAADnQAADoikCkQikCkjoAAQjnAAikikg");
	this.shape_5.setTransform(227.3,0.7);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#0C6B60").ss(2).p("AIwAAQAADoikCkQikCjjoAAQjnAAikijQikikAAjoQAAjmCkilQCkijDnAAQDoAACkCjQCkClAADmg");
	this.shape_6.setTransform(-227.2,135.2);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#20CDBB").s().p("AmLGMQikikAAjoQAAjnCkijQCkikDnAAQDoAACjCkQCkCjAADnQAADoikCkQijCkjoAAQjnAAikikg");
	this.shape_7.setTransform(-227.2,135.2);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#0C6B60").ss(2).p("AIvAAQAADoijCkQikCkjoAAQjnAAikikQijikAAjoQAAjnCjikQCkikDnAAQDoAACkCkQCjCkAADng");
	this.shape_8.setTransform(76.1,-136.1);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#20CDBB").s().p("AmKGMQikikAAjoQAAjnCkikQCjikDnAAQDoAACkCkQCjCkAADnQAADoijCkQikCkjoAAQjnAAijikg");
	this.shape_9.setTransform(76.1,-136.1);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#0C6B60").ss(2).p("AIwAAQAADoikCkQikCkjoAAQjnAAikikQikikAAjoQAAjnCkikQCkikDnAAQDoAACkCkQCkCkAADng");
	this.shape_10.setTransform(-226.2,-136.1);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#20CDBB").s().p("AmLGMQikikAAjoQAAjnCkikQClikDmAAQDoAACjCkQCkCkABDnQgBDoikCkQijCkjoAAQjnAAikikg");
	this.shape_11.setTransform(-226.2,-136.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.ansview, new cjs.Rectangle(-284.2,-193,568.5,386.2), null);


(lib.ans12 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FF7878").s().p("AkxouQBygBBwAtQBsAqBXBNQC+CpAADiQAADki+CoQhXBNhsAqQhwAshyAAg");
	this.shape.setTransform(27.4,-0.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FF7878").s().p("AmaGMQikikAAjoQAAjnCkikQCkijDnAAQDqAACxCjQCzCnAADkQAADlizCnQixCkjqgBQjnAAikijg");
	this.shape_1.setTransform(-0.6,-0.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#681818").s().p("Ak4o8QB0AABzAtQBuAsBaBPQDCCsAADoQAADpjCCsQhaBPhuAsQhzAth0AAg");
	this.shape_2.setTransform(28.1,-0.1);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#681818").s().p("AmpGaQiriqAAjwQAAjwCripQCpiqDxAAQDyAAC4CqQC6CsAADtQAADti6CtQi4CqjyAAQjxAAipiqg");
	this.shape_3.setTransform(-0.6,-0.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.ans12, new cjs.Rectangle(-60.2,-58.1,119.7,116.1), null);


(lib.ans11 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#681818").ss(2).p("AIOAZQAADtiCCTQiGCXjjAAQjoAAikikQikikAAjoQAAjnCkikQCkikDoAAQDfAACKCxQCCCmAADxg");

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FF7878").s().p("AlpGMQikikAAjoQAAjnCkikQCkikDnAAQDgAACKCxQCCCmAADxQAADtiCCTQiGCXjkAAQjnAAikikg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.ans11, new cjs.Rectangle(-53.5,-56.9,107.1,114), null);


(lib.ans10 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#681818").ss(2).p("AHIAAQAADoiGCkQiFCki9AAQi7AAiGikQiGikAAjoQAAjnCGikQCGikC7AAQC9AACFCkQCGCkAADng");
	this.shape.setTransform(0.1,0);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FF7878").s().p("AlBGMQiFikgBjoQABjnCFikQCFikC8AAQC8AACGCkQCFCkAADnQAADoiFCkQiGCki8AAQi8AAiFikg");
	this.shape_1.setTransform(0.1,0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.ans10, new cjs.Rectangle(-46.5,-57,93.1,114), null);


(lib.ans9 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#681818").ss(2).p("AIwAAQAAjnikikQilikjnAAQjnAAikCkQikCkAADnQAADoCkCkQCkCkDnAAQCrAACOhgQCKhdBBiZIi5h1g");

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FF7878").s().p("AmLGMQikikAAjoQAAjnCkikQCkikDnAAQDnAACkCkQCkCkAADnIjkBlIC5B1QhBCZiLBdQiNBgirAAQjnAAikikg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.ans9, new cjs.Rectangle(-56.9,-57,113.9,114), null);


(lib.ans8 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#681818").ss(2).p("AG5lXQB+CigoDVQgoDXi3CPQi3CPjZgOQjbgNh9iiQh+ihAojWQAojXC3iPQC3iODZANQDaAOB+Chg");
	this.shape.setTransform(0.4,0);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FF7878").s().p("AhgIHQjbgNh9iiQh+ihAojWQAojXC3iPQC3iPDZAOQDaAOB+ChQB+CigoDVQgoDXi3CPQinCCjEAAIglgBg");
	this.shape_1.setTransform(0.4,0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.ans8, new cjs.Rectangle(-55,-53.3,110.2,106.3), null);


(lib.ans7 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#681818").ss(2).p("AIwg3QAADnikCkQikCkjoAAQjnAAikikQikikAAjnQAAjdCih3QCUhsD2AAQD2AACXBsQCmB3AADdg");

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FF7878").s().p("AmLFUQikikAAjnQAAjdCih3QCUhsD2AAQD2AACXBsQCmB3AADdQAADnikCkQikCkjoAAQjnAAikikg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.ans7, new cjs.Rectangle(-56.9,-51.3,114,102.8), null);


(lib.ans6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#0C6B60").ss(2).p("AIwAAQAADoikCkQikCkjoAAQjnAAikikQikikAAjoQAAjnCkikQCkikDnAAQDoAACkCkQCkCkAADng");

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#20CDBB").s().p("AmLGLQikijAAjoQAAjnCkikQCkikDnAAQDoAACjCkQCkCkAADnQAADoikCjQijCkjoAAQjnAAikikg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.ans6, new cjs.Rectangle(-56.9,-56.9,113.9,113.9), null);


(lib.ans5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#0C6B60").ss(2).p("AIwAAQAADoikCkQikCkjoAAQjnAAikikQikikAAjoQAAjnCkikQCkikDnAAQDoAACkCkQCkCkAADng");
	this.shape.setTransform(0.1,0);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#20CDBB").s().p("AmLGLQikijAAjoQAAjnCkikQCkikDnAAQDoAACjCkQCkCkAADnQAADoikCjQijCkjoAAQjnAAikikg");
	this.shape_1.setTransform(0.1,0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.ans5, new cjs.Rectangle(-56.9,-56.9,113.9,113.9), null);


(lib.ans4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#0C6B60").ss(2).p("AIwAAQAADoikCkQikCkjoAAQjnAAikikQikikAAjoQAAjnCkikQCkikDnAAQDoAACkCkQCkCkAADng");

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#20CDBB").s().p("AmLGMQikikAAjoQAAjnCkikQCkikDnAAQDoAACkCkQCkCkAADnQAADoikCkQikCkjoAAQjnAAikikg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.ans4, new cjs.Rectangle(-56.9,-56.9,114,114), null);


(lib.ans3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#0C6B60").ss(2).p("AIwAAQAADoikCkQikCkjoAAQjnAAikikQikikAAjoQAAjmCkilQClikDmAAQDoAACkCkQCkCkAADng");

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#20CDBB").s().p("AmLGMQikikAAjoQAAjmCkilQClikDmAAQDoAACkCkQCkCkAADnQAADoikCkQikCkjoAAQjnAAikikg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.ans3, new cjs.Rectangle(-56.9,-57,114,114), null);


(lib.ans2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#0C6B60").ss(2).p("AIwAAQAADoikCkQikCkjoAAQjnAAikikQikikAAjoQAAjnCkikQCkikDnAAQDoAACkCkQCkCkAADng");

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#20CDBB").s().p("AmLGMQikikAAjoQAAjnCkikQCkikDnAAQDoAACjCkQCkCkAADnQAADoikCkQijCkjoAAQjnAAikikg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.ans2, new cjs.Rectangle(-56.9,-57,113.9,114), null);


(lib.ans1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#0C6B60").ss(2).p("AIwAAQAADoikCkQikCkjoAAQjnAAikikQikikAAjoQAAjnCkikQCkikDnAAQDoAACkCkQCkCkAADng");

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#20CDBB").s().p("AmLGMQikikAAjoQAAjnCkikQCkikDnAAQDoAACjCkQCkCkAADnQAADoikCkQijCkjoAAQjnAAikikg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.ans1, new cjs.Rectangle(-56.9,-57,113.9,114), null);


(lib.playbtn = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgXA4QgZgBgNgNQgOgNAAgYIABgOIAKAAQAAAJAMAAQAQAAAHgSQACgIgDgIQgDgKgIAAQgGAAgEAFQgDAEAAAGIABAHIgNAAQgCgLAEgIQAHgOAQAAQAKAAAGAFQAGAGABAJQAOgUAaAAQAdABAQAYQAJAPAAARQgBAggcAPQgMAGgVABIAAgLQAUgBAOgKQAOgLABgVQAAgPgJgNQgKgOgPgDIgIgBQgWAAgMAVIAFAAQALAAAKAJQANALAAASQAAAHgEAIQgEAJgGAFQgMAMgVAAIgCAAgAg6AbQAGAJAKAFQAHADANAAQAOABAJgIQALgJABgNQAAgLgHgHQgGgJgKAAIgGAAQgDABgDAFQgJALgKAEQgFACgGAAQgJAAgCgEQAAALAFAJg");
	this.shape.setTransform(28.1,-1.5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgFAaIgGgDIAAgjQAAgFgEgBQgDgCgEACQgEACgCADIAAgMQAGgGAIABQAGAAAFAFQAEgGAKABQAIAAAFAFQAFAFAAAJQABAMgKAFQgGADgGAAQgGgBgCgBIAAAYIgFgFgAAIgUIgFACQgDACAAADIAAALQAEACAFAAQAJgBgBgKQgBgJgHAAIgBAAg");
	this.shape_1.setTransform(16.7,-9);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgXA4QgZgBgNgNQgOgNAAgYIABgOIAKAAQAAAJAMAAQAQAAAHgSQACgIgDgIQgDgKgIAAQgGAAgEAFQgDAEAAAGIABAHIgNAAQgCgLAEgIQAHgOAQAAQAKAAAGAFQAGAGABAJQAOgUAaAAQAdABAQAYQAJAPAAARQgBAggcAPQgMAGgVABIAAgLQAUgBAOgKQAOgLABgVQAAgPgJgNQgKgOgPgDIgIgBQgWAAgMAVIAFAAQALAAAKAJQANALAAASQAAAHgEAIQgEAJgGAFQgMAMgVAAIgCAAgAg6AbQAGAJAKAFQAHADANAAQAOABAJgIQALgJABgNQAAgLgHgHQgGgJgKAAIgGAAQgDABgDAFQgJALgKAEQgFACgGAAQgJAAgCgEQAAALAFAJg");
	this.shape_2.setTransform(11.8,-1.5);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgZBLQgWgBgLgTQgJgQAAgXQAAgTAHgPQAKgUASABQAQABAAAOIgBAIQgCAGABACQABAOARAAQAHAAAFgEQAFgEABgGQgFAFgIAAQgHAAgFgFQgGgEAAgIQAAgTAUAAQALAAAGAJQAGAIAAALQAAANgJAHQgJAIgNAAQgdAAgBgaQgBgCACgFQABgFAAgCQgBgFgFAAQgHABgFAIQgHAOAAAVQAAAQAHANQAJAQAOAAQAJAAAAgJQAAgDgGgEQgHgFAAgGQAAgQAcAAQAcAAAAARQAAAFgHAEQgHAEAAAFQAAAIALAAQARAAAIgUQAFgOAAgVQAAgcgNgUQgPgXgbAAQgiAAgNAWIgOAAQARghAsAAQAQAAANAHQAPAGAIANQAQAZAAAeQAAAsgTAQQgJAIgMAAQgbAAAAgVQAAgHAEgDIAGgEQABgBABAAQAAgBABAAQAAgBABAAQAAAAAAgBQAAgCgHgDIgIgBQgPAAAAAGQAAACAEACIAGAEQADAEAAAHQAAAUgWAAIgBAAgAAGgaQgGAAAAAHQAAADABACQADACADAAQAIgBAAgGQAAgDgDgCQgCgCgDAAIgBAAg");
	this.shape_3.setTransform(-3.7,-3.5);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AgqA5QgQgPAAgZQAAgPAHgLIAJgMQADgEAQgKIATgLQAPgKAGgQIAOAAQgFAVgWANIAKABIAKADQAQAGAJAOQAKAOAAAQQAAAagRAPQgQAQgaAAQgZAAgRgQgAgggOQgNANgBASQAAAUAOAMQAOAMATAAQATAAANgMQANgNAAgTQAAgSgNgNQgOgNgTAAIAAAAQgSAAgOANg");
	this.shape_4.setTransform(-17.5,-3.3);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AARBKIAAgkIgUAAQgngCgNgUQgGgJAAgMQAAgSAMgLQALgKAUAAIAWAAQAAgSgTAAQgYAAABAUIgNAAQABgfAiAAQAdAAAEAdIACgPQABgFAHgFQAFgEAMAAIAPAAIAAALQgIgCgBACIgBACIAGALQAHAKAAAGQAAAJgHAGQgGAFgJABIgLAAIAAAlQAJgBAHgDIAOgHIAAANQgQAIgOACIAAAlgAgngbQgJAIAAAOQAAALAHAIQAIAIAMADQARADAVgBIAAg9IghAAQgOAAgJAHgAAdgwIAAAZQAUABAAgMQAAgFgIgLQgEgGAAgGQgIACAAAMg");
	this.shape_5.setTransform(-30.7,0.2);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#FFFFFF").ss(3).p("ALADzI1/AAQgoAAgegdQgegdAAgqIAAkdQAAgpAegeQAegdAoAAIV/AAQApAAAdAdQAeAeAAApIAAEdQAAAqgeAdQgdAdgpAAg");
	this.shape_6.setTransform(0,-2.1);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#F48500").s().p("Aq+DzQgpAAgegdQgdgdAAgqIAAkdQAAgpAdgeQAegdApAAIV9AAQAqAAAdAdQAeAegBApIAAEdQABAqgeAdQgdAdgqAAg");
	this.shape_7.setTransform(0,-2.1);

	this.instance = new lib.Path_2();
	this.instance.parent = this;
	this.instance.setTransform(0.7,5.9,1,1,0,0,0,81.9,24.3);
	this.instance.alpha = 0.301;

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-81.9,-27.9,164.5,58.8);


(lib.face3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.rf(["#442914","#3D230F","#2B1302","#281100"],[0.129,0.467,0.945,1],0,0,0,0,0,16.4).s().p("AjRAVQgIAAgGgGQgGgHAAgIQAAgIAGgGQAGgGAIAAIGiAAQAJAAAGAGQAGAGAAAIQAAAIgGAHQgGAGgJAAg");
	this.shape.setTransform(-43.2,15);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(0.6).p("Agcg0IgBABQgDAEAEADQAjAmALAdQAGAPgCAIQgBAEAEACQAEADACgFQACgJgGgRQgMghgmgpQgCgDgDABg");
	this.shape_1.setTransform(1.5,-54.4);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFF00").s().p("AAZA0QgEgCABgEQACgIgGgPQgLgdgjgmQgEgDADgEIABgBQADgBACADQAmApAMAhQAGARgCAJQgBABAAAAQAAABgBAAQAAABAAAAQgBAAAAAAIgDgBg");
	this.shape_2.setTransform(1.5,-54.4);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#000000").ss(0.6).p("AAxB6IAoggQArgmAMggQANgihHg6QgcgWgUgIQgVgHAEAQQgZgjg7AFQg5AEgTAfQgcAwBuANQAVADAKADQASAFAKALQAOANADAXQACANABAcQABARAFARQAIAZANgJg");
	this.shape_3.setTransform(-3.3,-49.8);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFF00").s().p("AAcBqQgFgRgBgRQgBgcgCgNQgDgXgOgNQgKgLgSgFQgKgDgVgDQhugNAcgwQATgfA5gEQA7gFAZAjQgEgQAVAHQAUAIAcAWQBHA6gNAiQgMAggrAmIgoAgQgEADgDAAQgIAAgGgTg");
	this.shape_4.setTransform(-3.3,-49.8);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#632E16").s().p("ABHC8QgJAAgIgHQg0gvgohEQhRiGAzhuIADgEQAKgKAQAKQAIAFAEAJQADAJgDAHQgrBcBEBwQAhA5AvArQAIAHACAJQACAJgFAGQgFAFgIAAIgBAAg");
	this.shape_5.setTransform(10.9,-30.4);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#3E2723").s().p("AgUBEQgKABgHgIQgIgHABgKIAAhXQgBgKAIgHQAHgIAKABIApAAQAKgBAHAIQAIAHgBAKIAABXQABAKgIAHQgHAIgKgBg");
	this.shape_6.setTransform(13.2,-13.6);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#3E2723").s().p("AgSBBQgKABgIgIQgHgHAAgKIAAhRQAAgKAHgIQAIgGAKAAIAlAAQAKAAAIAGQAHAIAAAKIAABRQAAAKgHAHQgIAIgKgBg");
	this.shape_7.setTransform(-100.6,-13.2);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#000000").ss(0.6).p("AhehiIAsgYQAxgXAgABQAkABAZBVQAJAggBAUQgBAVgNgKQAVAkgaAzQgZAxgjAEQg0AHAehlQALgngJgVQgGgSgTgLQgXgMgMgGQgPgHgNgLQgTgRAMgHg");
	this.shape_8.setTransform(-102.8,22.9);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFF00").s().p("AgBAzQALgngJgVQgGgSgTgLIgjgSQgPgHgNgLQgTgRAMgHIAsgYQAxgXAgABQAkABAZBVQAJAggBAUQgBAVgNgKQAVAkgaAzQgZAxgjAEIgGAAQgsABAchfg");
	this.shape_9.setTransform(-102.8,22.9);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#632E16").s().p("AAjDEIgFgDQhjhLAJibQADgxANgzIANgrQAEgJAIgEQAHgFAIADQAGAEADAJQADAIgEAKQgHAUgEATQgMAsgCApQgHCBBSA/QAHAFABAJQABAKgFAIQgEAHgHAEQgEACgEAAIgEgBg");
	this.shape_10.setTransform(-104.9,2.4);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.rf(["#442914","#3D230F","#2B1302","#281100"],[0.129,0.467,0.945,1],0,0,0,0,0,5.3).s().p("AglAmQgQgQAAgWQAAgVAQgQQAQgPAVAAQAWAAAQAPQAPAQAAAVQAAAWgPAQQgQAQgWAAQgVAAgQgQg");
	this.shape_11.setTransform(-26.5,-20.5);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.rf(["#442914","#3D230F","#2B1302","#281100"],[0.129,0.467,0.945,1],0,0,0,0,0,5.3).s().p("AglAmQgQgQAAgWQAAgVAQgQQAQgPAVAAQAWAAAQAPQAPAQAAAVQAAAWgPAQQgQAQgWAAQgVAAgQgQg");
	this.shape_12.setTransform(-59.9,-20.5);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#FFFFFF").p("AIWAAQAADeicCcQicCdjeAAQjcAAididQidicAAjeQAAjdCdicQCcidDdAAQDeAACcCdQCcCcAADdg");
	this.shape_13.setTransform(-43.2,-8.8);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.lf(["#FFD712","#F6911E"],[0,1],0,-53.8,0,41.7).s().p("Al5F6QicicgBjeQABjcCcidQCdicDcgBQDeABCcCcQCdCdgBDcQABDeidCcQicCdjegBQjcABididg");
	this.shape_14.setTransform(-43.2,-8.8);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.lf(["#C4E034","#748D16"],[0,1],-216.6,0,216.7,0).s().p("A3vJYIiRivIn1CvIKGywMA5lAAAIAASwg");
	this.shape_15.setTransform(4.5,-8.8);

	this.instance = new lib.Path_12_1();
	this.instance.parent = this;
	this.instance.setTransform(0.1,8.9,1,1,0,0,0,221.7,60);
	this.instance.alpha = 0.141;

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.face3, new cjs.Rectangle(-221.6,-68.8,443.3,137.8), null);


(lib.face2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#3E2723").s().p("AgSBBQgKAAgIgGQgGgIAAgKIAAhRQAAgKAGgHQAIgIAKABIAlAAQAKgBAHAIQAIAHgBAKIAABRQABAKgIAIQgHAGgKAAg");
	this.shape.setTransform(13.6,-15.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(0.6).p("ABdhhQgSgLgZgMQgwgWgfABQgkABgXBTQgJAfAAAUQABAUANgJQgUAjAZAyQAYAvAiAFQAzAHgdhkQgEgTgBgJQgCgSAFgMQAGgSASgLQALgGAYgLQAPgHAMgLQATgQgMgIg");
	this.shape_1.setTransform(15.8,20.9);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFF00").s().p("AgUCOQgigFgYgvQgZgyAUgjQgNAJgBgUQAAgUAJgfQAXhTAkgBQAfgBAwAWQAZAMASALQAMAIgTAQQgMALgPAHQgYALgLAGQgSALgGASQgFAMACASQABAJAEATQAbBdgqAAIgHAAg");
	this.shape_2.setTransform(15.8,20.9);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#632E16").s().p("AgtC/QgHgEgEgHQgFgIACgJQABgKAGgEQBRg+gHh+QgCgpgMgqQgGgZgFgOQgDgJACgJQADgIAGgEQAHgDAIAFQAIAEADAJIANAqQANAyADAwQAICYhgBJIgFADIgFABQgDAAgEgCg");
	this.shape_3.setTransform(17.9,0.8);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#3E2723").s().p("AgSBBQgKABgIgIQgHgHAAgKIAAhRQAAgKAHgIQAIgGAKAAIAlAAQAKAAAIAGQAHAIAAAKIAABRQAAAKgHAHQgIAIgKgBg");
	this.shape_4.setTransform(-101,-14.4);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(0.6).p("AhehjIAsgXQAxgYAgABQAkACAZBUQAJAhgBAUQgBAUgNgJQAVAkgaAzQgZAxgjAEQgzAHAdhmQAEgSACgLQACgRgGgNQgGgSgTgMQgXgLgMgGQgQgJgMgJQgTgRAMgIg");
	this.shape_5.setTransform(-103.2,21.7);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFF00").s().p("AgBAyQAEgSACgLQACgRgGgNQgGgSgTgMIgjgRQgQgJgMgJQgTgRAMgIIAsgXQAxgYAgABQAkACAZBUQAJAhgBAUQgBAUgNgJQAVAkgaAzQgZAxgjAEIgGABQgsAAAchgg");
	this.shape_6.setTransform(-103.2,21.7);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#632E16").s().p("AAjDEIgFgDQhjhLAJibQADgxANgzIANgrQAEgJAIgEQAHgFAIAEQAGADADAJQADAIgEAKIgLAnQgMAsgCApQgHCBBSA/QAHAFABAJQABAKgFAJQgEAHgHADQgEACgEAAIgEgBg");
	this.shape_7.setTransform(-105.4,1.2);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.rf(["#442914","#3D230F","#2B1302","#281100"],[0.129,0.467,0.945,1],0,0,0,0,0,5.3).s().p("AglAmQgQgQAAgWQAAgVAQgQQAQgQAVAAQAWAAAQAQQAPAQAAAVQAAAWgPAQQgQAPgWAAQgVAAgQgPg");
	this.shape_8.setTransform(-27,-20.5);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.rf(["#442914","#3D230F","#2B1302","#281100"],[0.129,0.467,0.945,1],0,0,0,0,0,5.3).s().p("AglAmQgQgQAAgWQAAgVAQgQQAQgQAVAAQAWAAAQAQQAPAQAAAVQAAAWgPAQQgQAPgWAAQgVAAgQgPg");
	this.shape_9.setTransform(-60.4,-20.5);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.rf(["#442914","#3D230F","#2B1302","#281100"],[0.129,0.467,0.945,1],0,0,0,0,0,22.4).s().p("AivA6QhQg0gohWQgDgIADgIQACgHAIgEQAJgEAIADQAHADAEAIQAiBMBHAsQBGAuBSAAQBTAABHguQBGgsAihMQAEgIAIgDQAIgDAHAEQAIAEAEAHQACAIgDAIQgnBWhRA0QhRA0hfAAQheAAhRg0g");
	this.shape_10.setTransform(-43.7,14.5);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#FFFFFF").p("AIXAAQAADeidCcQicCdjeAAQjcAAididQicicAAjeQAAjcCcidQCdidDcAAQDeAACcCdQCdCcAADdg");
	this.shape_11.setTransform(-43.7,-8.8);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.lf(["#FFD712","#F6911E"],[0,1],0,-53.7,0,41.8).s().p("Al5F6QicidgBjdQABjcCcidQCdidDcABQDegBCcCdQCdCcgBDdQABDdidCdQicCcjeABQjcgBidicg");
	this.shape_12.setTransform(-43.7,-8.8);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.lf(["#C4E034","#748D16"],[0,1],-216.6,0,216.7,0).s().p("A3vJYIiRivIn2CvIKHyvMA5mAAAIAASvg");
	this.shape_13.setTransform(4.5,-8.8);

	this.instance = new lib.Path_11_1();
	this.instance.parent = this;
	this.instance.setTransform(0.1,8.8,1,1,0,0,0,221.7,60);
	this.instance.alpha = 0.141;

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.face2, new cjs.Rectangle(-221.6,-68.8,443.3,137.7), null);


(lib.face = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#3E2723").s().p("AgSBBQgKABgIgIQgGgHAAgKIAAhRQAAgKAGgIQAIgGAKAAIAlAAQAKAAAHAGQAIAIgBAKIAABRQABAKgIAHQgHAIgKgBg");
	this.shape.setTransform(77.7,-16.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(0.6).p("ABfhiQgTgMgYgMQgygXggABQgkABgYBVQgKAgABAUQABAVANgKQgVAkAaAzQAZAxAjAEQAzAHgdhlQgEgSgBgLQgCgSAFgNQAGgSATgLQALgHAYgKQARgKALgJQATgRgMgHg");
	this.shape_1.setTransform(79.8,20);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFF00").s().p("AgUCRQgjgEgZgxQgagzAVgkQgNAKgBgVQgBgUAKggQAYhVAkgBQAggBAyAXQAYAMATAMQAMAHgTARQgLAJgRAKQgYAKgLAHQgTALgGASQgFANACASQABALAEASQAbBfgrgBIgGAAg");
	this.shape_2.setTransform(79.8,20);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#632E16").s().p("AguDDQgHgEgEgHQgFgIABgKQABgJAHgFQBSg/gHiBQgCgpgLgsQgHgXgFgQQgDgKACgIQADgJAGgEQAIgDAHAFQAIAEAEAJIANArQANAzADAxQAJCchjBKIgFADIgFABQgDAAgEgCg");
	this.shape_3.setTransform(82,-0.5);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#3E2723").s().p("AgSBCQgKAAgIgIQgHgHAAgKIAAhRQAAgKAHgIQAIgGAKgBIAlAAQAKABAIAGQAHAIAAAKIAABRQAAAKgHAHQgIAIgKAAg");
	this.shape_4.setTransform(-37,-15);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#000000").ss(0.6).p("AhehjIAsgXQAxgYAgABQAkACAZBUQAJAhgBAUQgBAUgNgJQAVAkgaAzQgZAxgjAEQgzAHAdhmQAEgSACgLQACgRgGgNQgGgSgTgMQgXgLgMgGQgQgJgMgJQgTgRAMgIg");
	this.shape_5.setTransform(-39.2,21.2);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFF00").s().p("AgBAyQAEgSACgLQACgRgGgNQgGgSgTgMIgjgRQgQgJgMgJQgTgRAMgIIAsgXQAxgYAgABQAkACAZBUQAJAhgBAUQgBAUgNgJQAVAkgaAzQgZAxgjAEIgGABQgsAAAchgg");
	this.shape_6.setTransform(-39.2,21.2);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#632E16").s().p("AAjDEIgFgDQhjhLAJibQADgxANgzIANgrQAEgJAIgEQAHgFAIADQAGAEADAJQADAIgEAKIgLAnQgMAsgCApQgHCBBSA/QAHAFABAJQABAKgFAJQgEAHgHADQgEACgEAAIgEgBg");
	this.shape_7.setTransform(-41.3,0.6);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.rf(["#442914","#3D230F","#2B1302","#281100"],[0.129,0.467,0.945,1],0,0,0,0,0,5.3).s().p("AglAmQgQgQAAgWQAAgVAQgQQAQgPAVgBQAWABAQAPQAPAQAAAVQAAAWgPAQQgQAPgWAAQgVAAgQgPg");
	this.shape_8.setTransform(37.1,-18.3);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.rf(["#442914","#3D230F","#2B1302","#281100"],[0.129,0.467,0.945,1],0,0,0,0,0,5.3).s().p("AglAmQgQgQAAgWQAAgVAQgQQAQgPAVgBQAWABAQAPQAPAQAAAVQAAAWgPAQQgQAPgWAAQgVAAgQgPg");
	this.shape_9.setTransform(3.7,-18.3);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.rf(["#442914","#3D230F","#2B1302","#281100"],[0.129,0.467,0.945,1],0,0,0,0,0,22.4).s().p("AEOBtQgIgDgEgIQgihMhGgsQhHguhTAAQhSAAhGAuQhHAsgiBMQgEAIgHADQgIADgJgEQgIgDgCgIQgDgIADgIQAohWBQg0QBRg0BeAAQBfAABRA0QBRA0AnBWQADAIgCAIQgEAIgIADQgEACgEAAIgHgBg");
	this.shape_10.setTransform(20.4,11.7);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#FFFFFF").p("AIXAAQAADdidCdQicCdjeAAQjcAAididQicidAAjdQAAjdCcicQCdidDcAAQDeAACcCdQCdCcAADdg");
	this.shape_11.setTransform(20.4,-6.6);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.lf(["#FFD712","#F6911E"],[0,1],0,-53.8,0,41.7).s().p("Al5F6QicidgBjdQABjdCcicQCdidDcAAQDeAACcCdQCdCcgBDdQABDdidCdQicCcjeABQjcgBidicg");
	this.shape_12.setTransform(20.4,-6.6);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.lf(["#C4E034","#748D16"],[0,1],-216.6,0,216.7,0).s().p("A3vJYIiRivIn2CvIKHyvMA5mAAAIAASvg");
	this.shape_13.setTransform(68.6,-6.8);

	this.instance = new lib.Path_11_2();
	this.instance.parent = this;
	this.instance.setTransform(64.1,10.8,1,1,0,0,0,221.7,60);
	this.instance.alpha = 0.141;

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.face, new cjs.Rectangle(-157.6,-66.8,443.3,137.7), null);


(lib.enterans = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_30 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(30).call(this.frame_30).wait(1));

	// Layer 1
	this.instance = new lib.Symbol1();
	this.instance.parent = this;
	this.instance.setTransform(0,7.7,1,1,0,0,0,101.1,26.7);
	this.instance.alpha = 0;

	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AAQAoIAAgPQAKAAAGgGQAGgIAAgKQAAgKgGgIQgGgHgKgBQgLABgHAIQAJACAGAHQAGAHAAAJQAAAPgLAIQgJAIgQAAQglAAAAgoIAAgHIAOAAIAAACQAAADAEAAQADAAADgCIAFgEIACgFIABgDQAAgDgCgDQAAAAgBgBQAAAAgBgBQAAAAgBAAQAAgBgBAAQgGAAAAAJIgPAAIgBgEQAAgIAGgGQAGgFAJgBQAHAAAFAEQAFADABAHQAKgOASAAQAQAAALAMQAKALABARQAAAQgJAMQgJAMgSAAIgDAAgAgPAYQAGgBAFgDQAEgDABgFIAAgDQAAgIgEgBIgFgCQgBAAgBAAQAAAAgBAAQAAABgBAAQAAAAgBABIgJAIQgGADgFAAIgFgBQAEAPATgBg");
	this.shape.setTransform(44.9,-7.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgKAUIAAgZQAAgFgGAAQgGAAgDAFIAAgNQADgDAEgCQAEgCAEAAQAHAAADAHQACgHAKAAQAGAAAEAFQADAFAAAGQAAAMgMADQgGABgEgCIAAAVQgGgFgHgBgAADgKIAAAHIADABQAFAAgBgGQAAAAAAgBQAAAAAAgBQAAAAAAgBQgBAAAAAAQAAgBgBAAQAAAAAAgBQgBAAAAAAQgBAAAAAAQgBAAAAAAQAAAAgBABQAAAAAAAAQgBABAAABg");
	this.shape_1.setTransform(36.3,-13.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AAQAoIAAgPQAKAAAGgGQAGgIAAgKQAAgKgGgIQgGgHgKgBQgLABgHAIQAJACAGAHQAGAHAAAJQAAAPgLAIQgJAIgQAAQglAAAAgoIAAgHIAOAAIAAACQAAABAAAAQABABAAAAQABABAAAAQABAAABAAQADAAADgCIAFgEIACgFIABgDQAAgDgCgDQgBgDgDAAQgGAAAAAJIgPAAIgBgEQAAgIAGgGQAGgFAJgBQAHAAAFAEQAFADABAHQAKgOASAAQAQAAALAMQAKALABARQAAAQgJAMQgJAMgSAAIgDAAgAgPAYQAGgBAFgDQAEgDABgFIAAgDQAAgIgEgBIgFgCQgBAAgBAAQAAAAgBAAQAAABgBAAQAAAAgBABIgJAIQgGADgFAAIgFgBQAEAPATgBg");
	this.shape_2.setTransform(33.2,-7.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AABAgQgFAIgQAAQgPAAgHgLQgIgLABgSQABgQALgMQALgMAQAAIAFABIAAAOQgLgBgIAHQgHAIgBALQgBAJADAHQAEAIAIAAQAMAAAAgLIAAgHIAOAAIAAAHQAAAFAEADQAEADAGAAQAGAAADgEQADgEgBgGQAAgNgTAAIgSAAIAAgIQAAgbAYgCQAWAAACAVIgQAAQAAgGgHAAQgEAAgCACQgDACAAAEIAIAAQANAAAJAIQAIAIAAAMQAAANgHAIQgHAIgKAAIgCABQgSAAgFgJg");
	this.shape_3.setTransform(22.1,-7.7);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AgiAhQgPgJAAgQQAAgKAIgIQAJgHANAAIAMAAQAAgIgLAAQgMAAABAHIgQAAIAAgDQgBgJAJgFQAHgFAKAAQAPAAAGAIQADgEAGgCQAEgCAKAAQAMAAAFAHQAEAEAAAHIAAAEIgSAAQAAgBAAAAQAAgBAAgBQAAAAAAAAQAAgBAAAAQgCgDgGAAQgLAAAAAIIALAAQAOAAAGAFQALAJAAAMQAAAQgPAIQgOAIgVAAQgVAAgNgIgAgbAAQgEACgBAFQAAAJAKAFQAJAEANAAQAOAAAJgEQAJgFAAgIQAAgGgEgCQgFgDgGAAIghAAQgHAAgEADg");
	this.shape_4.setTransform(11.5,-7.7);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AgSAoQgPAAgIgIQgHgIAAgOQABgJAHgEIgMAAIAAgNIAvAAQAAgJgLAAQgKAAAAAHIgQAAQgBgKAIgGQAHgFAKAAIAMABQAGACACAEQAEgFAHgBIANgBQAIAAAGAGQAGAGAAAIIgRAAQABgGgKAAQgKAAAAAJIAOAAQAdAAAAAcQAAANgIAHQgJAIgMAAQgRAAgFgJQgEAJgPAAIgBAAgAAKAHIAAAHQAAAKAOAAQAFAAAEgEQADgFAAgGQAAgGgFgDQgFgDgHAAIgiAAQgGAAgFADQgFAEAAAGQAAAGAEAEQADAEAGAAQAGAAAEgDQADgCAAgFIAAgHg");
	this.shape_5.setTransform(0.6,-7.8);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AgQAgQgJgGABgRQAAgLAJgLQAHgIAIgDIgZAAIAAgOIAwAAIAAANQgNAIgHAGQgIAKgBAJIABAKQACAGAFgBQAEAAACgDQACgDAAgDIAAgEIAQAAIAAAHQAAAKgGAGQgHAFgJABQgNgBgHgGg");
	this.shape_6.setTransform(-13.3,-7.5);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AgUAyQgJgEgGgHQgJgMAAgOQAAgHACgGQAGgSAVgIIAJgFIAKgJQAEgEAEgJIATAAQgCAIgEAGQgHAJgIAEIAGABQAOADAIAMQAHAKAAANQAAAKgEAJQgFAKgMAGQgLAGgNAAQgKAAgKgEgAgUgDQgGAGAAAJQAAALAIAHQAHAGAMAAQAOAAAHgJQAFgHAAgIQAAgLgIgHQgHgGgMAAQgNAAgHAJg");
	this.shape_7.setTransform(-20.9,-9);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AgSAPQgHgGAAgJQgCgSARgCIAAAOQAAAGADADQAEAEAFAAQAEAAADgDQACgDAAgCIAAgEIAPAAIABAFQAAAIgIAHQgHAGgKAAQgNAAgHgGg");
	this.shape_8.setTransform(-28.1,-3.1);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AgmAlQgKgEgCgJQAAgCABgDQABgBAAgBQAAgBABAAQAAgBAAAAQAAgBAAAAIgCgBIgEACIgCADIAAgOIAOgOIANgLIAAgCQAAAAAAgBQAAAAAAAAQAAgBgBAAQAAAAAAAAIgEgBQgEABAAAFIABAEIgQAAIAAgFQAAgIAGgFQAFgFAIAAQAOgBADALQAPgKASAAQAQAAALAKQAMAMAAATQAAARgKAKQgKALgRAAIgEgBIAAgNQALAAAGgGQAHgHAAgLQAAgMgHgHQgHgHgNAAQgHAAgFACQAIADAHAIQAGAIAAAKQAAAPgMAJQgKAJgQAAQgMAAgJgDgAgZgEIgLAJQACACACADIAAACIAAACIgBADQAAAEAFACQAEACAFAAQAIAAAHgEQAHgFAAgHQABgIgEgFQgFgFgHAAIgDAAQgEAAgGAFg");
	this.shape_9.setTransform(-31.6,-7.7);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AAZAOQABgHgJgEQgHgDgKAAQgJAAgHADQgJADAAAGIABACIgRAAIgBgGQAAgKAOgGQANgFAPAAQARAAAMAEQAOAHAAAJIgCAHg");
	this.shape_10.setTransform(-42.9,-13);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("AggApQgOgLgCgTIgBgGQAAgWAOgHQAAgMAHgHQAHgHALAAQAKAAAFAHQAGgIAMABQAKAAAHAKQAFAKAAAMIAAADIg5AAIgHABQABAKAHAFQAHAGAKAAQAPAAAGgOIARAAQgEANgKAIQgLAIgPAAQgMAAgKgGQgKgHgEgKIAAADQAAAPAKAKQAKAJAOAAQALAAAIgFQAJgFADgKIARAAQgCARgOAJQgNAKgTAAQgUAAgOgLgAALgcIASAAQgCgJgHAAQgHAAgCAJgAgTgcIAQAAQgBgJgHAAQgHAAgBAJg");
	this.shape_11.setTransform(-42.6,-6.6);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("AAZAOQABgHgJgEQgHgDgKAAQgJAAgHADQgJADAAAGIABACIgRAAIgBgGQAAgKAOgGQANgFAPAAQARAAAMAEQAOAHAAAJIgCAHg");
	this.shape_12.setTransform(-53.3,-13);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("AgiAhQgPgJAAgQQAAgKAIgIQAJgHANAAIAMAAQAAgIgLAAQgMAAACAHIgRAAIAAgDQgBgJAJgFQAHgFALAAQAOAAAGAIQAEgEAFgCQAEgCAKAAQAMAAAFAHQAEAEAAAHIAAAEIgSAAQAAgBAAAAQAAgBAAgBQAAAAAAAAQAAgBAAAAQgCgDgGAAQgLAAAAAIIALAAQAOAAAGAFQALAJAAAMQAAAQgPAIQgOAIgVAAQgWAAgMgIgAgbAAQgEACgBAFQAAAJAKAFQAJAEANAAQAOAAAJgEQAJgFAAgIQAAgGgEgCQgFgDgGAAIghAAQgGAAgFADg");
	this.shape_13.setTransform(-53.2,-7.7);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#F44336").s().p("Ai4hxIFxAAIi5Djg");
	this.shape_14.setTransform(0,15.4);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#F44336").s().p("As2C8QhNABg4g4Qg3g4AAhNQAAhNA3g3QA4g4BNAAIZtAAQBOAAA3A4QA3A3AABNQAABNg3A4Qg3A4hOgBg");
	this.shape_15.setTransform(0,-7.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance}]},1).to({state:[{t:this.instance}]},1).to({state:[{t:this.instance}]},1).to({state:[{t:this.instance}]},1).to({state:[{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]},1).to({state:[{t:this.instance}]},21).to({state:[{t:this.instance}]},1).to({state:[{t:this.instance}]},1).to({state:[{t:this.instance}]},1).to({state:[{t:this.instance}]},1).wait(1));
	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1).to({y:6.9,alpha:0.25},0).wait(1).to({y:6.1,alpha:0.5},0).wait(1).to({y:5.3,alpha:0.75},0).wait(1).to({y:4.4,alpha:1},0).to({_off:true},1).wait(21).to({_off:false},0).wait(1).to({y:5.3,alpha:0.75},0).wait(1).to({y:6.1,alpha:0.5},0).wait(1).to({y:6.9,alpha:0.25},0).wait(1).to({y:7.7,alpha:0},0).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-101.1,-19,202.3,53.4);


(lib.confirm = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgXA4QgZgBgNgNQgOgNAAgYIABgOIAKAAQAAAJAMAAQAQAAAHgSQACgIgDgIQgDgKgIAAQgGAAgEAFQgDAEAAAGIABAHIgNAAQgCgLAEgIQAHgOAQAAQAKAAAGAFQAGAGABAJQAOgUAaAAQAdABAQAYQAJAPAAARQgBAggcAPQgMAGgVABIAAgLQAUgBAOgKQAOgLABgVQAAgPgJgNQgKgOgPgDIgIgBQgWAAgMAVIAFAAQALAAAKAJQANALAAASQAAAHgEAIQgEAJgGAFQgMAMgVAAIgCAAgAg6AbQAGAJAKAFQAHADANAAQAOABAJgIQALgJABgNQAAgLgHgHQgGgJgKAAIgGAAQgDABgDAFQgJALgKAEQgFACgGAAQgJAAgCgEQAAALAFAJg");
	this.shape.setTransform(60.5,-1.5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgFAaIgGgDIAAgjQAAgFgEgBQgDgCgEACQgEACgCADIAAgMQAGgGAIABQAGAAAFAFQAEgGAKABQAIAAAFAFQAFAFAAAJQABAMgKAFQgGADgGAAQgGgBgCgBIAAAYIgFgFgAAIgUIgFACQgDACAAADIAAALQAEACAFAAQAJgBgBgKQgBgJgHAAIgBAAg");
	this.shape_1.setTransform(49.1,-9);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgXA4QgZgBgNgNQgOgNAAgYIABgOIAKAAQAAAJAMAAQAQAAAHgSQACgIgDgIQgDgKgIAAQgGAAgEAFQgDAEAAAGIABAHIgNAAQgCgLAEgIQAHgOAQAAQAKAAAGAFQAGAGABAJQAOgUAaAAQAdABAQAYQAJAPAAARQgBAggcAPQgMAGgVABIAAgLQAUgBAOgKQAOgLABgVQAAgPgJgNQgKgOgPgDIgIgBQgWAAgMAVIAFAAQALAAAKAJQANALAAASQAAAHgEAIQgEAJgGAFQgMAMgVAAIgCAAgAg6AbQAGAJAKAFQAHADANAAQAOABAJgIQALgJABgNQAAgLgHgHQgGgJgKAAIgGAAQgDABgDAFQgJALgKAEQgFACgGAAQgJAAgCgEQAAALAFAJg");
	this.shape_2.setTransform(44.2,-1.5);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgqA5QgQgPAAgZQAAgPAHgLIAJgMQADgEAQgKIATgLQAPgKAGgQIAOAAQgFAVgWANIAKABIAKADQAQAGAJAOQAKAOAAAQQAAAagRAPQgQAQgaAAQgZAAgRgQgAgggOQgNANgBASQAAAUAOAMQAOAMATAAQATAAANgMQANgNAAgTQAAgSgNgNQgOgNgTAAIAAAAQgSAAgOANg");
	this.shape_3.setTransform(29.5,-3.3);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AAZA4IAAgLQATABAMgNQAMgOgBgSQgCgSgNgNQgOgNgTAAQgTAAgNAIQApAGAEAqQABAQgHANQgIAOgPAAQgUAAgEgMQgDAMgWAAQgKAAgHgHQgHgGAAgKQgBgDACgEIABgHQAAAAAAgBQAAgBgBAAQAAAAgBAAQgBgBgBAAQgCAAgFAFIAAgNQAIgKAMgLIARgSQADgCAAgEQAAgHgKABQgFAAgEAEQgDAEAAAGIABAFIgNAAIgBgGQAAgKAHgHQAGgHAKgBQAVgBADAQQATgOAagBQAagBASASQASASAAAZQgBAWgNAOQgNAPgWAAIgGAAgAgOANIAAAMQAAAUAQgBQAIgBAFgJQAEgIAAgJQAAgQgJgLQgJgMgOgDIgGgBQgLAAgGAHIgbAaQAIABAAAKIgBAFIgBAEIAAAEQAAANANAAQAJAAAEgFQAFgFAAgJIAAgMg");
	this.shape_4.setTransform(14.5,-1.6);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AgfAfQgCgFAAgIQAAgJADgGQAIgWAXgRQAGgEAEgBIguAAIAAgLIBDAAIAAAKQgSAKgIAHQgHAGgIAJQgLAOAAAOQAAALAGAGQAHAGAJAAQATAAACgRQABgKgEgIIANAAQABACABAGIABAJQgDAeggAAQgXAAgJgWg");
	this.shape_5.setTransform(-5.8,-1.3);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AgqA5QgQgPAAgZQAAgPAHgLIAJgMQADgEAQgKIATgLQAPgKAGgQIAOAAQgFAVgWANIAKABIAKADQAQAGAJAOQAKAOAAAQQAAAagRAPQgQAQgaAAQgZAAgRgQgAgggOQgNANgBASQAAAUAOAMQAOAMATAAQATAAANgMQANgNAAgTQAAgSgNgNQgOgNgTAAIAAAAQgSAAgOANg");
	this.shape_6.setTransform(-16.2,-3.3);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AgsAnQgRAAAAgTQAAgLAGgEIAOAAQgHAGAAAIQAAAJAKAAIBXAAIAAgwQAJgIAEgKIAABNg");
	this.shape_7.setTransform(-30.2,4.1);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AgrA/QgVgMgBgZQABgVARgJQAMgFAOAAIAPAAQgBgIgEgFQgFgFgJAAQgJAAgEAFQgGAEAAAJIgNAAQAAgOAKgIQAJgHANAAQAgAAAAAmIgZAAQgOAAgJAGQgLAHABAOQABATAQAJQAOAGAUAAQAZAAAOgPQANgPAAgaQACgZgPgUQgSgWgYABQgRABgJAEQgLAFgIALIgOAAQAOggAsAAQAgAAATAWQASAVABAgQgBAggQASQgRAUggAAQgZAAgSgKg");
	this.shape_8.setTransform(-29.9,-3.4);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AASAtQASAAAMgOQAMgNABgSQAAgTgNgOQgIgKgMgBQgPgBgLAJQgDAEgDAHQgCAHAAAGQAEgDAGABIAEABIAIAFQAJAIABANQABATgOAMQgNAMgTAAQgSAAgNgHQgQgIgFgPQgCgJABgJQABgSAOgKQAJgGARAAIgBgJQgDgJgJAAQgJAAgBAMQgBAEABADIgNAAIAAgHIABgHQAGgPARgBQANAAAHAKQAEAFABAPIgBAJIgKAAQgLAAgFACQgHADgEAIQgEAGAAAJQAAAQAMAJQALAHARAAQAMAAAIgGQANgIAAgQQAAgHgDgFQgEgFgGAAQgIAAABAJIgMAAIAAgHIABgPQABgLAEgIQANgWAdACQAVACAPAVQAJANAAATQAAAKgFAMQgFANgHAGQgRAPgWAAg");
	this.shape_9.setTransform(-45,-1.6);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AARAtQAeAAAMgTQAGgLAAgNQAAgSgNgNQgNgOgTAAQgUAAgQALQAQgBAMAJQASAOAAAVQgBAVgQANQgNALgWAAQgSAAgKgEQgPgFgDgPIgBgEIABgGIABgFQAAAAAAgBQAAAAAAgBQAAAAgBAAQAAAAAAAAQgFAAgDAFIAAgOIAUgVIATgSIAAgCQAAgIgKAAQgJAAgCAMQgBAEACAFIgNAAQgBgIACgIQACgIAIgEQAGgFAIAAQARABADAOQAVgPAYAAQAZABAQAQQAQAQAAAYQAAANgEAMQgGANgKAGQgPAKgZAAgAgTgWQgIAAgFADIgLAIIgRAQQAGACAAAHIgBAGQgBAEAAADQADASAfAAQAQAAAKgJQAMgJAAgPQAAgPgKgJQgJgKgPAAIgBAAg");
	this.shape_10.setTransform(-61.2,-1.6);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#FFFFFF").ss(3).p("ALADzI1/AAQgoAAgegdQgegdAAgqIAAkdQAAgpAegeQAegdAoAAIV/AAQApAAAdAdQAeAeAAApIAAEdQAAAqgeAdQgdAdgpAAg");
	this.shape_11.setTransform(0,-2.1);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#EF6C00").s().p("Aq+DzQgpAAgegdQgdgdAAgqIAAkdQAAgpAdgeQAegdApAAIV9AAQAqAAAdAdQAeAegBApIAAEdQABAqgeAdQgdAdgqAAg");
	this.shape_12.setTransform(0,-2.1);

	this.instance = new lib.Path_1();
	this.instance.parent = this;
	this.instance.setTransform(0.7,5.9,1,1,0,0,0,81.9,24.3);
	this.instance.alpha = 0.301;

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-81.9,-27.9,164.5,58.8);


(lib.answer = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgfAfQgCgFAAgIQAAgJACgGQAJgWAYgRQAFgEAFgBIgvAAIAAgLIBDAAIAAAKQgSAKgIAHQgIAGgGAJQgMAOAAAOQAAALAGAGQAGAGALAAQARAAADgRQACgKgFgIIANAAQACACAAAGIABAJQgDAegfAAQgZAAgIgWg");
	this.shape.setTransform(64.5,-0.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgqA5QgQgPAAgZQAAgPAHgLIAJgMQADgEAQgKIATgLQAPgKAGgQIAOAAQgFAVgWANIAKABIAKADQAQAGAJAOQAKAOAAAQQAAAagRAPQgQAQgaAAQgZAAgRgQgAgggOQgNANgBASQAAAUAOAMQAOAMATAAQATAAANgMQANgNAAgTQAAgSgNgNQgOgNgTAAIAAAAQgSAAgOANg");
	this.shape_1.setTransform(54.1,-2.2);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgggCQAAgOAKgEIAAAJIAGAAQgCADAAAGQAAAFAHAEQAFADAGAAQAHAAAGgEQAGgDABgGQABgGgDgEIANAAQACAGgBAGQAAALgKAGQgJAFgNAAQggAAAAgXg");
	this.shape_2.setTransform(43.9,6.2);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AARAtQAeAAAMgTQAGgLgBgNQAAgSgMgNQgNgOgTAAQgUAAgQALQAQgBAMAJQASAOgBAVQAAAVgPANQgOALgVAAQgTAAgKgEQgOgFgFgPIAAgEIABgGIACgFQAAAAgBgBQAAAAAAgBQAAAAgBAAQAAAAgBAAQgDAAgFAFIAAgOIAVgVIAUgSIAAgCQAAgIgKAAQgLAAgBAMQgBAEACAFIgNAAQgBgIACgIQADgIAGgEQAHgFAIAAQARABADAOQAVgPAXAAQAZABARAQQARAQAAAYQgBANgFAMQgFANgKAGQgPAKgZAAgAgTgWQgHAAgHADIgLAIIgRAQQAIACgBAHIgBAGQgBAEABADQACASAgAAQAQAAAKgJQALgJABgPQAAgPgLgJQgJgKgPAAIgBAAg");
	this.shape_3.setTransform(39.2,-0.5);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AAiAWIAAgKIACAAQAIAAAAgHQAAgFgEgCQgMgIgcAAQgaAAgNAIQgEACAAAFQAAAHAIAAIACAAIAAAKIgFABQgIAAgFgFQgGgFAAgIQAAgbA5AAQA6AAAAAbQAAAIgFAFQgGAFgIAAIgFgBg");
	this.shape_4.setTransform(23.3,-7.9);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AgxA0QgTgUABggQACgaARgMQAAgGADgHQAHgTAXgBQAHAAAHAEQAFADACAHQAEgOATAAQAOAAAIAJQAIAKAAAOIAAAGIhPAAQgLAAgEACQgBAGABAKQADARAOAKQAIAGAPAAQAXAAAMgRQAHgIgBgOIANAAQAAAXgPAOQgPAOgZAAQgLAAgIgDQgWgIgIgVQgDgKABgLQgGAIgBANQgCAgAXASQAOALAWAAQATAAAMgHQAUgNABgTIANAAQAAAZgVAOQgSAMgaAAQggAAgUgUgAARg2QgEAEAAAIIAhAAQgBgRgRAAQgHAAgEAFgAgfg0QgBACgCAIIAHAAIAbAAQAAgHgEgFQgEgFgHAAQgLAAgFAHg");
	this.shape_5.setTransform(24,1.1);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AAiAWIAAgKIACAAQAIAAAAgHQAAgFgEgCQgMgIgcAAQgaAAgNAIQgEACAAAFQAAAHAIAAIACAAIAAAKIgFABQgIAAgFgFQgGgFAAgIQAAgbA5AAQA6AAAAAbQAAAIgFAFQgGAFgIAAIgFgBg");
	this.shape_6.setTransform(9.1,-7.9);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AgnAxQgagMAAgcQAAgQAKgKQAKgKARAAIAOAAQAAgRgOAAQgPgBgBANQAAACABADIgNAAIAAgEQAAgZAcAAQAJAAAIAFQAHAFACAJQABAGAAAJIAAAFIgWAAQgKAAgFADQgJADgEALQgBAEAAAGQAAAMAJAJQAOAOAdAAQAeAAAOgOQAJgJAAgMQAAgGgBgEQgEgLgJgDQgFgDgKAAIgWAAIAAgFQAAgVAKgIQAIgFAJAAQAcAAAAAZIAAAEIgNAAQABgDAAgCQgBgNgPABQgOAAAAARIAOAAIALABQANADAHAKQAGAKAAAMQAAAcgaAMQgQAIgYAAQgXAAgQgIg");
	this.shape_7.setTransform(9.3,-0.4);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AAiAWIAAgKIACAAQAIAAAAgHQAAgFgEgCQgMgIgcAAQgaAAgNAIQgEACAAAFQAAAHAIAAIACAAIAAAKIgFABQgIAAgFgFQgGgFAAgIQAAgbA5AAQA6AAAAAbQAAAIgFAFQgGAFgIAAIgFgBg");
	this.shape_8.setTransform(-13.3,-7.9);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AAABIQgIgCgGgFQgHgGAAgHQAAgLAIgDQgNgBgGgCQgIgDgIgHQgLgLAAgQQAAgOAIgKIAJgJIAHgEQAIgEATAAIAjAAQgFgKgHgFQgHgDgKAAQgZAAgGAPIgBADIgNAAQAAgHAHgIQAGgIAHgDIAMgCIANgBQAlAAAGAiIAAAFIgyAAIgMABQgGABgGAFQgOAKABAOQAAAIABADQADAFAIAFQALAJAVAAQAOgBAJgBQAPgFAAgGIAOAAQgBAFgDADQgKAMgXADIgIABQgMADAAAHQAAANAWAAQAMgBAGgDIAKgHQADgCABgFQABgEgBgDIANAAQACASgQAKQgMAIgQAAQgNAAgFgBg");
	this.shape_9.setTransform(-11.3,1.2);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AgqA5QgQgPAAgZQAAgPAHgLIAJgMQADgEAQgKIATgLQAPgKAGgQIAOAAQgFAVgWANIAKABIAKADQAQAGAJAOQAKAOAAAQQAAAagRAPQgQAQgaAAQgZAAgRgQgAgggOQgNANgBASQAAAUAOAMQAOAMATAAQATAAANgMQANgNAAgTQAAgSgNgNQgOgNgTAAIAAAAQgSAAgOANg");
	this.shape_10.setTransform(-24.3,-2.2);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("AgfAfQgCgFAAgIQAAgJACgGQAJgWAYgRQAFgEAEgBIguAAIAAgLIBDAAIAAAKQgSAKgIAHQgIAGgGAJQgMAOAAAOQAAALAGAGQAHAGAJAAQASAAADgRQACgKgFgIIANAAQABACABAGIABAJQgDAegfAAQgZAAgIgWg");
	this.shape_11.setTransform(-34.9,-0.2);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("AgqA/QgXgMAAgZQABgVARgJQAMgFAOAAIAPAAQgBgIgFgFQgEgFgJAAQgJAAgFAFQgFAEAAAJIgNAAQAAgOAJgIQAKgHANAAQAgAAAAAmIgZAAQgOAAgJAGQgLAHABAOQABATARAJQANAGAUAAQAZAAAOgPQANgPAAgaQABgZgOgUQgSgWgYABQgRABgJAEQgMAFgHALIgOAAQAOggAsAAQAgAAATAWQASAVABAgQAAAggRASQgRAUggAAQgaAAgQgKg");
	this.shape_12.setTransform(-46,-2.3);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("AAiAWIAAgKIACAAQAIAAAAgHQAAgFgEgCQgMgIgcAAQgaAAgNAIQgEACAAAFQAAAHAIAAIACAAIAAAKIgFABQgIAAgFgFQgGgFAAgIQAAgbA5AAQA6AAAAAbQAAAIgFAFQgGAFgIAAIgFgBg");
	this.shape_13.setTransform(-60.7,-7.9);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FFFFFF").s().p("AgXA4QgZgBgNgNQgOgNAAgYIABgOIAKAAQAAAJAMAAQAQAAAHgSQACgIgDgIQgDgKgIABQgGgBgEAFQgDAEAAAGIABAHIgNAAQgCgLAEgIQAHgOAQAAQAKAAAGAFQAGAGABAKQAOgWAaABQAdABAQAYQAJAPAAARQgBAggcAPQgMAGgVABIAAgLQAUgBAOgKQAOgMABgUQAAgPgJgNQgKgOgPgDIgIAAQgWgBgMAVIAFAAQALAAAKAJQANALAAASQAAAHgEAIQgEAJgGAFQgMAMgVAAIgCAAgAg6AbQAGAJAKAFQAHADANAAQAOABAJgIQALgJABgNQAAgLgHgHQgGgJgKAAIgGAAQgDABgDAFQgJALgKAFQgFABgGAAQgJAAgCgEQAAALAFAJg");
	this.shape_14.setTransform(-61.3,-0.4);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#FFFFFF").ss(3).p("AK/DzI19AAQgpAAgegdQgdgdAAgqIAAkdQAAgpAdgeQAegdApAAIV9AAQAqAAAdAdQAdAeAAApIAAEdQAAAqgdAdQgdAdgqAAg");
	this.shape_15.setTransform(-0.1,-1);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#F4511E").s().p("Aq+DzQgpAAgegdQgdgdAAgqIAAkdQAAgpAdgeQAegdApAAIV9AAQAqAAAdAdQAdAeAAApIAAEdQAAAqgdAdQgdAdgqAAg");
	this.shape_16.setTransform(-0.1,-1);

	this.instance = new lib.Path();
	this.instance.parent = this;
	this.instance.setTransform(0.7,7,1,1,0,0,0,82,24.3);
	this.instance.alpha = 0.301;

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-81.9,-26.8,164.5,58.8);


// stage content:
(lib.slide_11 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		//import com.greensock.*;
		//import com.greensock.easing.*;
		
		
		var count = 12;
		var clip = this;
		
		this.enterans.visible = false;
		//correct.visible = false;
		this.ansview.visible = false;
		
		
		this.answer.mouseEnabled = false;
		this.answer.visible=false;
		
		this.confirm.visible=false;
		this.confirm.mouseEnabled=false;
		
		this.face.visible=false;
		this.face2.visible=false;
		this.face3.visible=false;
		
		
		
		
		//this.box.visible=false;
		//console.log("aa")
		
		
		for (var i = 1; i <= count; i++) {
			
			var hidd = clip["hid"+i];
			hidd.visible = false;
			
			//hidd.mouseEnabled=false;
			
			
			var chkk = clip["chk"+i];
			chkk.mouseEnabled=false;
			
			
			var anss = clip["ans"+i];
			anss.visible = false;
			
			
		}
		
		
		for (var n = 1; n <= count; n++) {
		
			//for chk button
			//var b = getChildByName("chk" + n)
			//b.addEventListener(MouseEvent.CLICK, PlayMC);
			var b = clip["chk"+n];
			
			//trace(b);
			b.addEventListener("click", chku.bind(this));
			
			var h = clip["hid"+n];
			h.addEventListener("click", hidu.bind(this));
			
			//for hid buttons
			//var h = getChildByName("hid" + n)
			//h.addEventListener(MouseEvent.CLICK, PlayMC2);
		
		
		
		}
		
		//var yes = []; //Define the array switchyes
		//var no = []; //Define the array switchyes
		
		for (var i=1; i<=count; i++){
			var cchk = clip["chk"+i];
			cchk.name = i;
		
			var hhid = clip["hid"+i];
			hhid.name = i;
			
			
			//yes[i-1] = clip["yes"+i];
			//yes[i-1].addEventListener("click", yes_clk.bind(this));
			
			//no[i-1] = clip["no"+i];
			//no[i-1].addEventListener("click", no_clk.bind(this));
		}
		
		var crntT;
		var chk = 0;
		
		this.getC = function() {
		
		
			var asign = chk;
			//alert(asign);
			
			return asign;
			
			
		}
		
		
		function chku(evt) {
		
			
			 var clickss = createjs.Sound.play("clicks"); 
			console.log("algito");
			
			crntT = evt.currentTarget.name;
			
		
			//trace(crntT)
			clip["hid"+crntT].visible = true;
		    
			chk = chk + 1;
			this.getC();
		
		}
		
		var crntT2;
		
		function hidu(evt) {
		
		    var clickss = createjs.Sound.play("clicks"); 
			console.log("algito");
			
			crntT2 = evt.currentTarget.name;
		
			//trace(crntT2)
			clip["hid"+crntT2].visible = false;
		
			chk = chk - 1;
			this.getC();
		
		}
		
		
		
		this.playbtn.addEventListener("click", playb.bind(this));
		function playb() {
			
			this.timer.gotoAndPlay(0);
			
			for (var i = 1; i <= count; i++) {
			
			
			var chkk = clip["chk"+i];
			chkk.mouseEnabled=true;
				
			this.confirm.visible=true;
			this.confirm.mouseEnabled=true;
			this.playbtn.visible=false;
			
			//console.log("algito");
		    //var count = createjs.Sound.play("csound");
			var clickss = createjs.Sound.play("clicks"); 
			console.log("algito");
				
			var csound = createjs.Sound.play("ssound"); 	
			csound.on("complete", audComplete.bind(this));
		
		
		
		function audComplete(event) {
			//main.play_btn.addEventListener("click", getaudio);
			//alert("aaaaa");
			var csound = createjs.Sound.play("ssound");
			csound.on("complete", audComplete.bind(this));
		}
		
			
		}
		
		
		}
		
		
		
		
		this.confirm.addEventListener("click", confirm_btn.bind(this));
		function confirm_btn() {
			//trace(chk)
			if (chk > 0) {
				this.confirm.mouseEnabled = false;
				this.confirm.visible=false;
				this.answer.visible=true;
				this.answer.mouseEnabled = true;
		
				for (i = 1; i <= count; i++) {
					//var chkk = this["chk" + i];
					//chkk.mouseEnabled = false;
					var chkk = clip["chk"+i];
					chkk.mouseEnabled = false;
					
				}
				
				this.timer.stop();
				
				
				var clickss = createjs.Sound.play("clicks"); 
			    console.log("algito");
				
				
				var csound = createjs.Sound.stop("ssound"); 
				//this.confirm2.visible=true;
				
		       // trace(chkk)
				
				/*
				var chkkk = 0;
				for (i = 1; i <= 3; i++) {
					if(clip["hid"+i].visible == true)
					{
						chkkk = chkkk +1;
					}
				}
				*/
				
				
				
				//trace(chkkk)
				//var score  = 0;
				/*if(chkkk == 3)
				{
					//txt1.text = "100";
					score = 100;
				}else if (chkkk == 2)
				{
					//txt1.text = "75";
					score = 66;
				}else if (chkkk == 1)
				{
					//txt1.text = "33";
					score = 33;
				}
				
				if(hid4.visible == true && hid5.visible==true)
				{
					score = score - 66;
					//txt1.text =score;
					
				}
				
				else if(hid4.visible == true )
				{
					score = score - 33;
					//txt1.text =score;
					
				}
				else if(hid5.visible == true )
				{
					score = score - 33;
					//txt1.text =score;
					
				}
				
				
				
				/*
				if (hid1.visible == true && hid2.visible == true && hid4.visible == true) {
					//correct.visible = true;
					//correct.gotoAndStop(1);
					score = 100;
				} else {
		
					if (hid1.visible == true) {
						//correct.visible = true;
						//correct.gotoAndStop(1);
						score = 33;
					}
		
					if (hid2.visible == true) {
						//correct.visible = true;
						//correct.gotoAndStop(1);
						score = 33;
					}
		
		
					if (hid4.visible == true) {
						//correct.visible = true;
						//correct.gotoAndStop(1);
						score = 33;
					}
		
		
					if (hid1.visible == true && hid2.visible == true) {
						//correct.visible = true;
						//correct.gotoAndStop(1);
						score = 66;
					}
		
		
					if (hid2.visible == true && hid4.visible == true) {
						//correct.visible = true;
						//correct.gotoAndStop(1);
						score = 66;
					}
		
					if (hid1.visible == true && hid4.visible == true) {
						//correct.visible = true;
						//correct.gotoAndStop(1);
						score = 66;
					}
		
		
		
				}
		
		
		
		
		
		
				if (hid3.visible == true) {
					score = score - 33;
				}
		*/
		    
				for (i = 1; i <= count; i++) {
					if (clip["hid"+i].visible == true) {
						clip["ans"+i].visible = true;
					}
		
				}
				
		      for (i = 7; i <= count; i++){
			  if (this.hid1.visible==true && this.hid2.visible==true && this.hid3.visible==true && this.hid4.visible==true && this.hid5.visible==true && this.hid6.visible==true && clip["hid"+i].visible != true ) {
						
						this.face2.visible=true;
						var claps = createjs.Sound.play("clap");
				        this.answer.mouseEnabled=false;
				        
					}else{
						
						this.face3.visible=true;
						
						}
		
					}		
				
		        /*
				if (score > 0) {
					txt13.text = score;
					star.play();
				} else {
					txt13.text = "0";
					wrong1.play();
				}
				
				if(score==100){
					th.visible = true;
				    th.gotoAndPlay(0);
				}
				else if (score<100 && score>50){
					tw.visible = true;
				    tw.gotoAndPlay(0);
				}
				else if (score<50 && score>0){
					one.visible = true;
				    one.gotoAndPlay(0);
				}else{
					th.visible=false;
		            tw.visible=false;
		            one.visible=false;
				}*/
		
		
			} else {
				this.enterans.visible = true;
				this.enterans.gotoAndPlay(0);
				
			}
		    
			
		}
		
		
		
		
		
		this.answer.addEventListener("click", answer_btn.bind(this));
		function answer_btn() {
			
			//this.answer2.visible=true;
			this.answer.mouseEnabled = false;
			for (var i = 1; i <= count; i++) {
				//var ans = this["ans"+i];
				//ans.visible = false;
				var hid = clip["hid"+i];
				hid.visible = false;
		
			}
			/*
			for (i = 1; i <= count; i++) {
					var chkk = clip["chk"+i];
					chkk.visible = false;
				}
			
			*/
			var clickss = createjs.Sound.play("clicks"); 
			console.log("algito");
		
			for (i = 1; i <= count; i++) {
				if (clip["ans"+i].visible == true) {
					clip["ans"+i].visible = false;
				}
		
			}
		
		
		    this.face.visible=false;
			this.face2.visible=false;
			this.face3.visible=false;
			this.ansview.visible = true;
			
		}
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		this.validate_fun = function() {
		
			//trace(chk)
			if (chk > 0) {
				this.confirm.mouseEnabled = false;
				this.confirm.visible=false;
				this.answer.visible=true;
				this.answer.mouseEnabled = true;
		
				for (i = 1; i <= count; i++) {
					//var chkk = this["chk" + i];
					//chkk.mouseEnabled = false;
					var chkk = clip["chk"+i];
					chkk.mouseEnabled = false;
					
				}
				
				this.timer.stop();
				
				
				var clickss = createjs.Sound.play("clicks"); 
			    console.log("algito");
				
				
				var csound = createjs.Sound.stop("ssound"); 
				//this.confirm2.visible=true;
				
		       // trace(chkk)
				
				/*
				var chkkk = 0;
				for (i = 1; i <= 3; i++) {
					if(clip["hid"+i].visible == true)
					{
						chkkk = chkkk +1;
					}
				}
				*/
				
				
				
				//trace(chkkk)
				//var score  = 0;
				/*if(chkkk == 3)
				{
					//txt1.text = "100";
					score = 100;
				}else if (chkkk == 2)
				{
					//txt1.text = "75";
					score = 66;
				}else if (chkkk == 1)
				{
					//txt1.text = "33";
					score = 33;
				}
				
				if(hid4.visible == true && hid5.visible==true)
				{
					score = score - 66;
					//txt1.text =score;
					
				}
				
				else if(hid4.visible == true )
				{
					score = score - 33;
					//txt1.text =score;
					
				}
				else if(hid5.visible == true )
				{
					score = score - 33;
					//txt1.text =score;
					
				}
				
				
				
				/*
				if (hid1.visible == true && hid2.visible == true && hid4.visible == true) {
					//correct.visible = true;
					//correct.gotoAndStop(1);
					score = 100;
				} else {
		
					if (hid1.visible == true) {
						//correct.visible = true;
						//correct.gotoAndStop(1);
						score = 33;
					}
		
					if (hid2.visible == true) {
						//correct.visible = true;
						//correct.gotoAndStop(1);
						score = 33;
					}
		
		
					if (hid4.visible == true) {
						//correct.visible = true;
						//correct.gotoAndStop(1);
						score = 33;
					}
		
		
					if (hid1.visible == true && hid2.visible == true) {
						//correct.visible = true;
						//correct.gotoAndStop(1);
						score = 66;
					}
		
		
					if (hid2.visible == true && hid4.visible == true) {
						//correct.visible = true;
						//correct.gotoAndStop(1);
						score = 66;
					}
		
					if (hid1.visible == true && hid4.visible == true) {
						//correct.visible = true;
						//correct.gotoAndStop(1);
						score = 66;
					}
		
		
		
				}
		
		
		
		
		
		
				if (hid3.visible == true) {
					score = score - 33;
				}
		*/
		    
				for (i = 1; i <= count; i++) {
					if (clip["hid"+i].visible == true) {
						clip["ans"+i].visible = true;
					}
		
				}
				
		      for (i = 7; i <= count; i++){
			  if (this.hid1.visible==true && this.hid2.visible==true && this.hid3.visible==true && this.hid4.visible==true && this.hid5.visible==true && this.hid6.visible==true && clip["hid"+i].visible != true ) {
						
						this.face2.visible=true;
						var claps = createjs.Sound.play("clap");
				        this.answer.mouseEnabled=false;
				        
					}
					
					
					
					else{
						
						this.face3.visible=true;
						
						}
		
					}		
				
		        /*
				if (score > 0) {
					txt13.text = score;
					star.play();
				} else {
					txt13.text = "0";
					wrong1.play();
				}
				
				if(score==100){
					th.visible = true;
				    th.gotoAndPlay(0);
				}
				else if (score<100 && score>50){
					tw.visible = true;
				    tw.gotoAndPlay(0);
				}
				else if (score<50 && score>0){
					one.visible = true;
				    one.gotoAndPlay(0);
				}else{
					th.visible=false;
		            tw.visible=false;
		            one.visible=false;
				}*/
		
		
			} 
			
			
			
			
			else {
				
				console.log("algito00009")
				//this.output.text="0";
			var csound = createjs.Sound.stop("ssound"); 
			this.face.visible=true;
			this.confirm.visible=false;
			this.confirm.mouseEnabled=false;
			this.answer.visible=true;
			this.answer.mouseEnabled=true;
			//alert("yes");
			//this.getStage().getChildAt(0).timer.output.text="0";
			this.timer.gotoAndStop(375);
			
			/*	
			for(var i=1 ; i<=12;i++){
		      
		        this.getStage().getChildAt(0)["hid"+i].visible=false;
				//this.getStage().getChildAt(0)["drop"+selected].visible= false;
				
			}
			*/
			
			
			
			
			for (i = 1; i <= count; i++) {
					//var chkk = this["chk" + i];
					//chkk.mouseEnabled = false;
					var chkk = clip["chk"+i];
					chkk.mouseEnabled = false;
					
				}
				
			}
		}
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(1));

	// face3
	this.face3 = new lib.face3();
	this.face3.parent = this;
	this.face3.setTransform(1060.3,371);

	this.timeline.addTween(cjs.Tween.get(this.face3).wait(1));

	// face2
	this.face2 = new lib.face2();
	this.face2.parent = this;
	this.face2.setTransform(1058.4,371);

	this.timeline.addTween(cjs.Tween.get(this.face2).wait(1));

	// face
	this.face = new lib.face();
	this.face.parent = this;
	this.face.setTransform(995.5,369);

	this.timeline.addTween(cjs.Tween.get(this.face).wait(1));

	// ansview
	this.ansview = new lib.ansview();
	this.ansview.parent = this;
	this.ansview.setTransform(419,435);

	this.timeline.addTween(cjs.Tween.get(this.ansview).wait(1));

	// enterans
	this.enterans = new lib.enterans();
	this.enterans.parent = this;
	this.enterans.setTransform(998,516.1);

	this.timeline.addTween(cjs.Tween.get(this.enterans).wait(1));

	// playbtn
	this.playbtn = new lib.playbtn();
	this.playbtn.parent = this;
	this.playbtn.setTransform(1000,570.9,1,1,0,0,0,-0.1,1.9);
	new cjs.ButtonHelper(this.playbtn, 0, 1, 1);

	this.timeline.addTween(cjs.Tween.get(this.playbtn).wait(1));

	// confirmbtn
	this.confirm = new lib.confirm();
	this.confirm.parent = this;
	this.confirm.setTransform(1000,570.9,1,1,0,0,0,-0.1,1.9);
	new cjs.ButtonHelper(this.confirm, 0, 1, 1);

	this.timeline.addTween(cjs.Tween.get(this.confirm).wait(1));

	// answerbtn
	this.answer = new lib.answer();
	this.answer.parent = this;
	this.answer.setTransform(1000,570.9,1,1,0,0,0,-0.1,1.9);
	new cjs.ButtonHelper(this.answer, 0, 1, 1);

	this.timeline.addTween(cjs.Tween.get(this.answer).wait(1));

	// timer
	this.timer = new lib.timer();
	this.timer.parent = this;
	this.timer.setTransform(707.5,-46.4);

	this.timeline.addTween(cjs.Tween.get(this.timer).wait(1));

	// ans
	this.ans12 = new lib.ans12();
	this.ans12.parent = this;
	this.ans12.setTransform(648.5,570.3);

	this.ans11 = new lib.ans11();
	this.ans11.parent = this;
	this.ans11.setTransform(340.9,571.3);

	this.ans10 = new lib.ans10();
	this.ans10.parent = this;
	this.ans10.setTransform(491,435);

	this.ans9 = new lib.ans9();
	this.ans9.parent = this;
	this.ans9.setTransform(193.1,435);

	this.ans8 = new lib.ans8();
	this.ans8.parent = this;
	this.ans8.setTransform(646.8,303);

	this.ans7 = new lib.ans7();
	this.ans7.parent = this;
	this.ans7.setTransform(344.3,304.3);

	this.ans6 = new lib.ans6();
	this.ans6.parent = this;
	this.ans6.setTransform(495.5,571);

	this.ans5 = new lib.ans5();
	this.ans5.parent = this;
	this.ans5.setTransform(192.1,570.3);

	this.ans4 = new lib.ans4();
	this.ans4.parent = this;
	this.ans4.setTransform(646.8,436);

	this.ans3 = new lib.ans3();
	this.ans3.parent = this;
	this.ans3.setTransform(344.3,434);

	this.ans2 = new lib.ans2();
	this.ans2.parent = this;
	this.ans2.setTransform(495.6,298.8);

	this.ans1 = new lib.ans1();
	this.ans1.parent = this;
	this.ans1.setTransform(193.1,298.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.ans1},{t:this.ans2},{t:this.ans3},{t:this.ans4},{t:this.ans5},{t:this.ans6},{t:this.ans7},{t:this.ans8},{t:this.ans9},{t:this.ans10},{t:this.ans11},{t:this.ans12}]}).wait(1));

	// hid
	this.hid12 = new lib.hid12();
	this.hid12.parent = this;
	this.hid12.setTransform(649.6,571.4);
	new cjs.ButtonHelper(this.hid12, 0, 1, 1);

	this.hid11 = new lib.hid11();
	this.hid11.parent = this;
	this.hid11.setTransform(341.3,572);
	new cjs.ButtonHelper(this.hid11, 0, 1, 1);

	this.hid10 = new lib.hid10();
	this.hid10.parent = this;
	this.hid10.setTransform(491,434.8);
	new cjs.ButtonHelper(this.hid10, 0, 1, 1);

	this.hid9 = new lib.hid9();
	this.hid9.parent = this;
	this.hid9.setTransform(193.1,434.8);
	new cjs.ButtonHelper(this.hid9, 0, 1, 1);

	this.hid8 = new lib.hid8();
	this.hid8.parent = this;
	this.hid8.setTransform(646.3,303.1,1,1,0,0,0,-0.4,0);
	new cjs.ButtonHelper(this.hid8, 0, 1, 1);

	this.hid7 = new lib.hid7();
	this.hid7.parent = this;
	this.hid7.setTransform(344.4,304.3);
	new cjs.ButtonHelper(this.hid7, 0, 1, 1);

	this.hid6 = new lib.hid6();
	this.hid6.parent = this;
	this.hid6.setTransform(495.6,571.4);
	new cjs.ButtonHelper(this.hid6, 0, 1, 1);

	this.hid5 = new lib.hid5();
	this.hid5.parent = this;
	this.hid5.setTransform(192.1,570.4);
	new cjs.ButtonHelper(this.hid5, 0, 1, 1);

	this.hid4 = new lib.hid4();
	this.hid4.parent = this;
	this.hid4.setTransform(646.7,435.9);
	new cjs.ButtonHelper(this.hid4, 0, 1, 1);

	this.hid3 = new lib.hid3();
	this.hid3.parent = this;
	this.hid3.setTransform(344.3,434.8);
	new cjs.ButtonHelper(this.hid3, 0, 1, 1);

	this.hid2 = new lib.hid2();
	this.hid2.parent = this;
	this.hid2.setTransform(495.5,299.1);
	new cjs.ButtonHelper(this.hid2, 0, 1, 1);

	this.hid1 = new lib.hid1();
	this.hid1.parent = this;
	this.hid1.setTransform(193.1,299.1);
	new cjs.ButtonHelper(this.hid1, 0, 1, 1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.hid1},{t:this.hid2},{t:this.hid3},{t:this.hid4},{t:this.hid5},{t:this.hid6},{t:this.hid7},{t:this.hid8},{t:this.hid9},{t:this.hid10},{t:this.hid11},{t:this.hid12}]}).wait(1));

	// chk
	this.chk12 = new lib.chk12();
	this.chk12.parent = this;
	this.chk12.setTransform(650.3,571.3);
	new cjs.ButtonHelper(this.chk12, 0, 1, 1);

	this.chk11 = new lib.chk11();
	this.chk11.parent = this;
	this.chk11.setTransform(341.8,571.3);
	new cjs.ButtonHelper(this.chk11, 0, 1, 1);

	this.chk10 = new lib.chk10();
	this.chk10.parent = this;
	this.chk10.setTransform(491.9,435.1);
	new cjs.ButtonHelper(this.chk10, 0, 1, 1);

	this.chk9 = new lib.chk9();
	this.chk9.parent = this;
	this.chk9.setTransform(194,435.1);
	new cjs.ButtonHelper(this.chk9, 0, 1, 1);

	this.chk8 = new lib.chk8();
	this.chk8.parent = this;
	this.chk8.setTransform(647.2,304.6,1,1,0,0,0,-0.4,0.6);
	new cjs.ButtonHelper(this.chk8, 0, 1, 1);

	this.chk7 = new lib.chk7();
	this.chk7.parent = this;
	this.chk7.setTransform(345.2,304.4);
	new cjs.ButtonHelper(this.chk7, 0, 1, 1);

	this.chk6 = new lib.chk6();
	this.chk6.parent = this;
	this.chk6.setTransform(496.4,571.3);
	new cjs.ButtonHelper(this.chk6, 0, 1, 1);

	this.chk5 = new lib.chk5();
	this.chk5.parent = this;
	this.chk5.setTransform(192.1,570);
	new cjs.ButtonHelper(this.chk5, 0, 1, 1);

	this.chk4 = new lib.chk4();
	this.chk4.parent = this;
	this.chk4.setTransform(647.6,435.1);
	new cjs.ButtonHelper(this.chk4, 0, 1, 1);

	this.chk3 = new lib.chk3();
	this.chk3.parent = this;
	this.chk3.setTransform(345.2,435.1);
	new cjs.ButtonHelper(this.chk3, 0, 1, 1);

	this.chk2 = new lib.chk2();
	this.chk2.parent = this;
	this.chk2.setTransform(496.4,298.8);
	new cjs.ButtonHelper(this.chk2, 0, 1, 1);

	this.chk1 = new lib.chk1();
	this.chk1.parent = this;
	this.chk1.setTransform(194,298.8);
	new cjs.ButtonHelper(this.chk1, 0, 1, 1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.chk1},{t:this.chk2},{t:this.chk3},{t:this.chk4},{t:this.chk5},{t:this.chk6},{t:this.chk7},{t:this.chk8},{t:this.chk9},{t:this.chk10},{t:this.chk11},{t:this.chk12}]}).wait(1));

	// Layer 1
	this.instance = new lib.Path_12();
	this.instance.parent = this;
	this.instance.setTransform(641.5,583.5,1,1,0,0,0,97.5,93);

	this.instance_1 = new lib.Path_1_1();
	this.instance_1.parent = this;
	this.instance_1.setTransform(481,583.5,1,1,0,0,0,97,93);

	this.instance_2 = new lib.Path_2_1();
	this.instance_2.parent = this;
	this.instance_2.setTransform(335.5,583.5,1,1,0,0,0,97.5,93);

	this.instance_3 = new lib.Path_3();
	this.instance_3.parent = this;
	this.instance_3.setTransform(184,583.5,1,1,0,0,0,97,93);

	this.instance_4 = new lib.Path_4();
	this.instance_4.parent = this;
	this.instance_4.setTransform(641.5,448.5,1,1,0,0,0,97.5,93);

	this.instance_5 = new lib.Path_5();
	this.instance_5.parent = this;
	this.instance_5.setTransform(481,448.5,1,1,0,0,0,97,93);

	this.instance_6 = new lib.Path_6();
	this.instance_6.parent = this;
	this.instance_6.setTransform(335.5,448.5,1,1,0,0,0,97.5,93);

	this.instance_7 = new lib.Path_7();
	this.instance_7.parent = this;
	this.instance_7.setTransform(184,448.5,1,1,0,0,0,97,93);

	this.instance_8 = new lib.Path_8();
	this.instance_8.parent = this;
	this.instance_8.setTransform(638,314.5,1,1,0,0,0,97,93);

	this.instance_9 = new lib.Path_9();
	this.instance_9.parent = this;
	this.instance_9.setTransform(485,314.5,1,1,0,0,0,97,93);

	this.instance_10 = new lib.Path_10();
	this.instance_10.parent = this;
	this.instance_10.setTransform(332,314.5,1,1,0,0,0,97,93);

	this.instance_11 = new lib.Path_11();
	this.instance_11.parent = this;
	this.instance_11.setTransform(180.5,314.5,1,1,0,0,0,97.5,93);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_11},{t:this.instance_10},{t:this.instance_9},{t:this.instance_8},{t:this.instance_7},{t:this.instance_6},{t:this.instance_5},{t:this.instance_4},{t:this.instance_3},{t:this.instance_2},{t:this.instance_1},{t:this.instance}]}).wait(1));

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.lf(["#E5E5E5","#FFFFFF","#EEEEEE","#FFFFFF"],[0.071,0.251,0.498,0.816],-150.1,73.3,212.5,-103.8).s().p("AnzH0QjPjPAAklQAAiPA4iDQA1h/BihiQBihiB/g1QCDg4CPAAQCQAACDA4QB/A1BiBiQDPDPAAEkQAAEljPDPQjPDPklAAQkkAAjPjPg");
	this.shape.setTransform(995.9,159.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AkbKfQiCg4hlhkQhkhlg4iCQg5iIAAiUQAAiTA5iHQA4iDBkhkQBlhlCCg4QCIg5CTAAQCUAACHA5QCDA4BlBlQBkBkA4CDQA5CHAACTQAACUg5CIQg4CChkBlQhlBkiDA4QiHA5iUAAQiTAAiIg5g");
	this.shape_1.setTransform(994.7,160.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.lf(["#E5E5E5","#FFFFFF","#EEEEEE","#FFFFFF"],[0.071,0.251,0.498,0.816],-82.9,0,83,0).s().p("AlCL8QiUg+h0h0Qhzhyg+iVQhCibAAioQAAioBCiaQA+iUBzh0QB0hyCUhAQCbhBCnAAQCpAACaBBQCUA/B0BzQByB0A/CUQBBCaABCoQgBCphBCaQg/CVhyByQh0B0iUA+QiaBBipAAQinAAibhBgAm2m3Qi3C3AAEAQAAEBC3C2QC1C2EBAAQEBAAC2i2QC2i1AAkCQAAkAi2i3Qi2i2kBAAQkAAAi2C2g");
	this.shape_2.setTransform(993.3,161.4);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AlEMDQiWhAh1hzQh0h1g/iWQhCibAAiqQAAipBCibQBAiWBzh1QB1hzCWhAQCbhCCpAAQCqAACbBCQCWA/B1B0QBzB1BACWQBCCbAACpQAACqhCCbQhACWhzB1Qh1BziWBAQibBBiqAAQipAAibhBgAm6m6Qi4C4AAECQAAEEC4C3QC4C3ECAAQEDAAC4i3QC3i3AAkEQAAkCi3i4Qi4i4kDAAQkCAAi4C4g");
	this.shape_3.setTransform(996.7,158.9);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AkCECQhrhqAAiYQAAiWBrhsQBrhrCXAAQCXAABsBrQBrBsAACWQAACXhrBrQhsBriXAAQiXAAhrhrg");
	this.shape_4.setTransform(991.5,164.1);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.lf(["#FFFFFF","#FCFCFC"],[0,1],-24.5,30.5,22.7,-28.1).s().p("AkJELQhvhvAAicQAAibBvhuQBuhvCbAAQCcAABvBvQBuBugBCbQABCchuBvQhvBticAAQibAAhuhtg");
	this.shape_5.setTransform(991.6,164);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.lf(["#FFFFFF","#F9F9F9"],[0,1],-25.2,31.4,23.4,-28.9).s().p("AkRESQhyhxAAihQAAigByhxQBxhyCgAAQChAABxByQByBxAACgQAAChhyBxQhxByihAAQigAAhxhyg");
	this.shape_6.setTransform(991.7,163.9);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.lf(["#FFFFFF","#F5F5F5"],[0,1],-26,32.3,24.1,-29.8).s().p("AkZEaQh1h0AAimQAAilB1h0QB1h1CkAAQClAAB1B1QB1B0AAClQAACmh1B0Qh1B1ilAAQikAAh1h1g");
	this.shape_7.setTransform(991.8,163.9);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.lf(["#FFFFFF","#F2F2F2"],[0,1],-26.7,33.2,24.7,-30.6).s().p("AkhEiQh4h4AAiqQAAipB4h4QB4h4CpAAQCqAAB4B4QB4B4AACpQAACqh4B4Qh4B4iqAAQipAAh4h4g");
	this.shape_8.setTransform(992,163.8);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.lf(["#FFFFFF","#EFEFEF"],[0,1],-27.4,34.1,25.4,-31.4).s().p("AkpEqQh7h8AAiuQAAiuB7h7QB7h7CuAAQCuAAB8B7QB7B7AACuQAACvh7B7Qh7B7ivAAQiuAAh7h7g");
	this.shape_9.setTransform(992.1,163.7);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.lf(["#FFFFFF","#ECECEC"],[0,1],-28.2,35,26,-32.3).s().p("AkxEyQh+h/gBizQABiyB+h/QB/h/CyAAQCzAAB+B/QB/B/AACyQAACzh/B/Qh+B+izAAQiyAAh/h+g");
	this.shape_10.setTransform(992.2,163.7);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.lf(["#FFFFFF","#E9E9E9"],[0,1],-28.9,35.9,26.7,-33.1).s().p("Ak4E5QiDiBAAi4QAAi3CDiCQCBiCC3AAQC4AACBCCQCDCCAAC3QAAC4iDCBQiBCCi4AAQi3AAiBiCg");
	this.shape_11.setTransform(992.3,163.6);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.lf(["#FFFFFF","#E8E8E8","#E6E6E6"],[0,0.918,1],-29.6,36.8,27.4,-33.9).s().p("AlBFBQiEiFAAi8QAAi7CEiFQCGiGC7AAQC8AACGCGQCECFAAC7QAAC8iECFQiGCFi8AAQi7AAiGiFg");
	this.shape_12.setTransform(992.5,163.5);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.lf(["#FFFFFF","#E8E8E8","#E2E2E2"],[0,0.804,1],-30.3,37.7,28.1,-34.8).s().p("AlIFJQiJiIAAjBQAAjACJiIQCIiJDAAAQDBAACICJQCJCIAADAQAADBiJCIQiICJjBAAQjAAAiIiJg");
	this.shape_13.setTransform(992.6,163.4);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.lf(["#FFFFFF","#E8E8E8","#DFDFDF"],[0,0.737,1],-31.1,38.6,28.7,-35.6).s().p("AlRFRQiLiLAAjGQAAjECLiNQCNiLDEAAQDFAACMCLQCMCNAADEQAADGiMCLQiMCMjFAAQjEAAiNiMg");
	this.shape_14.setTransform(992.7,163.4);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.lf(["#FFFFFF","#E8E8E8","#DCDCDC"],[0,0.682,1],-31.8,39.5,29.4,-36.5).s().p("AlYFZQiPiPAAjKQAAjJCPiPQCPiPDJAAQDKAACPCPQCPCPAADJQAADKiPCPQiPCPjKAAQjJAAiPiPg");
	this.shape_15.setTransform(992.8,163.3);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.lf(["#FFFFFF","#E8E8E8","#D9D9D9"],[0,0.635,1],-32.5,40.4,30.1,-37.3).s().p("AlgFhQiSiSAAjPQAAjOCSiSQCSiSDOAAQDOAACTCSQCSCSAADOQAADPiSCSQiTCSjOAAQjOAAiSiSg");
	this.shape_16.setTransform(993,163.2);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.lf(["#FFFFFF","#E8E8E8","#D6D6D6"],[0,0.592,1],-33.2,41.3,30.8,-38.1).s().p("AloFpQiViWAAjTQAAjTCViVQCWiWDSABQDTgBCWCWQCVCVAADTQAADTiVCWQiWCVjTAAQjSAAiWiVg");
	this.shape_17.setTransform(993.1,163.2);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.lf(["#FFFFFF","#E8E8E8","#D3D3D3"],[0,0.557,1],-34,42.2,31.5,-39).s().p("AlvFxQiaiZABjYQgBjXCaiZQCYiZDXAAQDYAACZCZQCYCZAADXQAADYiYCZQiZCYjYAAQjXAAiYiYg");
	this.shape_18.setTransform(993.2,163.1);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.lf(["#FFFFFF","#E8E8E8","#CFCFCF"],[0,0.514,1],-34.7,43.1,32.1,-39.8).s().p("Al4F5QicicAAjdQAAjcCcicQCcicDcAAQDdAACcCcQCcCcAADcQAADdicCcQicCcjdAAQjcAAicicg");
	this.shape_19.setTransform(993.3,163);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.lf(["#FFFFFF","#E8E8E8","#CCCCCC"],[0,0.49,1],-35.4,44.1,32.8,-40.6).s().p("AmAGBQififAAjiQAAjgCfifQCfigDhAAQDhAACfCgQCgCfAADgQAADiigCfQifCfjhAAQjhAAififg");
	this.shape_20.setTransform(993.5,162.9);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.lf(["#FFFFFF","#E8E8E8","#C9C9C9"],[0,0.467,1],-36.2,45,33.5,-41.4).s().p("AmIGJQiiijAAjmQAAjlCiiiQCjijDlAAQDmAACjCjQCiCiAADlQAADmiiCjQijCijmAAQjlAAijiig");
	this.shape_21.setTransform(993.6,162.9);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.lf(["#FFFFFF","#E8E8E8","#C6C6C6"],[0,0.443,1],-36.9,45.8,34.2,-42.3).s().p("AmPGQQimimAAjqQAAjpCmimQClimDqAAQDrAAClCmQCmCmAADpQAADqimCmQilCmjrAAQjqAAilimg");
	this.shape_22.setTransform(993.7,162.8);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.lf(["#FFFFFF","#E8E8E8","#C3C3C3"],[0,0.424,1],-37.6,46.7,34.8,-43.2).s().p("AmXGYQipipAAjvQAAjuCpipQCpipDuAAQDvAACpCpQCpCpAADuQAADvipCpQipCpjvAAQjuAAipipg");
	this.shape_23.setTransform(993.8,162.7);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.lf(["#FFFFFF","#E8E8E8","#C0C0C0"],[0,0.408,1],-38.3,47.7,35.5,-43.9).s().p("AmfGgQitisAAj0QAAjyCtitQCsisDzAAQD0AACsCsQCsCtAADyQAAD0isCsQisCsj0AAQjzAAisisg");
	this.shape_24.setTransform(994,162.7);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.lf(["#FFFFFF","#E8E8E8","#BDBDBD"],[0,0.388,1],-39.1,48.5,36.2,-44.8).s().p("AmnGoQiviwAAj4QAAj3CviwQCwiwD3AAQD4AACwCwQCvCwAAD3QAAD4ivCwQiwCvj4AAQj3AAiwivg");
	this.shape_25.setTransform(994.1,162.6);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.lf(["#FFFFFF","#E8E8E8","#B9B9B9"],[0,0.369,1],-39.8,49.4,36.8,-45.6).s().p("AmvGwQizizAAj9QAAj8CzizQCzizD8AAQD9AACzCzQCzCzAAD8QAAD9izCzQizCzj9AAQj8AAizizg");
	this.shape_26.setTransform(994.2,162.5);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.lf(["#FFFFFF","#E8E8E8","#B6B6B6"],[0,0.357,1],-40.5,50.4,37.5,-46.4).s().p("Am3G4Qi2i3AAkBQAAkBC2i2QC3i2EAAAQECAAC2C2QC2C2AAEBQAAEBi2C3Qi2C2kCAAQkAAAi3i2g");
	this.shape_27.setTransform(994.3,162.5);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.lf(["#FFFFFF","#E8E8E8","#B3B3B3"],[0,0.345,1],-41.3,51.3,38.1,-47.3).s().p("Am+G/Qi6i5AAkGQAAkGC6i4QC5i6EFAAQEGAAC5C6QC6C4AAEGQAAEGi6C5Qi5C6kGAAQkFAAi5i6g");
	this.shape_28.setTransform(994.5,162.4);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.lf(["#FFFFFF","#E8E8E8","#B0B0B0"],[0,0.333,1],-42,52.2,38.8,-48.1).s().p("AnGHIQi9i9AAkLQAAkKC9i8QC8i9EKAAQELAAC9C9QC8C8AAEKQAAELi8C9Qi9C8kLAAQkKAAi8i8g");
	this.shape_29.setTransform(994.6,162.3);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.lf(["#FFFFFF","#E8E8E8","#AEAEAE","#ADADAD"],[0,0.322,0.992,1],-42.7,53,39.5,-49).s().p("AnOHQQjAjAAAkQQAAkODAjAQC/jAEPAAQEPAADADAQDADAAAEOQAAEQjADAQjAC/kPAAQkPAAi/i/g");
	this.shape_30.setTransform(994.7,162.2);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.lf(["#FFFFFF","#E8E8E8","#AEAEAE","#AAAAAA"],[0,0.31,0.961,1],-43.5,54,40.1,-49.7).s().p("AnXHYQjCjEAAkUQAAkTDCjDQDEjEETAAQEUAADDDEQDEDDAAETQAAEUjEDEQjDDCkUABQkTgBjEjCg");
	this.shape_31.setTransform(994.9,162.2);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.lf(["#FFFFFF","#E8E8E8","#AEAEAE","#A6A6A6"],[0,0.298,0.922,1],-44.2,54.9,40.8,-50.6).s().p("AneHfQjHjHABkYQgBkYDHjGQDHjGEXAAQEZAADGDGQDGDGAAEYQAAEYjGDHQjGDGkZAAQkXAAjHjGg");
	this.shape_32.setTransform(995,162.1);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.lf(["#FFFFFF","#E8E8E8","#AEAEAE","#A3A3A3"],[0,0.29,0.894,1],-44.9,55.7,41.5,-51.5).s().p("AkLJ7Qh8g1hfhfQjKjKAAkdQAAkcDKjKQDKjKEcAAQEeAADJDKQDKDKAAEcQAAEdjKDKQhfBfh8A1QiAA2iMAAQiLAAiAg2g");
	this.shape_33.setTransform(995.1,162);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.lf(["#FFFFFF","#E8E8E8","#AEAEAE","#A0A0A0"],[0,0.282,0.871,1],-45.6,56.7,42.2,-52.2).s().p("AnuHvQjNjNAAkiQAAkhDNjNQDNjNEhAAQEiAADNDNQDNDNAAEhQAAEijNDNQjNDNkiAAQkhAAjNjNg");
	this.shape_34.setTransform(995.2,162);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.lf(["#FFFFFF","#E8E8E8","#AEAEAE","#9D9D9D"],[0,0.275,0.847,1],-46.4,57.6,42.8,-53.1).s().p("An2H3Qhihjg2iAQg4iDAAiRQAAklDQjRQDQjQEmAAQCRAACDA4QCBA2BhBiQDRDQAAEmQAAEmjRDRQjPDQknAAQkmAAjQjQg");
	this.shape_35.setTransform(995.4,161.9);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.lf(["#FFFFFF","#E8E8E8","#AEAEAE","#9A9A9A"],[0,0.267,0.824,1],-47.1,58.4,43.5,-54).s().p("AkYKZQiCg2hkhkQhkhkg3iCQg4iGAAiTQAAiSA4iGQA3iCBkhkQBkhkCCg2QCGg5CSAAQCTAACGA5QCCA2BkBkQBkBkA2CCQA5CGAACSQAACTg5CGQg2CChkBkQhkBkiCA2QiGA5iTAAQiSAAiGg5g");
	this.shape_36.setTransform(995.5,161.8);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.lf(["#FFFFFF","#E8E8E8","#AEAEAE","#979797"],[0,0.259,0.804,1],-47.8,59.4,44.2,-54.8).s().p("AkdKkQiDg4hmhlQhlhmg4iDQg6iJABiVQgBiUA6iJQA4iDBlhlQBmhmCDg4QCJg6CUABQCVgBCJA6QCDA4BmBmQBlBlA4CDQA5CJABCUQgBCVg5CJQg4CDhlBmQhmBliDA4QiJA6iVAAQiUAAiJg6g");
	this.shape_37.setTransform(995.6,161.8);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.lf(["#FFFFFF","#E8E8E8","#AEAEAE","#939393"],[0,0.251,0.776,1],-48.5,60.3,44.9,-55.6).s().p("AkhKuQiGg4hmhoQhohmg4iGQg7iLAAiXQAAiXA7iJQA4iHBohnQBmhnCGg4QCLg7CWAAQCYAACKA7QCFA4BoBnQBmBnA5CHQA6CJAACXQAACXg6CLQg5CGhmBmQhoBoiFA4QiKA6iYABQiWgBiLg6g");
	this.shape_38.setTransform(995.7,161.7);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.lf(["#FFFFFF","#E8E8E8","#AEAEAE","#909090"],[0,0.247,0.757,1],-49.3,61.2,45.5,-56.4).s().p("AklK4QiIg5hphpQhohog5iIQg7iMAAiaQAAiZA7iMQA5iIBohpQBphoCIg5QCMg8CZAAQCaAACMA8QCIA5BoBoQBpBpA5CIQA7CMABCZQgBCag7CMQg5CIhpBoQhoBpiIA5QiMA8iagBQiZABiMg8g");
	this.shape_39.setTransform(995.9,161.6);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.lf(["#FFFFFF","#E8E8E8","#AEAEAE","#8D8D8D"],[0,0.239,0.741,1],-50,62.1,46.2,-57.3).s().p("AkqLDQiJg7hqhqQhqhqg7iJQg8iPAAicQAAibA8iOQA7iKBqhqQBqhqCJg6QCPg9CbAAQCcAACOA9QCKA6BqBqQBqBqA6CKQA9COAACbQAACcg9CPQg6CJhqBqQhqBqiKA7QiOA8icAAQibAAiPg8g");
	this.shape_40.setTransform(996,161.5);

	this.instance_12 = new lib.Path_13();
	this.instance_12.parent = this;
	this.instance_12.setTransform(977.5,191.1,1,1,0,0,0,109.5,116.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_12},{t:this.shape_40},{t:this.shape_39},{t:this.shape_38},{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer 1
	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f("#FFFFFF").s().p("AgMAOQgDgDAAgLQAAgKADgCQAEgEAIAAQAJAAADADQAEAFAAAIQAAAJgEAFQgDADgJAAQgIAAgEgDg");
	this.shape_41.setTransform(811.7,135);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f("#FFFFFF").s().p("AAeBNIAAgdQAUAAAMgNQAMgOgBgVQgBgTgLgPQgMgOgSABQgVAAgOAPQASAEALANQALANAAATQAAAcgVARQgSAPgdAAQhHAAAAhNIAAgOIAbAAIAAAEQAAAFAHAAQAGAAAFgDQAGgDAEgGQADgDABgFIABgGQAAgGgDgGQgCgEgFAAQgMAAAAAOIgdAAIgBgFQAAgRALgKQANgLAPAAQANAAALAGQAJAHABALQAUgYAkAAQAdAAAUAWQAVAWAAAfQACAfgRAXQgTAYghAAgAgdAtQAMgBAJgFQAJgGABgKIABgGQAAgOgKgFQgFgDgEAAQgGABgDADQgLAKgHAFQgKAHgKgBQgFAAgEgCQAIAdAjgCg");
	this.shape_42.setTransform(796.6,128.9);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f("#FFFFFF").s().p("AgUAmIAAgwQAAgJgLAAQgNAAgDAIIAAgYQAEgHAIgDQAIgDAHAAQAPABAEALQAFgNATABQALAAAHAJQAHAIAAANQAAAYgXAFQgKABgJgDIAAAoQgNgIgNgDgAAGgVIAAAOQADACADAAQAIAAAAgLQAAgDgCgEQgDgCgDAAQgEAAgCAEg");
	this.shape_43.setTransform(780.2,118.8);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f("#FFFFFF").s().p("AAeBNIAAgdQAUAAAMgNQAMgOgBgVQgBgTgLgPQgMgOgSABQgVAAgOAPQASAEALANQALANAAATQAAAcgVARQgSAPgdAAQhHAAAAhNIAAgOIAbAAIAAAEQAAAFAHAAQAGAAAFgDQAGgDAEgGQADgDABgFIABgGQAAgGgDgGQgCgEgFAAQgMAAAAAOIgdAAIgBgFQAAgRALgKQANgLAPAAQAOAAAJAGQAKAHABALQAUgYAkAAQAdAAAUAWQAVAWAAAfQACAfgRAXQgTAYghAAgAgdAtQAMgBAJgFQAJgGABgKIABgGQAAgOgKgFQgFgDgEAAQgGABgDADQgLAKgHAFQgKAHgKgBQgFAAgEgCQAIAdAjgCg");
	this.shape_44.setTransform(774.4,128.9);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f("#FFFFFF").s().p("AgnBfQgSgHgKgOQgSgXAAgbQAAgMAEgNQALgjApgPIAQgJQANgJAHgIQAIgIAIgQIAkAAQgDALgJAPQgNARgQAHIAMACQAaAHAQAVQAOAUAAAZQAAASgIASQgKAVgXALQgVAKgYAAQgTAAgUgHgAgogIQgLAOAAARQAAAVAQAOQAOALAWAAQAbAAAOgSQAKgNAAgPQAAgVgQgOQgOgNgWAAQgaAAgOARg");
	this.shape_45.setTransform(754.2,126.6);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f("#FFFFFF").s().p("AgdBAQgKAOgXgBQgdAAgHgYIgBgHQAAgEACgGIADgGQAAgEgFAAQgGAAgDAHIAAgYIAXgZIAZgYQAAgBAAAAQABAAAAgBQAAAAAAgBQAAAAAAgBQAAgGgIAAQgIACAAALIAAAFIgbAAIgBgIQAAgOAKgLQALgKANgBQAMAAAKAFQAJAFADAJQAPgMAPgDQALgEAWAAQAhAAAYAXQAYAWAAAgQAAAigQAUQgRAWgfAAIAAgcQAPgBAJgNQAJgOAAgSQgBgVgOgOQgPgPgUgBQgQgBgOAEQAUADAPARQANASACAXQACAagMASQgMARgVAAIgDAAQgVAAgKgNgAgbgXQgMACgPAOQgPAMgFAHQAIADAAAIIgCANQAAALAMABQAHAAAEgGQAEgGAAgJIAAgKIAbAAIAAAKQAAAVAQgBQAIAAAEgLQADgKgBgMQgBgQgNgLQgLgKgOAAIgEAAg");
	this.shape_46.setTransform(733.5,128.9);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f("#FFFFFF").s().p("AgVAmIAAgwQAAgJgKAAQgNAAgDAIIAAgYQADgHAIgDQAJgDAGAAQAPAAAFAMQAEgNAUABQAKAAAJAJQAGAJABAMQAAAYgZAFQgKABgIgDIAAAoQgNgIgOgDgAAGgVIAAAOQADACADAAQAIAAAAgLQAAgEgDgDQgCgCgDAAQgEAAgCAEg");
	this.shape_47.setTransform(706.2,118.8);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.f("#FFFFFF").s().p("AgdBAQgKAOgXgBQgdAAgHgYIgBgHQAAgEACgGIADgGQAAgEgFAAQgGAAgDAHIAAgYIAXgZIAZgYQAAgBAAAAQABAAAAgBQAAAAAAgBQAAAAAAgBQAAgGgIAAQgIACAAALIAAAFIgbAAIgBgIQAAgOAKgLQALgKANgBQAMAAAKAFQAJAFADAJQAPgMAPgDQALgEAWAAQAhAAAYAXQAYAWAAAgQAAAigQAUQgRAWgfAAIAAgcQAPgBAJgNQAJgOAAgSQgBgVgOgOQgPgPgUgBQgQgBgOAEQAUADAPARQANASACAXQACAagMASQgMARgVAAIgDAAQgVAAgKgNgAgbgXQgMACgPAOQgPAMgFAHQAIADAAAIIgCANQAAALANABQAGAAAEgGQAFgGAAgJIAAgKIAaAAIAAAKQAAAVAQgBQAIAAAEgLQADgIgBgOQgBgQgNgLQgLgKgOAAIgEAAg");
	this.shape_48.setTransform(699.9,128.9);

	this.shape_49 = new cjs.Shape();
	this.shape_49.graphics.f("#FFFFFF").s().p("AAwAbQACgOgRgHQgNgGgTAAQgTAAgNAFQgQAGAAAMIAAAEIgfAAIgCgLQAAgUAcgMQAXgKAeAAQAhAAAVAJQAaALABAUQgBAGgCAHg");
	this.shape_49.setTransform(678.1,118.8);

	this.shape_50 = new cjs.Shape();
	this.shape_50.graphics.f("#FFFFFF").s().p("AhCBKQgdgbAAgtQAAgtAbgcQAbgaArgCQAkAAAbAUQAeAXABAjQABAhgSATQgTAUgfABQgbACgRgNQgTgOABgZQAAgUANgHIgQAAIAAgaIBEAAIAAAaIgLAAQgKAAgGAIQgFAHAAAKQAAAMAIAGQAKAHANgBQAPgBAJgMQAKgMgBgTQgBgagUgNQgRgLgZAAQgXAAgTAQQgUASgBAfQAAAiARAUQASAVAhABQASABAPgGQAPgGAFgLIAhAAQgHAYgbAPQgXAMgdAAQgrAAgdgag");
	this.shape_50.setTransform(678.1,131.3);

	this.shape_51 = new cjs.Shape();
	this.shape_51.graphics.f("#FFFFFF").s().p("AgUAmIAAgwQAAgJgLAAQgNAAgDAIIAAgYQADgHAJgDQAHgDAIAAQAPABAEALQAFgNATABQALAAAHAJQAHAJAAAMQAAAYgXAFQgLABgIgDIAAAoQgNgIgNgDgAAGgVIAAAOQACACADAAQAJAAAAgLQAAgDgCgEQgEgCgCAAQgEAAgCAEg");
	this.shape_51.setTransform(662.5,118.8);

	this.shape_52 = new cjs.Shape();
	this.shape_52.graphics.f("#FFFFFF").s().p("Ag+BNQgdAAgHgYQgBgDAAgEQAAgEACgGQADgDAAgDQAAgEgFAAQgFAAgEAHIAAgYIAwgxQAAgBAAAAQABAAAAgBQAAAAAAgBQAAAAAAgBQAAgGgIAAQgIACAAALIABAFIgcAAIAAgIQAAgPAJgKQALgKANgBQANAAAJAFQAKAFACAJQAPgMAPgDQAMgEAVAAQAiAAAXAXQAYAWAAAgQABAhgRAVQgQAWggAAIAAgcQAPgBAJgNQAJgNAAgTQAAgUgPgPQgPgPgUgBQgQgBgOAEQAVAEAOAQQAOARABAYQACAagMASQgMARgVAAQgXAAgKgNQgKANgVAAIgDAAgAgbgXQgLACgQAOIgUATQAIADAAAIIgBAHIgBAGQAAALANABQAGAAAFgGQAEgHAAgIIAAgKIAaAAIAAAKQAAAVAQgBQAJAAADgLQADgJgBgNQgBgQgNgLQgKgKgPAAIgEAAg");
	this.shape_52.setTransform(656.2,128.9);

	this.shape_53 = new cjs.Shape();
	this.shape_53.graphics.f("#FFFFFF").s().p("AhIBGQgUgIgDgRQgBgFADgFQADgFgBgDQAAAAgBgBQAAAAAAAAQgBgBAAAAQgBAAAAAAQgDAAgEADQgDACgCADIAAgbIA0gwIABgDQAAgDgEgBQgDgCgEABQgIABAAAKQAAAFACACIgdAAIgBgKQAAgPALgJQALgJAOAAQAbgCAGAUQAcgTAkABQAeAAAVATQAXAWAAAlQAAAhgTAUQgTATggAAIgJAAIAAgbQAVACANgNQANgNAAgVQAAgYgOgNQgNgNgZAAQgNAAgLAFQASAEALAQQALAPAAATQAAAegWARQgTARggAAIgFAAQgTAAgOgGgAgwgJQgKAHgMAMQAFADADAFIABAEIgBAFIgBAFQAAAHAKAFQAHADAJAAQAQAAAMgIQAOgIABgPQABgPgIgJQgKgLgOAAIgEAAQgKACgJAIg");
	this.shape_53.setTransform(623,129);

	this.shape_54 = new cjs.Shape();
	this.shape_54.graphics.f("#FFFFFF").s().p("AhABTQgbgWgCgsQAAgaAMgTQAPgXAYAAQARAAAHALQAIgLATAAQAUAAALAQQAKAQAAATQAAAUgOANQgOAOgWAAQgUAAgNgKQgPgKAAgVQAAgLAHgHQACgCAAgEQgBgDgDgBIgCAAQgHAAgFAMQgEAKABAPQABAbAQAPQAQAOAbAAQAbAAASgTQARgTAAgdQAAgggRgWQgRgYgcAAQggAAgRATIgiAAQAXgtA9AAQAqAAAZAcQAaAdAAAvQAAAugbAaQgZAZgqAAQgoAAgYgSgAgTAFQAAAGAFAFQAHAFAHAAQAKAAAGgGQAFgEABgGQgHAGgPgBQgLgCgGgKQgCADAAAEgAAAgbQgCADAAAFQAAAEACADQADADAEAAQAEAAADgDQADgDAAgEQAAgFgDgDQgDgDgEAAQgEAAgDADg");
	this.shape_54.setTransform(601.7,126.5);

	this.shape_55 = new cjs.Shape();
	this.shape_55.graphics.f("#FFFFFF").s().p("AAGAmIgCgBQgBAAAAgBQAAAAAAAAQAAAAAAAAQAAgBAAAAIAAgCIAUgnIAAgSIAAgHIADgDIAEgCIAGgBIAGABIAEACQABAAAAAAQABABAAAAQAAABAAAAQABABAAAAIABAHIgBAIIgBAHIgDAGIgYAkIgBACIgGADgAgtAmIgDgBQAAAAAAgBQAAAAAAAAQgBAAAAAAQAAgBAAAAIABgCIATgnIAAgSIABgHIACgDIAFgCIAGgBIAGABIAEACIACADIABAHIAAAIIgCAHIgCAGIgYAkIgCACIgCACIgDABg");
	this.shape_55.setTransform(574.6,119.8);

	this.shape_56 = new cjs.Shape();
	this.shape_56.graphics.f("#FFFFFF").s().p("AAeBNIAAgdQAUAAALgNQAMgNgBgWQgBgTgLgPQgLgOgTABQgVAAgNAPQASAEALANQALANAAATQAAAcgVARQgTAPgcAAQhIAAAAhNIABgOIAbAAIgBAEQAAAFAIAAQAGAAAFgDQAGgEAEgFIADgIIABgGQAAgFgCgHQgDgEgFAAQgMAAABAOIgeAAIAAgFQAAgQALgLQAMgLAQAAQANAAAKAGQAJAHACALQATgYAkAAQAeAAAUAWQAVAWAAAfQABAfgRAXQgSAYghAAgAgdAtQAMgBAIgFQAJgGACgKIABgGQAAgPgKgEQgFgDgFAAQgFABgDADQgKAKgIAFQgKAHgKgBQgGAAgDgCQAHAdAkgCg");
	this.shape_56.setTransform(557.7,128.9);

	this.shape_57 = new cjs.Shape();
	this.shape_57.graphics.f("#FFFFFF").s().p("AgUAmIAAgwQAAgJgLAAQgNAAgDAIIAAgYQADgHAJgDQAHgDAIAAQAOAAAFAMQAFgNATABQAKAAAIAJQAHAJAAAMQAAAYgYAFQgKABgIgDIAAAoQgNgIgNgDgAAGgVIAAAOQACACADAAQAJAAAAgLQAAgEgDgDQgCgCgDAAQgFAAgBAEg");
	this.shape_57.setTransform(541.3,118.8);

	this.shape_58 = new cjs.Shape();
	this.shape_58.graphics.f("#FFFFFF").s().p("AAeBNIAAgdQAUAAALgNQAMgNgBgWQgBgTgLgPQgLgOgTABQgVAAgNAPQASAEALANQALANAAATQAAAcgVARQgTAPgcAAQhIAAAAhNIABgOIAbAAIgBAEQAAAFAIAAQAGAAAFgDQAGgEAEgFIADgIIABgGQAAgFgCgHQgDgEgFAAQgMAAABAOIgeAAIAAgFQAAgQALgLQAMgLAQAAQANAAAKAGQAJAHACALQATgYAkAAQAeAAAUAWQAVAWAAAfQABAfgRAXQgSAYghAAgAgdAtQAMgBAIgFQAJgGACgKIABgGQAAgPgKgEQgFgDgFAAQgFABgDADQgKAKgIAFQgKAHgKgBQgGAAgDgCQAHAdAkgCg");
	this.shape_58.setTransform(535.5,128.9);

	this.shape_59 = new cjs.Shape();
	this.shape_59.graphics.f("#FFFFFF").s().p("AABBVQgFATgigBQgaAAgQgWQgOgWAAgjQAAgbALgVQAMgVAUgDIALgBQAeAAAAAYIgDASQAAAJANABQAGABAEgEQAFgCAAgGQAAgGgFgFQgFgEgGAAIgHABIAAgWQAIgEAJAAQAOAAAMALQAMALAAARQAAAOgIAJQgMAOgbAAQggAAgJgRQgDgIAAgDQAAgFACgDQACgDAAgEQAAgEgDgBQgJgCgFARQgEAOAAAMQAAAVAHAQQAIAPAMABQABAAABAAQAAAAABAAQAAgBABAAQAAAAABgBQAAAAAAAAQABgBAAAAQAAgBAAAAQAAgBAAgBIgGgGQgEgCAAgGQAAgHAFgFQAGgGAKgBQAVgCARACQATACAAARQAAAFgDADIgGAFQgBABAAAAQAAABAAABQAAAAABABQAAAAAAAAQACADADAAQAOgBAHgWQAGgSAAgYQAAgjgRgWQgSgagdAAQgkAAgOAVIghAAQAIgXAXgOQAXgMAdAAQAsgBAaAfQAaAeAAAzQABAlgNAbQgPAegbAAIgDAAQggAAgFgSgAgKA/QAAAEAEADIAFADIACAGQABgGAHgDQAEgCAAgEQAAgJgMAAQgLAAAAAIg");
	this.shape_59.setTransform(514.3,126.3);

	this.shape_60 = new cjs.Shape();
	this.shape_60.graphics.f("#FFFFFF").s().p("AgnBfQgSgHgKgOQgSgYAAgaQAAgMAEgNQALgjAogPIARgJQANgIAHgJQAIgIAIgQIAkAAQgEANgIANQgNARgQAHIAMACQAaAHAQAVQAOAVAAAYQAAASgIASQgLAWgWAKQgVAKgYAAQgUAAgTgHgAgogIQgLAOAAARQAAAVAQAOQAOALAWAAQAbAAAOgSQAKgOAAgOQAAgUgQgPQgOgNgWAAQgbAAgNARg");
	this.shape_60.setTransform(495,126.6);

	this.shape_61 = new cjs.Shape();
	this.shape_61.graphics.f("#FFFFFF").s().p("AAUBmIAAgyIgcABQhRgDAAg2QAAgaASgPQARgNAZAAIAZAAQABgOgSAAQgPAAgDALIgfAAQADgYAQgJQAMgHAVAAQANABAKAHQAMAHAEAQQAHgeAdgBIAdAAIAAAaQgHgEgCAFQgBAEAHAKQAHAMAAAIQAAASgNAKQgKAJgRABIAAAXQAOgDAVgMIAAAeQgHAFgKADIgSAGIAAA0gAgsgZQgKAGgBANQAAAcA1ACIAWgBIAAg2IgoAAQgPAAgJAGgAAyg4IAAAaQAHAAAEgHQACgHgBgFQAAgDgFgJQgDgFABgGQgFADAAANg");
	this.shape_61.setTransform(476.3,131.4);

	this.shape_62 = new cjs.Shape();
	this.shape_62.graphics.f("#FFFFFF").s().p("AAKAmIgEgDIgCgDIgBgHIAAgIIACgGIACgHIAagmIACgCIAIgBIAEABIACAAIABACIgBACIgTAnIgBAZIgCADIgFADgAgpAmIgEgDIgDgDIgBgHIABgIIABgGIADgHIAZgmIADgCIAHgBIAEABIACAAIABACIAAACIgUAnIAAAZIgDADIgEADg");
	this.shape_62.setTransform(461.4,119.8);

	this.shape_63 = new cjs.Shape();
	this.shape_63.graphics.f("#FFFFFF").s().p("AhaAoQgBgTAQgPQAPgNAYAAIANAAQgBgPgMAAQgNAAgCAKIgeAAQABgmAtAAQAUAAANAPQAMAQAAAYIgBANIggAAQgPAAgJAGQgKAFAAAMQAAARAUAHQARAHAWgBQAZgCARgSQAQgSAAgeQAAgfgSgWQgSgXgdAAQgSAAgLAFQgNAGgGAJIghAAQASgwBEAAQAmAAAaAfQAbAfAAApQAAAvgYAaQgXAZgqACIgJAAQhQAAgDg+g");
	this.shape_63.setTransform(434,126.4);

	this.shape_64 = new cjs.Shape();
	this.shape_64.graphics.f("#FFFFFF").s().p("AgxA6QgeAAgGgTIgCgJQAAgNAJgKIAhAAQgHAJAAAGQAAAIALAAIBjAAIAAhAQAMgGASgRIAABzg");
	this.shape_64.setTransform(414.1,136.4);

	this.shape_65 = new cjs.Shape();
	this.shape_65.graphics.f("#FFFFFF").s().p("AhABTQgbgWgCgsQAAgbAMgSQAOgXAZAAQARAAAHALQAIgLATAAQATAAAMAQQAJAQABATQAAAUgOANQgOAOgWAAQgVAAgMgKQgPgKAAgVQAAgLAHgHQABgCABgEQgBgDgDgBIgDAAQgGAAgFAMQgFAKACAPQAAAbAQAPQARAOAbAAQAbAAARgTQASgSAAgeQAAgggRgWQgRgYgcAAQggAAgRATIgiAAQAWgtA+AAQAqAAAZAcQAaAdAAAvQAAAugbAaQgZAZgqAAQgpAAgXgSgAgUAFQAAAGAHAFQAFAFAIAAQAKAAAGgGQAEgEACgGQgIAGgOgBQgLgCgHgKQgCADAAAEgAAAgbQgCADgBAFQABAEACADQADADAEAAQAEAAADgDQADgDAAgEQAAgFgDgDQgDgDgEAAQgEAAgDADg");
	this.shape_65.setTransform(414.1,126.5);

	this.shape_66 = new cjs.Shape();
	this.shape_66.graphics.f("#FFFFFF").s().p("Ag/BOQgagVgEgkIgBgNQAAgpAbgPQAAgXANgNQANgNAVAAQAUAAAJAOQALgOAYAAQATABANATQAKASAAAXIAAAGIhuAAIgNACQACASANALQANALAVAAQAdAAAKgbIAhAAQgHAZgVAQQgVAQgbAAQgXAAgUgNQgUgMgHgUIAAAGQAAAeATARQASATAcAAQAWAAAQgKQAQgKAFgUIAhAAQgEAigaARQgYASglAAQgnAAgcgUgAAWg3IAhAAQgDgRgNAAQgOAAgDARgAglg3IAfAAQgBgRgPAAQgNAAgCARg");
	this.shape_66.setTransform(394.6,131.1);

	this.shape_67 = new cjs.Shape();
	this.shape_67.graphics.f("#FFFFFF").s().p("AhDA/QgbgSAAgeQAAgVAQgOQAQgOAZAAIAXAAQAAgPgXAAQgUABABAMIgfAAIAAgFQAAgRAQgLQAOgIATAAQAcABALAPQAIgJAJgDQAJgDASgBQAXAAALAOQAGAHAAANIAAAHIgiAAQACgEgCgDQgDgGgNAAQgUAAAAAPIAVAAQAaAAAOALQATAQAAAYQAAAegeARQgaANgoAAQgoAAgagOgAg1AAQgIAEAAAKQAAARASAJQAQAIAbAAQAaAAASgJQARgIAAgQQAAgKgJgFQgJgGgMAAIg+AAQgOAAgIAGg");
	this.shape_67.setTransform(374.4,129);

	this.shape_68 = new cjs.Shape();
	this.shape_68.graphics.f("#FFFFFF").s().p("AgqBFIAAgeQAMAIALAAIAHgBQAUgIABgjQABgygcAAQgMAAgLAIIAAgdQAOgHATAAQAZAAAOAYQANAUgBAeQAAAegMAWQgOAZgZAAIgEAAQgPAAgPgHg");
	this.shape_68.setTransform(349,128.9);

	this.shape_69 = new cjs.Shape();
	this.shape_69.graphics.f("#FFFFFF").s().p("AhTBAQgWgPgBgbQgBgVAIgNQAGgKAMgIQAMgHALAAQAAgKgIAAQgJAAAAAIIAAADIgbAAIgBgGQAAgPAKgJQAKgKAPAAQASAAAKAOQALAPgCAbQADgUAMgNQATgXAjAAQAdAAAUAXQAUAWAAAfQgBAjgUAUQgWAXgmgDIAAgaQAVABAOgOQAPgNAAgUQABgTgLgPQgKgOgRgCQgigCgHAgQAOAAAJALQALAKAAAPQACAbgUAQQgSAOgdAAIgCABQgdAAgTgNgAhJALIgBAGQAAAOAMAIQAJAHAPAAQAPACALgGQAMgHAAgNQAAgLgFAAQgEAAAAAFIgcAAQADgBABgIIABgRIgMAAQgZAAgEAVg");
	this.shape_69.setTransform(333.5,129);

	this.shape_70 = new cjs.Shape();
	this.shape_70.graphics.f("#FFFFFF").s().p("AgIBGQAAgOAKgDQgdgCgRgSQgQgPAAgUQAAgbATgQQASgOAeAAIAlAAQgCgJgIgDQgFgBgNAAQgXAAgEANIggAAQAAgFAEgGQgOgBgKARQgLAPgBARQAAAaAKAUQAIATANAKIgjAAQgcggACgyQAAgQALgSQAKgRAOgJQAPgJAOAAQANAAAHAHQARgKAbACQA8ADgDA/Ig+AAQgSAAgMAHQgNAHAAAOQAAAPAPAJQAMAHATgBQAmAAACgPIAfAAQgBAUgQAMQgQALgQABQgFAAgGADQgGAEAAAEQAAAIASAAQAfAAABgWIAfAAQgBAbgUAMQgRALgcAAQgxAAAAgfg");
	this.shape_70.setTransform(312.4,131.2);

	this.shape_71 = new cjs.Shape();
	this.shape_71.graphics.f("#FFFFFF").s().p("AAEA8QgJASgfgBQgbgBgPgPQgPgPABgaQABgRANgJIgWAAIAAgaIBZAAQAAgRgVAAQgSAAgBANIgfAAQAAgTAPgLQANgJATAAIAXACQAKADAGAJQAIgJAMgDQAIgDASABQAPABALALQALALAAAQIgfAAQAAgNgSAAQgUAAAAARIAcAAQA3AAgBA3QAAAYgPAPQgQAPgYAAIgDAAQgdAAgJgRgAgzAAQgJAIAAAMQAAAKAHAIQAGAIAMAAQALAAAHgFQAHgFAAgJIAAgNIAcAAIAAAOQAAASAbAAQALAAAGgIQAHgIAAgMQgBgMgJgGQgKgHgMAAIhCAAQgNAAgJAHg");
	this.shape_71.setTransform(290.5,128.9);

	this.shape_72 = new cjs.Shape();
	this.shape_72.graphics.f("#FFFFFF").s().p("AgUAmIAAgwQAAgJgLAAQgNAAgDAIIAAgYQAEgHAIgDQAIgDAHAAQAPABAEALQAFgNATABQALAAAHAJQAHAIAAANQAAAYgXAFQgKABgJgDIAAAoQgNgIgNgDgAAGgVIAAAOQADACADAAQAIAAAAgLQAAgDgCgEQgDgCgDAAQgEAAgCAEg");
	this.shape_72.setTransform(264.6,118.8);

	this.shape_73 = new cjs.Shape();
	this.shape_73.graphics.f("#FFFFFF").s().p("AhTAvQAAgGADgJQAEgKAFgCIgVAAIAAgZIBBAAQABgHgDgEQgEgEgEAAQgLAAABALIgcAAQgBgSALgKQAKgJARgBQAWAAAKAQQAKARgBAiIgUAAQghAAAAAZQACAgA4gDQAZAAAPgSQAPgRAAgcQAAgcgOgWQgIAIgSAAQgMAAgJgEQgYgLAAgaIABgHIgRgCIAAgWQATgCAWAFQAaAEAPAJQAxAfAABBQAAAtgXAZQgWAYgoACIgIAAQhPAAgEg6gAAGhAQADAFAFACQAMAEAJgGQgJgIgFgCQgJgEgIgCQgBAEADAHg");
	this.shape_73.setTransform(259.9,126.1);

	this.shape_74 = new cjs.Shape();
	this.shape_74.graphics.f("#FFFFFF").s().p("AgKATQgLgHAAgMQAAgGADgEQAGgLAMAAQAGAAAFADQALAHAAALQAAAGgDAFQgGALgNAAQgFAAgFgDg");
	this.shape_74.setTransform(236.6,134.5);

	this.shape_75 = new cjs.Shape();
	this.shape_75.graphics.f("#FFFFFF").s().p("AAeBNIAAgdQAVAAAKgNQAMgNgBgWQAAgTgMgPQgLgOgTABQgVAAgNAPQASAEALANQAMAOgBASQAAAcgVARQgTAPgcAAQhIAAAAhNIABgOIAbAAIgBAEQAAAFAIAAQAGAAAFgDQAGgEAEgFIAEgIIAAgGQAAgHgCgFQgDgEgEAAQgNAAABAOIgeAAIAAgFQAAgQALgLQAMgLAQAAQAOAAAJAGQAKAIABAKQAUgYAjAAQAeAAAUAWQAVAWAAAfQABAggRAWQgSAYghAAgAgdAtQALgBAKgFQAIgGACgKIABgGQAAgOgKgFQgFgDgFAAQgFABgDADQgLAKgHAFQgKAHgKgBQgGAAgDgCQAHAdAkgCg");
	this.shape_75.setTransform(222.1,128.9);

	this.shape_76 = new cjs.Shape();
	this.shape_76.graphics.f("#FFFFFF").s().p("AgUAmIAAgwQAAgJgLAAQgNAAgDAIIAAgYQADgHAJgDQAHgDAIAAQAPABAEALQAFgNATABQALAAAHAJQAHAJAAAMQAAAYgXAFQgLABgIgDIAAAoQgNgIgNgDgAAGgVIAAAOQACACADAAQAJAAAAgLQAAgDgCgEQgEgCgCAAQgEAAgCAEg");
	this.shape_76.setTransform(205.7,118.8);

	this.shape_77 = new cjs.Shape();
	this.shape_77.graphics.f("#FFFFFF").s().p("AAeBNIAAgdQAVAAAKgNQAMgNAAgWQgBgTgMgPQgLgOgTABQgVAAgNAPQASAEALANQAMAOgBASQAAAcgVARQgTAPgcAAQhIAAAAhNIABgOIAbAAIAAAEQAAAFAHAAQAGAAAFgDQAGgDAEgGIAEgIIAAgGQAAgHgCgFQgCgEgFAAQgNAAABAOIgeAAIAAgFQAAgQALgLQAMgLAQAAQAOAAAJAGQAKAIABAKQAUgYAkAAQAdAAAUAWQAVAWAAAfQABAggRAWQgSAYghAAgAgdAtQALgBAKgFQAIgGACgKIABgGQAAgOgKgFQgFgDgEAAQgGABgDADQgLAKgHAFQgKAHgKgBQgGAAgDgCQAHAdAkgCg");
	this.shape_77.setTransform(199.9,128.9);

	this.shape_78 = new cjs.Shape();
	this.shape_78.graphics.f("#FFFFFF").s().p("AgnBfQgSgHgKgOQgSgYAAgaQAAgOAEgLQALgjAogPIARgJQANgIAHgJQAIgIAIgQIAkAAQgEANgJANQgMARgQAHIAMACQAaAHAQAVQAOAVAAAYQAAASgIASQgLAWgWAKQgVAKgYAAQgUAAgTgHgAgogIQgLANAAASQAAAVAQAOQAOALAWAAQAbAAAOgSQAKgOAAgOQAAgUgQgPQgNgNgXAAQgbAAgNARg");
	this.shape_78.setTransform(179.7,126.6);

	this.shape_79 = new cjs.Shape();
	this.shape_79.graphics.f("#FFFFFF").s().p("AgdBAQgKAOgXgBQgdAAgHgYIgBgHQAAgFACgFIADgGQAAgEgFAAQgGAAgDAHIAAgYIAwgxQAAgBABAAQAAAAAAgBQAAAAAAgBQAAAAAAgBQAAgHgIABQgIACAAALIAAAFIgbAAIgBgIQAAgPAKgKQAKgKAOgBQAMgBAJAGQAKAFACAJQAQgMAPgDQALgEAWAAQAhAAAYAXQAXAWABAgQABAigRAUQgRAWgfAAIAAgcQAPgBAJgNQAJgNAAgTQgBgUgOgPQgPgPgUgBQgQgBgOAEQAVAEANAQQAOARACAYQABAbgLARQgMARgVAAIgDAAQgVAAgKgNgAgbgXQgMACgPAOIgVATQAJADAAAIIgCANQAAALAMABQAHAAAEgGQAEgHAAgIIAAgKIAbAAIAAAKQAAAVAQgBQAIAAAEgLQADgKgBgMQgCgQgNgLQgKgKgOAAIgEAAg");
	this.shape_79.setTransform(159,128.9);

	this.shape_80 = new cjs.Shape();
	this.shape_80.graphics.f("#FFFFFF").s().p("AgUAmIAAgwQAAgJgLAAQgNAAgDAIIAAgYQAEgHAHgDQAJgDAHAAQAOABAFALQAFgNATABQALAAAHAJQAIAJAAAMQAAAYgYAFQgLABgIgDIAAAoQgMgIgOgDgAAGgVIAAAOQADACACAAQAJAAAAgLQAAgDgDgEQgCgCgDAAQgEAAgCAEg");
	this.shape_80.setTransform(770.8,82.8);

	this.shape_81 = new cjs.Shape();
	this.shape_81.graphics.f("#FFFFFF").s().p("AgdBAQgKAOgXgBQgdAAgHgYIgBgHQAAgFACgFIADgGQAAgEgFAAQgGAAgDAHIAAgYIAXgZIAZgYQAAgBAAAAQABAAAAgBQAAAAAAgBQAAAAAAgBQAAgHgIABQgIACAAALIAAAFIgbAAIgBgIQAAgOAKgLQALgKANgBQAMAAAKAFQAJAFADAJQAPgMAPgDQALgEAWAAQAhAAAYAXQAYAWAAAgQAAAigQAUQgRAWgfAAIAAgcQAPgBAJgNQAKgNgBgTQgBgUgOgPQgPgPgUgBQgQgBgOAEQAUADAPARQANASACAXQACAagMASQgMARgVAAIgDAAQgVAAgKgNgAgbgXQgMACgPAOQgPAMgFAHQAIADAAAIIgCANQAAALAMABQAHAAAEgGQAEgGAAgJIAAgKIAbAAIAAAKQAAAVAQgBQAIAAAEgLQADgKgBgMQgBgQgOgLQgKgKgOAAIgEAAg");
	this.shape_81.setTransform(764.5,92.9);

	this.shape_82 = new cjs.Shape();
	this.shape_82.graphics.f("#FFFFFF").s().p("AAxAbQABgOgRgHQgNgGgUAAQgRAAgOAFQgQAGAAAMIAAAEIgfAAIgDgLQAAgUAdgMQAWgKAeAAQAiAAAVAJQAbALAAAUQAAAGgDAHg");
	this.shape_82.setTransform(742.7,82.8);

	this.shape_83 = new cjs.Shape();
	this.shape_83.graphics.f("#FFFFFF").s().p("AhCBKQgdgbAAgtQAAgtAbgcQAbgaArgCQAkAAAbAUQAeAXABAjQABAhgSATQgTAUgfABQgbACgRgNQgTgOABgZQAAgUANgHIgQAAIAAgaIBEAAIAAAaIgLAAQgKAAgGAIQgFAHAAAKQAAAMAIAGQAKAHANgBQAOAAAKgNQAKgMgBgTQgBgagUgNQgRgLgZAAQgXAAgTAQQgUASgBAfQgBAhASAVQASAVAhABQASABAPgGQAPgGAFgLIAhAAQgHAYgbAPQgXAMgdAAQgrAAgdgag");
	this.shape_83.setTransform(742.7,95.3);

	this.shape_84 = new cjs.Shape();
	this.shape_84.graphics.f("#FFFFFF").s().p("AgUAmIAAgwQAAgJgLAAQgNAAgDAIIAAgYQADgHAJgDQAHgDAIAAQAPABAEALQAFgNATABQAKAAAIAJQAHAJAAAMQAAAYgXAFQgLABgIgDIAAAoQgNgIgNgDgAAGgVIAAAOQACACADAAQAJAAAAgLQAAgEgDgDQgCgCgDAAQgEAAgCAEg");
	this.shape_84.setTransform(727.1,82.8);

	this.shape_85 = new cjs.Shape();
	this.shape_85.graphics.f("#FFFFFF").s().p("Ag+BNQgdAAgHgYQgBgDAAgEQAAgEACgGQADgDAAgDQAAgEgFAAQgFAAgEAHIAAgYIAwgxQAAgBAAAAQABAAAAgBQAAAAAAgBQAAAAAAgBQAAgGgIAAQgIACAAALIAAAFIgbAAIAAgIQAAgPAJgKQALgKANgBQANAAAJAFQAJAFADAJQAPgMAPgDQALgEAWAAQAiAAAXAXQAYAWAAAgQABAhgRAVQgRAWgfAAIAAgcQAPgBAJgNQAJgNAAgTQAAgUgPgPQgPgPgUgBQgQgBgOAEQAVAEAOAQQAOASABAXQACAbgMARQgLARgWAAQgXAAgKgNQgKANgVAAIgDAAgAgbgXQgLACgQAOIgUATQAIADAAAIIgBAHIgBAGQAAALANABQAHAAADgGQAFgGAAgJIAAgKIAaAAIAAAKQAAAVAQgBQAJAAADgLQADgJgBgNQgBgQgNgLQgLgKgOAAIgEAAg");
	this.shape_85.setTransform(720.8,92.9);

	this.shape_86 = new cjs.Shape();
	this.shape_86.graphics.f("#FFFFFF").s().p("AhJBGQgTgJgDgQQgBgFADgFQADgFgBgDQgBAAAAgBQAAAAgBAAQAAgBAAAAQgBAAAAAAQgDAAgEADQgDACgCADIAAgbIA0gwIABgDQAAgCgEgCQgDgCgEABQgIABAAAKQAAAFACACIgdAAIgBgKQAAgPALgJQALgJAOAAQAbgCAGAUQAcgTAkABQAfAAAUATQAXAWAAAlQAAAhgTAUQgTATggAAIgJAAIAAgbQAVACANgNQANgNAAgVQAAgYgOgNQgNgNgZAAQgNAAgLAFQARAEAMAQQALAPAAATQAAAegWARQgTARggAAIgFAAQgTAAgPgGgAgwgJQgKAHgMAMQAGADACAFIABAEIgBAFIgBAFQAAAHAKAFQAHADAJAAQAQAAAMgIQAOgIABgPQABgOgIgKQgKgLgOAAIgEAAQgKACgJAIg");
	this.shape_86.setTransform(687.6,93);

	this.shape_87 = new cjs.Shape();
	this.shape_87.graphics.f("#FFFFFF").s().p("AhABTQgcgWgBgsQAAgaAMgTQAOgXAZAAQARAAAHALQAIgLATAAQATAAAMAQQAKAQAAATQAAAUgOANQgOAOgWAAQgUAAgNgKQgPgKAAgVQAAgLAHgHQACgDgBgDQAAgDgDgBIgCAAQgHAAgFAMQgEAKAAAPQACAbAQAPQAQAOAbAAQAbAAASgTQARgTAAgdQAAgggRgWQgRgYgcAAQggAAgRATIgiAAQAWgtA+AAQAqAAAZAcQAaAdAAAvQAAAugbAaQgZAZgqAAQgoAAgYgSgAgTAFQAAAGAFAFQAHAFAHAAQAKAAAGgGQAFgFABgFQgIAGgOgBQgLgCgGgKQgCADAAAEgAAAgbQgCADAAAFQAAAEACADQADADAEAAQAEAAADgDQADgDAAgEQAAgFgDgDQgDgDgEAAQgEAAgDADg");
	this.shape_87.setTransform(666.3,90.5);

	this.shape_88 = new cjs.Shape();
	this.shape_88.graphics.f("#FFFFFF").s().p("AgUAmIAAgwQAAgJgLAAQgNAAgDAIIAAgYQAEgHAIgDQAIgDAHAAQAPABAEALQAFgNATABQALAAAHAJQAHAIAAANQAAAYgXAFQgKABgJgDIAAAoQgNgIgNgDgAAGgVIAAAOQADACADAAQAIAAAAgLQAAgDgCgEQgEgCgCAAQgEAAgCAEg");
	this.shape_88.setTransform(640.5,82.8);

	this.shape_89 = new cjs.Shape();
	this.shape_89.graphics.f("#FFFFFF").s().p("AhTAvQAAgGADgJQAEgKAFgCIgVAAIAAgZIBBAAQABgHgDgEQgEgEgEAAQgLAAABALIgcAAQgBgSALgKQAKgJARgBQAWAAAKAQQAKARgBAiIgUAAQghAAAAAZQACAgA4gDQAZAAAPgSQAPgRAAgcQAAgcgOgWQgIAIgSAAQgMAAgJgEQgYgLAAgaIABgHIgRgCIAAgWQATgCAWAFQAaAEAPAJQAxAfAABBQAAAtgXAZQgWAYgoACIgIAAQhPAAgEg6gAAGhAQADAFAFACQAMAEAJgGQgJgIgFgCQgJgEgIgCQgBAEADAHg");
	this.shape_89.setTransform(635.8,90.1);

	this.shape_90 = new cjs.Shape();
	this.shape_90.graphics.f("#FFFFFF").s().p("AgqBFIAAgeQAMAIALAAIAHgBQAUgIABgjQABgygcAAQgMAAgLAIIAAgdQAOgHATAAQAZAAAOAYQANAUgBAeQAAAegMAWQgOAZgZAAIgEAAQgPAAgPgHg");
	this.shape_90.setTransform(611,92.9);

	this.shape_91 = new cjs.Shape();
	this.shape_91.graphics.f("#FFFFFF").s().p("AgnBfQgSgHgKgOQgSgXAAgbQAAgMAEgNQALgjApgPIAQgJQANgJAHgIQAIgIAIgQIAkAAQgDALgJAPQgNARgQAHIAMACQAaAHAQAVQAOAUAAAZQAAASgIASQgKAVgXALQgVAKgYAAQgUAAgTgHgAgogIQgLAOAAARQAAAVAQAOQAOALAWAAQAbAAAOgSQAKgNAAgPQAAgVgQgOQgOgNgWAAQgaAAgOARg");
	this.shape_91.setTransform(597.7,90.6);

	this.shape_92 = new cjs.Shape();
	this.shape_92.graphics.f("#FFFFFF").s().p("AgUAmIAAgwQAAgJgLAAQgNAAgDAIIAAgYQADgHAJgDQAHgDAIAAQAPABAEALQAFgNATABQALAAAHAJQAHAJAAAMQAAAYgXAFQgKABgJgDIAAAoQgNgIgNgDgAAGgVIAAAOQADACACAAQAJAAAAgLQAAgDgCgEQgEgCgCAAQgEAAgCAEg");
	this.shape_92.setTransform(583.1,82.8);

	this.shape_93 = new cjs.Shape();
	this.shape_93.graphics.f("#FFFFFF").s().p("AgqBFIAAgeQANAIAKAAIAHgBQAVgIAAgjQAAgygbAAQgMAAgMAIIAAgdQAPgHASAAQAaAAAOAYQAMAUAAAeQgBAegLAWQgOAZgZAAIgCAAQgQAAgQgHg");
	this.shape_93.setTransform(583.6,92.9);

	this.shape_94 = new cjs.Shape();
	this.shape_94.graphics.f("#FFFFFF").s().p("AhIBGQgUgJgDgQQgBgEADgGQADgFgBgDQAAAAgBgBQAAAAAAAAQgBgBAAAAQgBAAgBAAQgCAAgEADQgDACgCADIAAgbIA0gwIABgDQAAgCgDgCQgEgCgDABQgJABAAAKQAAAEACADIgdAAIgBgKQAAgPALgJQALgJAPAAQAbgCAFAUQAdgTAjABQAfAAAUATQAXAWAAAlQAAAigTATQgTATghAAIgIAAIAAgbQAVACANgNQANgNAAgVQAAgYgNgNQgNgNgZAAQgOAAgLAFQARAEAMAQQALAPAAATQAAAegWARQgSARghAAIgFAAQgTAAgOgGgAgwgJQgKAIgMALQAEACAEAGIAAAEIgBAKQAAAHAKAFQAIADAIAAQAQAAANgIQANgIACgPQABgOgJgKQgJgLgPAAIgEAAQgJABgKAJg");
	this.shape_94.setTransform(568.2,93);

	this.shape_95 = new cjs.Shape();
	this.shape_95.graphics.f("#FFFFFF").s().p("Ag1A3QgVgWABghQABgiAUgVQAUgUAhgBQA3AAAOAlIgfAAQgKgHgHgCQgGgBgQAAQgRAAgNAPQgMAPAAATQAAAUANANQANAPATAAQASAAALgJQAKgJADgOQgPATgYgCQgMgBgJgJQgKgIAAgPQAAgTAPgKQALgIANAAQAeAAAQAQQAQAQgCAaQgCAbgUAOQgTAPgfAAQgiAAgVgWgAADAAQAAALAPAAQANAAADgIQACgCAAgDQAAgFgFgEQgFgDgHAAQgQAAAAAOg");
	this.shape_95.setTransform(548.9,92.9);

	this.shape_96 = new cjs.Shape();
	this.shape_96.graphics.f("#FFFFFF").s().p("AghBnQgcAAgOgQQgOgOAAgYQAAgWAOgJIgYAAIAAgYIBQAAQABgGgEgFQgEgEgGAAQgNAAgBAKIggAAQAAgRANgLQANgKAVAAQATAAAMAQQANAQAAAaIAAAJIgjAAQgQAAgJALQgHAIAAAHQgDAcAaAAQAKAAAFgHQAFgHAAgJIAAgQIAeAAIAAAOQAAALAEAGQAGAIAMAAQANAAAIgVQAGgRAAgZQAAgggRgYQgSgbgfAAQgjAAgNAVIgiAAQAIgYAYgNQAVgMAdAAQAsAAAcAgQAbAggBAvQAAAmgLAaQgOAegdAAQgiAAgJgRQgIARgZAAIgDAAg");
	this.shape_96.setTransform(519.8,90.3);

	this.shape_97 = new cjs.Shape();
	this.shape_97.graphics.f("#FFFFFF").s().p("AAHBJQgaAAgNgLQgQgOABgfQABgWAQgTQAOgSAPgFIgwAAIAAgZIBdAAIAAAWQgZAQgNANQgRATgCARQgBALADAHQAEALALAAQAHAAAFgGQAEgFAAgHIgCgHIAfAAIABANQAAAUgNAKQgLALgQAAIgDAAg");
	this.shape_97.setTransform(504.2,93.3);

	this.shape_98 = new cjs.Shape();
	this.shape_98.graphics.f("#FFFFFF").s().p("AhTBAQgWgPgBgbQgBgVAIgNQAGgKAMgIQANgHAKAAQAAgKgIAAQgJAAAAAIIAAADIgbAAIgBgGQAAgPAKgJQALgKAOAAQASAAAKAOQALAPgCAbQAEgVALgMQATgXAjAAQAdAAAUAXQAUAWAAAfQAAAigUAVQgXAXgmgDIAAgaQAVABAOgOQAPgNAAgUQAAgTgKgPQgKgOgRgCQgigCgHAgQAOAAAJALQALAJAAAQQACAbgUAQQgSAOgdAAIgDABQgcAAgTgNgAhJALIgBAGQAAAOAMAIQAJAHAPAAQAPACALgGQAMgHAAgNQAAgLgFAAQgEAAABAFIgdAAQADgBABgIIABgRIgMAAQgaAAgDAVg");
	this.shape_98.setTransform(487.6,93);

	this.shape_99 = new cjs.Shape();
	this.shape_99.graphics.f("#FFFFFF").s().p("AhIBGQgUgIgEgRQgBgFADgFQAEgGgBgCQAAAAgBgBQAAAAgBAAQAAgBgBAAQAAAAgBAAQgCAAgEADQgEACgBADIAAgbIA0gwIABgDQgBgDgDgBQgCgCgFABQgIABAAAKQAAAEACADIgdAAIgBgKQAAgPALgJQALgJAPAAQAbgCAFAUQAcgTAjABQAfAAAVATQAXAWAAAlQAAAhgTAUQgTATggAAIgJAAIAAgbQAVACANgNQAMgMAAgWQAAgYgNgNQgNgNgZAAQgOAAgKAFQASAEAKAQQAMAOAAAUQAAAegWARQgTARggAAIgFAAQgUAAgNgGgAgwgJQgIAGgOANQAFADADAFIAAAEIgBAKQAAAHAKAFQAHADAJAAQAQAAANgIQAOgIAAgPQABgOgIgKQgKgLgOAAIgEAAQgJABgKAJg");
	this.shape_99.setTransform(454.7,93);

	this.shape_100 = new cjs.Shape();
	this.shape_100.graphics.f("#FFFFFF").s().p("AgUAmIAAgwQAAgJgLAAQgNAAgDAIIAAgYQAEgHAIgDQAIgDAHAAQAPABAEALQAFgNATABQALAAAHAJQAHAIAAANQAAAYgXAFQgKABgJgDIAAAoQgNgIgNgDgAAGgVIAAAOQADACADAAQAIAAAAgLQAAgDgCgEQgDgCgDAAQgEAAgCAEg");
	this.shape_100.setTransform(438,82.8);

	this.shape_101 = new cjs.Shape();
	this.shape_101.graphics.f("#FFFFFF").s().p("AhJBGQgTgJgDgQQgBgFADgFQADgFgBgDQAAAAgBgBQAAAAgBAAQAAgBAAAAQgBAAAAAAQgDAAgEADQgDACgCADIAAgbIA0gwIABgDQAAgDgEgBQgDgCgEABQgIABAAAKQAAAFACACIgdAAIgBgKQAAgPALgJQALgJAOAAQAbgCAGAUQAcgTAkABQAfAAAUATQAXAWAAAlQAAAhgTAUQgTATggAAIgJAAIAAgbQAVACANgNQANgNAAgVQAAgYgOgNQgNgNgZAAQgNAAgLAFQASAEALAQQALAPAAATQAAAegWARQgTARggAAIgFAAQgTAAgPgGgAgwgJQgKAHgMAMQAGADACAFIABAEIgBAFIgBAFQAAAHAKAFQAHADAJAAQAQAAAMgIQAOgIABgPQABgPgIgJQgKgLgOAAIgEAAQgKACgJAIg");
	this.shape_101.setTransform(432,93);

	this.shape_102 = new cjs.Shape();
	this.shape_102.graphics.f("#FFFFFF").s().p("AgnA9QgJgPABgVQABgRALgMQAKgMAQgDQAQgCAKAGQACghgTAAQgOAAgDANIgcAAQABgVAQgLQAOgJASAAQAWABALAQQALAQAAAcQAAAMgDATQgDATAAALQAAAQAFANIgeAAIgEgOQgFAPgUABIgDAAQgPAAgJgQgAgFAIQgLAFAAAPQAAAIAEAHQAEAHAIgCQAIgCADgNIAEgZQgHgCgFAAQgEAAgEACg");
	this.shape_102.setTransform(415.3,92.9);

	this.shape_103 = new cjs.Shape();
	this.shape_103.graphics.f("#FFFFFF").s().p("AhaAoQgBgTAQgPQAQgNAXAAIANAAQAAgPgNAAQgNAAgCAKIgeAAQABgmAtAAQAVAAAMAPQAMAPAAAZIgBANIggAAQgPAAgJAGQgKAGAAALQABARATAHQARAHAWgBQAZgCARgSQAQgTAAgdQAAgfgSgWQgSgXgdAAQgRAAgMAFQgOAGgFAJIghAAQASgwBEAAQAmAAAbAfQAaAfAAApQAAAugXAbQgYAZgqACIgJAAQhQAAgDg+g");
	this.shape_103.setTransform(400.2,90.4);

	this.shape_104 = new cjs.Shape();
	this.shape_104.graphics.f("#FFFFFF").s().p("AgUAmIAAgwQAAgJgLAAQgNAAgDAIIAAgYQADgHAJgDQAHgDAIAAQAPABAEALQAFgNATABQALAAAHAJQAHAJAAAMQAAAYgXAFQgLABgIgDIAAAoQgNgIgNgDgAAGgVIAAAOQACACADAAQAJAAAAgLQAAgDgCgEQgEgCgCAAQgEAAgCAEg");
	this.shape_104.setTransform(374.6,82.8);

	this.shape_105 = new cjs.Shape();
	this.shape_105.graphics.f("#FFFFFF").s().p("AAeBNIAAgdQAVAAAKgNQAMgNgBgWQAAgTgMgPQgLgOgTABQgVAAgNAPQASAEALANQAMAOgBASQAAAcgVARQgTAPgcAAQhIAAAAhNIABgOIAbAAIgBAEQAAAFAIAAQAGAAAFgDQAGgDAEgGIAEgIIAAgGQAAgHgCgFQgDgEgEAAQgNAAABAOIgeAAIAAgFQAAgQALgLQAMgLAQAAQAOAAAJAGQAKAIABAKQAUgYAjAAQAeAAAUAWQAVAWAAAfQABAggRAWQgSAYghAAgAgdAtQALgBAKgFQAIgGACgKIABgGQAAgOgKgFQgFgDgFAAQgFABgDADQgLAKgHAFQgKAHgKgBQgGAAgDgCQAHAdAkgCg");
	this.shape_105.setTransform(368.8,92.9);

	this.shape_106 = new cjs.Shape();
	this.shape_106.graphics.f("#FFFFFF").s().p("AglBjQgSgHgKgOQgSgYAAgaQAAgeARgSQgVgFAAgWQAAgXAUgSQATgRAZAAQAPAAAIAHQALAHAAAMIgCAGQAJgJAHgPIAkAAQgEANgIANQgNARgQAHIAMACQAbAHAPAVQAOAUAAAZQAAASgIASQgLAVgWALQgVAKgYAAQgUAAgTgHgAgmgEQgLANAAASQAAAVAQAOQAOALAWAAQAbAAAOgSQAKgNAAgPQAAgUgQgPQgNgNgXAAQgbAAgNARgAgvhAQgIAHAAAJQABAJAJAAIARgIQAJgFADgEQAEgEAAgFQgBgHgGgDIgFgBQgMAAgLAMg");
	this.shape_106.setTransform(348.4,90.2);

	this.shape_107 = new cjs.Shape();
	this.shape_107.graphics.f("#FFFFFF").s().p("AhJBGQgTgJgDgQQgCgEAEgGQADgFgBgDQAAAAgBgBQAAAAAAAAQgBgBAAAAQgBAAAAAAQgDAAgEADQgDACgCADIAAgbIA0gwIABgDQAAgCgEgCQgDgCgEABQgIABAAAKQAAAEADADIgeAAIgBgKQAAgPALgJQALgJAOAAQAbgCAHAUQAbgTAkABQAfAAAUATQAXAWAAAlQAAAigTATQgTATghAAIgIAAIAAgbQAWACAMgNQANgNAAgVQAAgYgOgNQgNgNgZAAQgNAAgLAFQARAEAMAQQAMAPAAATQgBAegWARQgTARgfAAIgGAAQgUAAgOgGgAgwgJQgKAIgMALQAFACADAGIAAAEIgBAKQAAAHAKAFQAHADAJAAQAQAAAMgIQAOgIABgPQABgOgIgKQgKgLgNAAIgFAAQgJABgKAJg");
	this.shape_107.setTransform(328.2,93);

	this.shape_108 = new cjs.Shape();
	this.shape_108.graphics.f("#FFFFFF").s().p("AAUBmIAAgyIgcABQhRgDAAg2QAAgaASgPQARgNAZAAIAZAAQABgOgSAAQgPAAgDALIgfAAQACgXARgKQAMgHAVAAQANABAKAHQAMAIAEAPQAHgeAdgBIAcAAIAAAaQgGgEgCAFQgBAEAGAKQAIALAAAJQAAASgNAKQgKAJgSABIAAAXQAPgDAVgMIAAAeQgGAEgLAEQgLAFgIABIAAA0gAgsgZQgKAGgBANQgBAcA2ACIAWgBIAAg2IgoAAQgPAAgJAGgAAxg4IAAAaQAIgBAEgGQACgFgBgHQgBgFgEgHQgEgGACgFQgGADAAANg");
	this.shape_108.setTransform(307.4,95.4);

	this.shape_109 = new cjs.Shape();
	this.shape_109.graphics.f("#FFFFFF").s().p("AhDA/QgbgSAAgeQAAgVAQgOQAQgOAZAAIAXAAQAAgPgXAAQgUABABAMIgfAAIAAgFQAAgRAQgLQAOgIATAAQAcABALAPQAIgJAJgDQAJgDASgBQAXAAALAOQAGAHAAANIAAAHIgiAAQACgEgCgDQgDgGgNAAQgUAAAAAPIAVAAQAaAAAOALQATAQAAAYQAAAegeARQgaANgoAAQgoAAgagOgAg1AAQgIAEAAAKQAAARASAJQAQAIAbAAQAaAAASgJQARgIAAgQQAAgKgJgFQgJgGgMAAIg+AAQgOAAgIAGg");
	this.shape_109.setTransform(277.2,93);

	this.shape_110 = new cjs.Shape();
	this.shape_110.graphics.f("#FFFFFF").s().p("AglA7QgMgOAAgWQACgeAogKIglAAIAAgUIAngOIgvAAIAAgVIBiAAIAAATIgqAOIAqAAIAAAVIgmAPQgXAMgBAQQAAAHAGAHQAGAGAFAAQAJAAAGgGQAFgFAAgHQAAgFgCgDIAhAAIABALQAAAVgPAMQgMAKgXAAQgbAAgNgOg");
	this.shape_110.setTransform(261.7,93.3);

	this.shape_111 = new cjs.Shape();
	this.shape_111.graphics.f("#FFFFFF").s().p("AgnBfQgSgHgKgOQgSgYAAgaQAAgMAEgNQALgjAogPIARgJQANgIAHgJQAIgIAIgQIAkAAQgEANgIANQgNARgQAHIAMACQAaAHAQAVQAOAVAAAYQAAASgIASQgLAWgWAKQgVAKgYAAQgUAAgTgHgAgogIQgLAOAAARQAAAVAQAOQAOALAWAAQAbAAAOgSQAKgOAAgOQAAgUgQgPQgOgNgWAAQgbAAgNARg");
	this.shape_111.setTransform(247.1,90.6);

	this.shape_112 = new cjs.Shape();
	this.shape_112.graphics.f("#FFFFFF").s().p("AAwAbQACgOgRgHQgOgGgTAAQgRAAgOAFQgRAGAAAMIABAEIggAAQgCgFAAgGQAAgUAcgMQAYgKAdAAQAhAAAWAJQAaALAAAUQAAAHgDAGg");
	this.shape_112.setTransform(217.1,82.8);

	this.shape_113 = new cjs.Shape();
	this.shape_113.graphics.f("#FFFFFF").s().p("AhIBGQgUgIgEgRQAAgEADgGQADgFgBgDQAAAAgBgBQAAAAAAAAQgBgBAAAAQgBAAgBAAQgCAAgEADQgEACgBADIAAgbIA0gwIABgDQAAgDgEgBQgDgCgEABQgIABAAAKQAAAEACADIgdAAIgBgKQAAgPALgJQAMgJAOAAQAbgCAFAUQAdgTAiABQAfAAAVATQAXAWAAAlQAAAigTATQgTATggAAIgJAAIAAgbQAVACANgNQAMgMAAgWQAAgYgNgNQgNgNgZAAQgOAAgKAFQARAEAMAQQALAPAAATQABAegXARQgTARggAAIgFAAQgUAAgNgGgAgwgJQgKAIgMALQAFADADAFIABAEIgCAKQAAAHAKAFQAHADAJAAQAQAAANgIQANgIACgPQABgOgJgKQgKgLgOAAIgFAAQgIABgKAJg");
	this.shape_113.setTransform(216.1,93);

	this.shape_114 = new cjs.Shape();
	this.shape_114.graphics.f("#FFFFFF").s().p("AAHBJQgaAAgNgLQgQgOABgfQABgWAQgTQAOgSAPgFIgwAAIAAgZIBdAAIAAAWQgZAQgNANQgRATgCARQgBALADAHQAEALALAAQAHAAAFgGQAEgFAAgHIgCgHIAfAAIABANQAAAUgNAKQgLALgQAAIgDAAg");
	this.shape_114.setTransform(199,93.3);

	this.shape_115 = new cjs.Shape();
	this.shape_115.graphics.f("#FFFFFF").s().p("AAUBmIAAgyIgcABQhRgDAAg2QAAgaASgPQAQgNAaAAIAZAAQAAgOgRAAQgPAAgDALIgfAAQACgXARgKQAMgHAVAAQANABAKAHQAMAIAEAPQAGgeAegBIAcAAIAAAaQgGgEgCAFQgBADAGALQAIALAAAJQAAARgNALQgKAJgSABIAAAXQAPgDAVgMIAAAeQgGAEgLAEQgLAFgIABIAAA0gAgsgZQgLAHAAAMQgBAcA2ACIAWgBIAAg2IgoAAQgPAAgJAGgAAxg4IAAAaQAIgBADgGQADgFgBgHQgBgFgEgHQgDgGAAgFQgFADAAANg");
	this.shape_115.setTransform(184.2,95.4);

	this.shape_116 = new cjs.Shape();
	this.shape_116.graphics.f("#FFFFFF").s().p("AAOAdQgOgGgCgLQgDgLAFgFIgRABQgdAEAAATIAAAEIggAAIgBgKQAAgXAcgNQAYgKAeAAQBMAAABAhQABAOgNAJQgLAIgQABIgCAAQgNAAgMgEgAAdgDQgEADAAADQAAADAEADQAEACADAAQAOABAAgJQAAgGgLgBIgDgBQgDAAgEACg");
	this.shape_116.setTransform(153.8,83);

	this.shape_117 = new cjs.Shape();
	this.shape_117.graphics.f("#FFFFFF").s().p("AgfBGQAAgPALgCQgegDgRgPQgQgPAAgUQAAgbAVgPQATgPAbAAIAlAAQAAgPgbAAQgYAAgEAMIghAAQAEgVASgJQARgLAcACQA9ADgEBCIg+AAQgTAAgLAGQgNAHAAAOQAAAdAvgBQAlAAABgPIAgAAQgBAUgQAMQgQALgQAAQgHABgEADQgGAEAAAEQAAAIASAAQAfAAAAgWIAgAAQgBAagVANQgQALgcAAQgxAAAAgfg");
	this.shape_117.setTransform(156.4,95.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_117},{t:this.shape_116},{t:this.shape_115},{t:this.shape_114},{t:this.shape_113},{t:this.shape_112},{t:this.shape_111},{t:this.shape_110},{t:this.shape_109},{t:this.shape_108},{t:this.shape_107},{t:this.shape_106},{t:this.shape_105},{t:this.shape_104},{t:this.shape_103},{t:this.shape_102},{t:this.shape_101},{t:this.shape_100},{t:this.shape_99},{t:this.shape_98},{t:this.shape_97},{t:this.shape_96},{t:this.shape_95},{t:this.shape_94},{t:this.shape_93},{t:this.shape_92},{t:this.shape_91},{t:this.shape_90},{t:this.shape_89},{t:this.shape_88},{t:this.shape_87},{t:this.shape_86},{t:this.shape_85},{t:this.shape_84},{t:this.shape_83},{t:this.shape_82},{t:this.shape_81},{t:this.shape_80},{t:this.shape_79},{t:this.shape_78},{t:this.shape_77},{t:this.shape_76},{t:this.shape_75},{t:this.shape_74},{t:this.shape_73},{t:this.shape_72},{t:this.shape_71},{t:this.shape_70},{t:this.shape_69},{t:this.shape_68},{t:this.shape_67},{t:this.shape_66},{t:this.shape_65},{t:this.shape_64},{t:this.shape_63},{t:this.shape_62},{t:this.shape_61},{t:this.shape_60},{t:this.shape_59},{t:this.shape_58},{t:this.shape_57},{t:this.shape_56},{t:this.shape_55},{t:this.shape_54},{t:this.shape_53},{t:this.shape_52},{t:this.shape_51},{t:this.shape_50},{t:this.shape_49},{t:this.shape_48},{t:this.shape_47},{t:this.shape_46},{t:this.shape_45},{t:this.shape_44},{t:this.shape_43},{t:this.shape_42},{t:this.shape_41}]}).wait(1));

	// Layer 1
	this.shape_118 = new cjs.Shape();
	this.shape_118.graphics.f("#1144B7").s().p("Ehj/AMEIAA4HMDH/AAAIAAYHg");
	this.shape_118.setTransform(640,76.1);

	this.instance_13 = new lib.Group_2();
	this.instance_13.parent = this;
	this.instance_13.setTransform(1143.8,138.6,1,1,0,0,0,138.3,54.6);
	this.instance_13.alpha = 0.5;

	this.instance_14 = new lib.Group_3();
	this.instance_14.parent = this;
	this.instance_14.setTransform(517.1,142.6,1,1,0,0,0,517.1,50.6);
	this.instance_14.alpha = 0.5;

	this.shape_119 = new cjs.Shape();
	this.shape_119.graphics.f("#CAF9FF").s().p("Ehj/A4QMAAAhwfMDH/AAAMAAABwfg");
	this.shape_119.setTransform(640,359);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_119},{t:this.instance_14},{t:this.instance_13},{t:this.shape_118}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(640,358.8,1282,720.2);
// library properties:
lib.properties = {
	width: 1280,
	height: 720,
	fps: 25,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [
		{src:"images/slide_11_atlas_.png?1540578342116", id:"slide_11_atlas_"},
		{src:"sounds/clap.mp3?1540578342226", id:"clap"},
		{src:"sounds/clicks.mp3?1540578342226", id:"clicks"},
		{src:"sounds/csound.mp3?1540578342226", id:"csound"},
		{src:"sounds/ssound.mp3?1540578342226", id:"ssound"},
		{src:"sounds/tiksound.mp3?1540578342226", id:"tiksound"},
		{src:"sounds/tiksound2.mp3?1540578342226", id:"tiksound2"},
		{src:"sounds/tiksound3.mp3?1540578342226", id:"tiksound3"}
	],
	preloads: []
};




})(lib = lib||{}, images = images||{}, createjs = createjs||{}, ss = ss||{}, AdobeAn = AdobeAn||{});
var lib, images, createjs, ss, AdobeAn;